from ensembl_rest_client import client
import pytest
import requests


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def ensembl_rest_grch37_url():
    """Return a URL for the ensembl GRCh37 endpoint, that can be used to query
    the REST API. This will also perform a ping to make sure the service is ip.
    """
    url = 'https://grch37.rest.ensembl.org'
    ping_endpoint = '{0}/info/ping?'.format(url)
    try:
        r = requests.get(
            ping_endpoint,
            headers={"Content-Type": "application/json"}
        )

        if not r.ok:
            r.raise_for_status()
    except Exception:
        return False

    return url


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def ensembl_rest_grch37(ensembl_rest_grch37_url):
    """Return a URL for the ensembl GRCh37 endpoint, that can be used to query
    the REST API. This will also perform a ping to make sure the service is ip.
    """
    return client.Rest(ensembl_rest_grch37_url, ping=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def ensembl_rest_grch38_url():
    """Return a URL for the ensembl GRCh38 endpoint, that can be used to query
    the REST API. This will also perform a ping to make sure the service is ip.
    """
    url = 'https://rest.ensembl.org'
    ping_endpoint = '{0}/info/ping?'.format(url)
    try:
        r = requests.get(
            ping_endpoint,
            headers={"Content-Type": "application/json"}
        )

        if not r.ok:
            r.raise_for_status()
    except Exception:
        return False

    return url


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def ensembl_rest_grch38(ensembl_rest_grch38_url):
    """Return a URL for the ensembl GRCh37 endpoint, that can be used to query
    the REST API. This will also perform a ping to make sure the service is ip.
    """
    return client.Rest(ensembl_rest_grch38_url, ping=False)
