"""Tests for the overlapping canonical transcript features.
"""
from bio_misc.overlaps import trans_within
import pytest
import tempfile
import os
import csv
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _write_to_file(dir_name, rows):
    """Write the test coordinates to file.

    Parameters
    ----------
    dir_name : `str`
        The directory to create the temp data file in.
    rows : `list` of `list`
        The data to write to the file, each sub list is a row.

    Notes
    -----
    The file is written as a tab delimited file and the first row should be
    a header row.
    """
    fobj, fname = tempfile.mkstemp(dir=dir_name)
    with os.fdopen(fobj, "wt") as outfile:
        writer = csv.writer(outfile, delimiter="\t")
        for i in rows:
            writer.writerow(i)
    return fname


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "coords, species, flank, closest_trans",
    (
        (('8', 125857359), 'human', 250000, 'RP11-1082L8.3-001'),
        (('2', 179669931), 'human', 250000, 'TTN-018'),
    )
)
def test_yield_trans(tmpdir, ensembl_rest_grch37, coords, species, flank,
                     closest_trans):
    """Test that the correct nearest transcript is being returned.
    """
    chr_name = 'chr_name'
    start_pos = 'start_pos'

    test_file = _write_to_file(
        tmpdir,
        [[chr_name, start_pos], [*coords]]
    )

    gen = trans_within.yield_trans_within(
        test_file, ensembl_rest_grch37, species,
        chr_name=chr_name, start_pos=start_pos,
        delimiter="\t", flank=flank
    )
    header = next(gen)

    results = []
    for row in gen:
        results.append(
            dict([(i, row[c]) for c, i in enumerate(header)])
        )

    assert results[0]['external_trans_id'] == closest_trans
