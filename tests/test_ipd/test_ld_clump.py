"""Tests for the LD clumping algorthims
"""
from bio_misc.ipd import ld_clump, ld_handlers, ld_factory, common
from bio_misc.common import utils
from bio_misc.format import uni_id
from genomic_config import genomic_config
import numpy as np
import pytest
import gzip
import csv
import pprint as pp


# There are some broken tests in here, that I really need to investigate when
# I have time
_CLUMP_PARAMS = [
        (1, 0.01, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1, 0.2, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1, 0.5, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1, 0.8, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1, 0.99, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1000, 0.01, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1000, 0.2, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1000, 0.5, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1000, 0.8, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1000, 0.99, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (10000, 0.01, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (10000, 0.2, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (10000, 0.5, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (10000, 0.8, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (10000, 0.99, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (100000, 0.01, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (100000, 0.2, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (100000, 0.5, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (100000, 0.8, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (100000, 0.99, ld_handlers.DbClumpHandler, ld_factory.LDFactory),
        (1, 0.01, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1, 0.2, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1, 0.5, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1, 0.8, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1, 0.99, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1000, 0.01, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1000, 0.2, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1000, 0.5, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1000, 0.8, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (1000, 0.99, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (10000, 0.01, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (10000, 0.2, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (10000, 0.5, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (10000, 0.8, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (10000, 0.99, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (100000, 0.01, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (100000, 0.2, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (100000, 0.5, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (100000, 0.8, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
        (100000, 0.99, ld_handlers.FileClumpHandler, ld_factory.LDFactory),
]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_variants(var_file):
    variants = []
    with gzip.open(var_file, 'rt') as infile:
        reader = csv.DictReader(infile, delimiter="\t")
        for row in reader:
            v = common.ClumpVariant(
                row['chr_name'], int(row['start_pos']), row['effect_allele'],
                row['other_allele'], pvalue=float(row['pvalue']), is_index=0,
                is_clumped=0
            )
            variants.append(v)
    return variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def do_clump_test(variants, corr_matrix, flank, rsquared):
    """This performs a simple LD clumping algorithm to generate comparison data
    for the test. i.e. clumping with a completely distinct bit of code.

    The correlation matrix has been generated with the similar code (that has
    been independently tested against external sources)

    Parameters
    ----------
    variants : `list` or `bio_misc.ipd.common.ClumpVariant`
        The variants to clump
    corr_matrix : `pandas.DataFrame`
        A correlation matrix that has been defined for the variants under test.

    Returns
    -------
    variants : `list` or `bio_misc.ipd.common.ClumpVariant`
        The clumped variants with ``is_index``, ``is_clumped`` and
        ``is_unknown`` attributes set.
    """
    corr_matrix = np.square(corr_matrix.copy())
    variants = variants.copy()
    for i in variants:
        i.is_unknown = set([])
    variants.sort(key=lambda x: x.start_pos)
    variants.sort(key=lambda x: x.pvalue)
    variants.sort(key=lambda x: x.chr_name)

    # Loop through the index variants
    for idx in range(len(variants)):
        i = variants[idx]

        # If it is unknown
        if i.uni_id not in corr_matrix.index:
            i.is_unknown.add(1)
            continue
        elif i.is_clumped == 1 or len(i.is_unknown) > 0:
            # is it already clumped or unknown?
            continue
        elif np.all(np.isnan(corr_matrix.loc[i.uni_id, ])):
            i.is_unknown.add(1)
            continue

        i.is_index = 1
        for jdx in range(idx+1, len(variants)):
            j = variants[jdx]
            if j.uni_id not in corr_matrix.index:
                j.is_unknown.add(1)
                continue
            elif j.is_index == 1 or len(j.is_unknown) > 0:
                continue
            up = i.start_pos - flank
            down = i.start_pos + flank
            ld = corr_matrix.loc[i.uni_id, j.uni_id]
            if i.chr_name == j.chr_name and j.start_pos >= up and \
               j.start_pos <= down and ld >= rsquared:
                j.is_clumped = 1

    return variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# TODO: Add back in and look at why the tests are failing
# @pytest.mark.parametrize(
#     "flank,rsquared,handler,factory", _CLUMP_PARAMS
# )
# def test_ld_clump_c1(tmpdir, ld_clump_data, flank, rsquared, handler, factory):
#     """Run the LD clumping params for cohort 1
#     """
#     _do_clump_test(1, tmpdir, ld_clump_data, flank, rsquared, handler,
#                    factory)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "flank,rsquared,handler,factory", _CLUMP_PARAMS
)
def test_ld_clump_c2(tmpdir, ld_clump_data, flank, rsquared, handler, factory):
    """Run the LD clumping params for cohort 2
    """
    _do_clump_test(2, tmpdir, ld_clump_data, flank, rsquared, handler,
                   factory)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _do_clump_test(idx, tmpdir, ld_clump_data, flank, rsquared, handler,
                   factory):
    config_path, corr_matrix, nobs_matrix, tests, exceptions = ld_clump_data
    td = tests[idx]
    invars = read_variants(td['variants'])

    # Read the config file, if not present will go to the defaults
    config = genomic_config.open(config_file=config_path)
    cohort = common.Cohort(
        config, td['name'], td['species'], td['assembly'],
        td['file_format'], leading_zero=False, samples=td['samples']
    )

    # This is a fudge as we are creating so many cohort objects that
    # SQLite can't store their bitwise integers, so we re-set after each test
    # In reality only single cohorts will be created normally
    # I really need to restructure the test to get a single cohort created
    cohort.cohort_idx = 0
    cohort.bits = 1 << cohort.cohort_idx

    test_vars = dict(
        [
            (i.uni_id, i) for i in  do_clump_test(
                invars, corr_matrix, flank, rsquared
            )
        ]
    )
    temp_db = utils.get_tmp_file(dir=tmpdir)
    handler = ld_handlers.DbClumpHandler(
        td['variants'], temp_db, verbose=False,
        clump_flank=flank, overwrite=True
    )
    factory = ld_factory.LDFactory(
        [cohort], ld_batch_size=500
    )

    result_vars = dict()
    with ld_clump.SingleClumpRun(handler, factory,
                                 rsquared=rsquared) as runner:
        for row in runner.fetch():
            uid = uni_id.make_uni_id(
                row['chr_name'], row['start_pos'],
                row['effect_allele'], row['other_allele']
            )
            result_vars[uid] = row
            # if uid in exceptions:
            #     continue
            try:
                assert test_vars[uid].is_index == row['is_index']
                assert test_vars[uid].is_clumped == row['is_clumped']
                test_unknown = (
                    len(test_vars[uid].is_unknown) * (1 << cohort.cohort_idx)
                )
                assert test_unknown == row['is_unknown']
            except AssertionError:
                print("")
                print(flank, rsquared)
                print(cohort)
                print("CLUMP ROW")
                pp.pprint(row)
                print("TEST")
                print(test_vars[uid])
                print("ROW IS UNKNOWN")
                print(row['is_unknown'])
                print("TEST IS UNKNOWN")
                print(len(test_vars[uid].is_unknown))
                pp.pprint(corr_matrix)
                raise
    # for k, v in test_vars.items():
    #     if v.is_index != result_vars[k]['is_index']:
    #         print("")
    #         print(k)
    #         print(v)
    #         print(result_vars[k])
    #         print(corr_matrix.loc[k,])
