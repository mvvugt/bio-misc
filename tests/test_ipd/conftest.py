"""Fixtures for the ``bio_misc.ipd`` subpackage.
"""
from bio_misc.example_data import examples
import numpy as np
import numpy.ma as ma
import pytest
import math

# fix the random seed for numpy
a_rng = np.random.RandomState(1984)
b_rng = np.random.RandomState(4891)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_nan(data, prop_nan=0.2, prop_missing=0.05):
    nulls = np.random.randint(
        0, data.size - 1, math.ceil(data.size * prop_nan)
    )
    missing = np.random.randint(
        0, data.shape[0] - 1, math.ceil(data.size * prop_missing)
    )
    data.flat[nulls] = np.nan
    data[missing] = np.nan
    return ma.masked_invalid(data)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def ten_by_ten_a():
    nrows = 10
    ncols = 10
    return a_rng.random((nrows, ncols))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def ten_by_ten_nan_a():
    nrows = 10
    ncols = 10
    return add_nan(a_rng.random((nrows, ncols)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def ten_by_ten_b():
    nrows = 10
    ncols = 10
    return b_rng.random((nrows, ncols))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def ten_by_ten_nan_b():
    nrows = 10
    ncols = 10
    return add_nan(b_rng.random((nrows, ncols)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def hundred_by_hundred_a():
    nrows = 100
    ncols = 100
    return a_rng.random((nrows, ncols))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def hundred_by_hundred_nan_a():
    nrows = 100
    ncols = 100
    return add_nan(a_rng.random((nrows, ncols)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def hundred_by_hundred_b():
    nrows = 100
    ncols = 100
    return b_rng.random((nrows, ncols))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def hundred_by_hundred_nan_b():
    nrows = 100
    ncols = 100
    return add_nan(b_rng.random((nrows, ncols)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def ipd_config_file(tmpdir):
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("ipd_config_test", tmpdir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop1_vcf_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop1_vcf_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop1_bgen_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop1_bgen_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop1_corr_matrix():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop1_corr_matrix")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def pop1_variants():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files. Important the the scope is not
    session as they could get modified in parameterised tests.
    """
    return examples.get_data("pop1_variants")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop2_vcf_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop2_vcf_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop2_bgen_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop2_bgen_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop2_corr_matrix():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop2_corr_matrix")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def pop2_variants():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files. Important the the scope is not
    session as they could get modified in parameterised tests.
    """
    return examples.get_data("pop2_variants")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop3_vcf_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop3_vcf_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop3_bgen_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop3_bgen_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def pop3_variants():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files. Important the the scope is not
    session as they could get modified in parameterised tests.
    """
    return examples.get_data("pop3_variants")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop4_vcf_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop4_vcf_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def pop4_bgen_data():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files.
    """
    return examples.get_data("pop4_bgen_data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def pop4_variants():
    """Return a path to a config file that can be used to test
    and IPD extractionn from genotype files. Important the the scope is not
    session as they could get modified in parameterised tests.
    """
    return examples.get_data("pop4_variants")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def ld_clump_data(tmpdir):
    """Return all the paths and data to perform the LD clumping tests.
    """
    # The exceptions are things that give an erroneous test - due to the way
    # I have it structured. Basically, I have pre-calculated LD and these look
    # like missing data as they are NaN, however, they are NaN not because they
    # are missing rather because they are practically monomorphic and give an
    # NaN correlation coefficient. So the test assigns them as missing but the
    # real algorithm calls them indexes (which is intended behaviour). So
    # currently they have to be skipped.
    exceptions = ['15_87017019_C_T']
    config_path, corr_matrix, nobs_matrix, tests = examples.get_data(
        "ld_clump_test_data", tmpdir
    )
    return config_path, corr_matrix, nobs_matrix, tests, exceptions
