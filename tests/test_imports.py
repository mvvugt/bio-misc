import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'bio_misc.annotation.boolean_matrix',
        'bio_misc.annotation.genecards',
        # 'bio_misc.annotation.vcf_id_mapper',
        'bio_misc.common.arguments',
        'bio_misc.common.constants',
        'bio_misc.common.log',
        'bio_misc.common.utils',
        'bio_misc.drug_lookups.bnf_queries',
        'bio_misc.drug_lookups.chembl_queries',
        'bio_misc.drug_lookups.common',
        'bio_misc.drug_lookups.target_effects',
        'bio_misc.drug_lookups.target_summary',
        'bio_misc.drug_lookups.therapeutic_direction',
        'bio_misc.drug_lookups.search_effects',
        'bio_misc.example_data.examples',
        'bio_misc.format.chr_pos',
        'bio_misc.format.common',
        'bio_misc.format.uni_id',
        'bio_misc.id_map.ensembl_gene',
        'bio_misc.id_map.ensembl_human_gene_uniprot',
        'bio_misc.id_map.ensembl_human_uniprot_gene',
        'bio_misc.id_map.ensembl_uniprot',
        'bio_misc.id_map.somalogic_seq_ids',
        'bio_misc.ipd.common',
        'bio_misc.ipd.corr_matrix',
        'bio_misc.ipd.corr_matrix_single',
        'bio_misc.ipd.corr_matrix_zarr',
        'bio_misc.ipd.dosage_zarr',
        'bio_misc.ipd.extract_allele_counts',
        'bio_misc.ipd.index_bgen',
        'bio_misc.ipd.ld_clump',
        'bio_misc.ipd.ld_factory',
        'bio_misc.ipd.ld_handlers',
        'bio_misc.ipd.readers',
        'bio_misc.liftover.crossmap',
        'bio_misc.liftover.quick_lift',
        'bio_misc.overlaps.genes_within',
        'bio_misc.overlaps.nearest_genes',
        'bio_misc.overlaps.site_overlap',
        'bio_misc.overlaps.trans_qtl_link',
        'bio_misc.overlaps.trans_within',
        'bio_misc.pubmed.pubmed_util',
        'bio_misc.pubmed.query_genes',
        'bio_misc.reactome.common',
        'bio_misc.reactome.drug_path_dist',
        'bio_misc.reactome.pairwise_path_dist',
        'bio_misc.reactome.set_path_dist',
        'bio_misc.transforms.effect_flipper',
        'bio_misc.transforms.log_pvalue',
        'bio_misc.umls.mesh_mapping',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    module = importlib.import_module(import_loc, package=None)

