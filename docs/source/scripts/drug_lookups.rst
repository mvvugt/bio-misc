============
Drug Lookups
============

Various scripts that extract information from ChEMBL and the BNF. The scripts in here require several SQLite databases to be in place in order to function. These can obtained as follows:

* ChEMBL database - ChEMBL are really helpful in that they provide an off the shelf `SQLite database <https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/>`_. Look for the file ``*_sqlite.tar.gz``. Note that this will be quite large.
* BNF database - This has to be built using data from the free version of the `BNF <https://bnf.nice.org.uk/>`_. This can be done with `bnf-download <https://gitlab.com/cfinan/bnf_download>`_.

.. _dt_summary:

``bm-drug-target-summary``
--------------------------

.. argparse::
   :module: bio_misc.drug_lookups.target_summary
   :func: _init_cmd_args
   :prog: bm-drug-target-summary


Output columns
~~~~~~~~~~~~~~

This script is designed to annotate an existing data set. So all the columns below will appear after the original data columns. Note that this will cause the original data columns to be replicated as a single uniprot ID can be contained in many different targets. This is because the target definition in ChEMBL is a true biological target that can be a protein complex, protein family or a single target.

.. include:: ../data_dict/drug-target-summary.rst

.. _dt_effects:

``bm-drug-target-effects``
--------------------------

.. argparse::
   :module: bio_misc.drug_lookups.target_effects
   :func: _init_cmd_args
   :prog: bm-drug-target-effects


Output columns
~~~~~~~~~~~~~~

This script is designed to annotate an existing data set. So all the columns below will appear after the original data columns. Note that this will cause the original data columns to be replicated as a single uniprot ID can be contained in many different targets. This is because the target definition in ChEMBL is a true biological target that can be a protein complex, protein family or a single target.

.. include:: ../data_dict/drug-target-effects.rst

.. _search_effects:

``bm-search-drug-effects``
--------------------------

.. argparse::
   :module: bio_misc.drug_lookups.search_effects
   :func: _init_cmd_args
   :prog: bm-search-drug-effects

Example usage
~~~~~~~~~~~~~

To search for drugs that match a set of disease terms stored in a file ``disease_terms.txt``. This file has 38 disease terms, with one per line. This is using database connection parameters stored in a connection file ``.db.cnf``, with the ChEMBL database connection parameters being stored under the section ``chembl_latest`` and the BNF connection parameters being stored under ``bnf_latest``. For information connection parameters in config files see the `BNF-download repository <https://gitlab.com/cfinan/bnf-download#the-config-file>`_. Additionally, the ``--no-targets`` option has been selected so no target information will be in the output file ``~/drug_effects.txt``.

.. code-block:: console

   $ bm-search-drug-effects -vv --search-file disease_terms.txt -c ~/.db.cnf -B bnf_latest -C chembl_latest --no-targets -o ~/drug_effects.txt
   === bm-search-drug-effects (bio_misc v0.2.0a0) ===
   [info] 14:55:35 > bnf value: bnf_latest
   [info] 14:55:35 > chembl value: chembl_latest
   [info] 14:55:35 > config value: /home/user/.db.cnf
   [info] 14:55:35 > no_targets value: True
   [info] 14:55:35 > outfile value: /home/user/drug_effects.txt
   [info] 14:55:35 > search_file value: /home/user/disease_terms.txt
   [info] 14:55:35 > search_terms length: 0
   [info] 14:55:35 > tmp value: None
   [info] 14:55:35 > verbose value: 2
   [info] 14:55:36 > searching for drug effects...
   [info] searching for drug effect terms...: 100%|----| 38/38 [00:09<00:00,  4.17term(s)/s]
   [info] 14:55:46 > *** END ***

Output columns
~~~~~~~~~~~~~~

This script can either return drugs with drug effects matching the input search terms (`--no-targets` option) or it can return drugs and their target information (the default). The number of columns is different in each case. Additionally, if targets are output, each matching drug row will be replicated for each target component, to the total number of rows returned will increase. The columns outlined below are all the columns that are output by default. If the `--no-targets` option is applied then only columns 1-14 will be output.

.. include:: ../data_dict/search-drug-effects.rst

``bm-therapeutic-direction``
----------------------------

.. argparse::
   :module: bio_misc.drug_lookups.therapeutic_direction
   :func: _init_cmd_args
   :prog: bm-therapeutic-direction
