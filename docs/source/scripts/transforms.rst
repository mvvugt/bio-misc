====================
Data transformations
====================

Data transformations is defined loosly here as an operation that will change the data in someway (pretty general I know!).

``bm-effect-flipper``
---------------------

.. argparse::
   :module: bio_misc.transforms.effect_flipper
   :func: _init_cmd_args
   :prog: bm-effect-flipper

Output columns
~~~~~~~~~~~~~~

The effect size column is changed in place, as are any allele columns. However, some additional columns are added as a sanity check and to enable tracking of what has happened to the data.

.. include:: ../data_dict/effect-flipper.rst


``bm-log-pvalue``
-----------------

.. argparse::
   :module: bio_misc.transforms.log_pvalue
   :func: _init_cmd_args
   :prog: bm-log-pvalue

Output columns
~~~~~~~~~~~~~~

The original p-value column is left in place and a negative log10 transformed one is added to the end of the input.

.. include:: ../data_dict/log-pvalue.rst
