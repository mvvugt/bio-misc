==============
Pubmed queries
==============

Various utility scripts that perform pubmed queries. These use the API in `paper-scraper <https://gitlab.com/cfinan/paper-scraper>`_

.. _pubmed_genes:

``bm-pubmed-gene-query``
------------------------

.. argparse::
   :module: bio_misc.pubmed.query_genes
   :func: _init_cmd_args
   :prog: bm-pubmed-gene-query


Output columns
~~~~~~~~~~~~~~

If the ``--counts-only`` flag is omitted then this script will output two files. The first is the summary counts file (the only file output if ``--counts-only`` is used), the second file is a flat file of the pubmed citations.

Currently, both files are uncompressed but this may change in future.

Citation counts file
++++++++++++++++++++

The counts file will contain all the columns given in the input file in addition to the columns below. There will be one row per input row. This is designed to give an overview of the search results.

.. include:: ../data_dict/pubmed-gene-query-counts.rst

Citation information file
+++++++++++++++++++++++++

The citation information file can contain multiple rows per input row. Each row will contain the key data for citation that has been returned from the pubmed search.

.. include:: ../data_dict/pubmed-gene-query-info.rst
