===================================
Liftover/Crossmap genome assemblies
===================================

Liftover a flat file from one assembly to another..

``bm-flat-lift``
----------------

.. argparse::
   :module: bio_misc.liftover.quick_lift
   :func: _init_cmd_args
   :prog: bm-flat-lift
