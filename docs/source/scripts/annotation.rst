===================
Annotating features
===================

This sub-package has some scripts to perform some basic annotation of genomic features.

``bm-genecards-annotate``
-------------------------

.. argparse::
   :module: bio_misc.annotation.genecards
   :func: _init_cmd_args
   :prog: bm-genecards-annotate


Output columns
~~~~~~~~~~~~~~

This script will produce a single output row for each input row (of ``--missing`` is on). It will add the following columns after all the input file columns.

.. include:: ../data_dict/genecards-annotate.rst


``bm-vcf-id-mapper``
--------------------

.. argparse::
   :module: bio_misc.annotation.vcf_id_mapper
   :func: _init_cmd_args
   :prog: bm-vcf-id-mapper


``bm-annotation-matrix``
------------------------

.. argparse::
   :module: bio_misc.annotation.boolean_matrix
   :func: _init_cmd_args
   :prog: bm-annotation-matrix
