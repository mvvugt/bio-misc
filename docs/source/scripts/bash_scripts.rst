==========================
``./resouces/bin`` scripts
==========================

Alongside the installed Python scripts, there are several BASH scripts that may be of use. You can only access these if you clone the ``bio-misc`` repository. To install these you will need the ``bio-misc/resources/bin`` directory within your ``$PATH`` in your ``~/.bashrc``. For example:

.. code-block:: bash

   export "$PATH:/<your>/<path>/<to>/bio-misc/resources/bin"

Also, the `bash-helpers <https://gitlab.com/cfinan/bash-helpers>`_ repository will be installed.

``liftover-bgen.sh``
--------------------

.. _liftover_bgen:

Liftover a bgen file from one genome assembly to another. The basic process is as follows. Convert the BGEN file to a VCF file. Liftover the VCF file and align with the reference genome assembly. Finally, convert the lifted VCF back to a BGEN file.

This allows for the option of lifting over a region of the bgen file so it can be parallelised more easily, these can then be concatenated. If focussing on a region then there is an option to re-map the rsIds using a mapping file.

The input bgen file must be indexed with bgenix.

.. code-block:: console

   $ ./liftover-bgen.sh --help
   = liftover-bgen.sh =

   Liftover a bgen file from one genome assembly to another. The basic process is as follows. Convert the BGEN file to a VCF file. Liftover the VCF file and align with the reference genome assembly. Finally, convert the lifted VCF back to a BGEN file.

   This allows for the option of lifting over a region of the bgen file so it can be parallelised more easily, these can then be concatenated. If focussing on a region then there is an option to re-map the rsIds using a mapping file.

   The input bgen file must be indexed with bgenix.

   USAGE: liftover-bgen.sh [flags] <bgen input file> <bgen output file>
   flags:
     -v,--[no]verbose:  give more output (default: false)
     -T,--tmp-dir:  An alternative location for tmp files, default is to use the system temp dir (default: '')
     -g,--geno-tag:  The name of the genotype format to use in the VCF file, either GP or GT (default: 'GP')
     -s,--species:  The species you are working on (for the genomic-config) (default: 'human')
     -f,--source-assembly:  The source genome assembly (default: 'GRCh37')
     -t,--target-assembly:  The target genome assembly (default: 'GRCh38')
     -r,--refgen-name:  The name of the reference genome assembly in the config file (default: 'local')
     -C,--chr-name:  If lifting over a specific region, this is the chromosome name (default: '-')
     -S,--start-pos:  If lifting over a specific region, this is the start position (default: '-')
     -E,--end-pos:  If lifting over a specific region, this is the end position (default: '-')
     -M,--mapper-name:  The name of the mapping file to use to assign rsIDs (only used if a subset of variants is extracted). This is the name in the config file NOT the path
                        (default: '-')
     -I,--samples:  File containing sample IDs (1 per line) to include (or exclude if prefixed in ^) (default: '-')
     -h,--help:  show this help (default: false)


``cat-bgen-wrap.sh``
--------------------

.. _cat_bgen_wrap:

A wrapper around the ``cat-bgen`` `utility <https://enkre.net/cgi-bin/code/bgen/doc/trunk/doc/wiki/cat-bgen.md>`_ that concatenates bgen files together. The cat-bgen utility is unable to accept a list of file names to be concatenated, this small bash script is designed to handle that and create both a ``.bgi`` index and Python bgen-reader indexes of the final concatenated bgen. The list file should contain one full file path per line.

Obviously, this will need bgenix to be installed, Python bgen-reader and the bm-bgen-reader-index utility (which should be installed when you install bio-misc). There is no help interface for this script but it can be run like:

.. code-block::

   $ ./cat-bgen-wrap.sh /<path>/<to>/<bgen>/list_file.txt /<path>/<to>/<output>/concat-bgen.bgen
