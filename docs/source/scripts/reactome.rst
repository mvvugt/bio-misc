================
Reactome lookups
================

Scripts for performing lookups against a Neo4j copy of the Reactome database.

``bm-drug-target-interact``
---------------------------

.. argparse::
   :module: bio_misc.reactome.drug_path_dist
   :func: _init_cmd_args
   :prog: bm-drug-target-interact


``bm-set-interact``
-------------------

.. argparse::
   :module: bio_misc.reactome.set_path_dist
   :func: _init_cmd_args
   :prog: bm-set-interact


``bm-pairwise-interact``
------------------------

.. argparse::
   :module: bio_misc.reactome.pairwise_path_dist
   :func: _init_cmd_args
   :prog: bm-pairwise-interact
