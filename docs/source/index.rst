.. skeleton-package documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bio-misc
===================

The `bio-misc <https://gitlab.com/cfinan/bio-misc>`_ package is a miscellaneous collection of biological utilities. This is mainly designed with the command line in mind and not necessarily API calls. Having said that I will try to write things in a way to make API usage easier should you need to access from your own scripts.

If sub-packages get too big I might roll them out into their own package and place a deprecation warning on the scripts.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
