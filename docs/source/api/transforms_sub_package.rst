===================================
``bio_misc.transforms`` sub-package
===================================

``bio_misc.transforms.effect_flipper``
--------------------------------------

.. currentmodule:: bio_misc.transforms.effect_flipper
.. autofunction:: effect_flip
.. autofunction:: yield_effect_flip

``bio_misc.transforms.log_pvalue``
----------------------------------

.. currentmodule:: bio_misc.transforms.log_pvalue
.. autofunction:: log_pvalue
.. autofunction:: yield_log_pvalue
.. autofunction:: log_transform
