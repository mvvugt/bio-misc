===============================
``bio_misc.pubmed`` sub-package
===============================

``bio_misc.pubmed.query_genes``
-------------------------------

.. currentmodule:: bio_misc.pubmed.query_genes
.. autofunction:: search_pubmed
.. autofunction:: count_pubmed
.. autofunction:: build_pubmed_query
.. autofunction:: query_ensembl_synonyms
