===============================
``bio_misc.format`` sub-package
===============================

``bio_misc.format.chr_pos``
---------------------------

.. currentmodule:: bio_misc.format.chr_pos
.. autofunction:: split_chr_pos
.. autofunction:: yield_split_chr_pos

``bio_misc.format.common``
--------------------------

.. currentmodule:: bio_misc.format.common
.. autoclass:: ChrPosParser

``bio_misc.format.uni_id``
--------------------------

.. currentmodule:: bio_misc.format.uni_id
.. autofunction:: get_uni_id
.. autofunction:: yield_uni_id
