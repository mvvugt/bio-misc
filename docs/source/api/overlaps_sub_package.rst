=================================
``bio_misc.overlaps`` sub-package
=================================

``bio_misc.overlaps.genes_within``
----------------------------------

.. currentmodule:: bio_misc.overlaps.genes_within
.. autofunction:: get_genes_within
.. autofunction:: yield_genes_within

``bio_misc.overlaps.trans_within``
----------------------------------

.. currentmodule:: bio_misc.overlaps.trans_within
.. autofunction:: get_trans_within
.. autofunction:: yield_trans_within

``bio_misc.overlaps.nearest_genes``
-----------------------------------

.. currentmodule:: bio_misc.overlaps.nearest_genes
.. autofunction:: get_nearest_genes
.. autofunction:: yield_nearest_genes

``bio_misc.overlaps.site_overlap``
----------------------------------

.. currentmodule:: bio_misc.overlaps.site_overlap
.. autofunction:: site_overlaps
.. autofunction:: yield_site_overlaps

``bio_misc.overlaps.trans_qtl_link``
------------------------------------

.. currentmodule:: bio_misc.overlaps.trans_qtl_link
.. autofunction:: get_trans_qtl_link
.. autofunction:: yield_trans_qtl_link
