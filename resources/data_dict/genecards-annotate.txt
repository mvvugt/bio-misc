column	name	data_type	description
1	summary	string	The text based gene summary from gene cards. This is 3 distinct sections rolled into one. The start of each section is prefixed with ``[[<X> Summary for <GENE_NAME> Gene]]`` where ``<X>`` may be ``GeneCards``, ``UniProtKB/Swiss-Prot``, ``Gene Wiki entry`` or ``Additional gene information``.
2	synonyms	string	A pipe ``|`` separated string of various synonyms for the gene name.
