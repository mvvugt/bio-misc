#!/bin/bash
# A task for running a liftover of a genomic chunk from a BGEN file. This expects the job array file to have the following columns IN THIS ORDER:

# 1. rowidx
# 2. region_idx
# 3. chr_name
# 4. start_pos
# 5. end_pos
# 6. nsites
# 7. infile
# 8. outfile
# 9. source_assembly
# 10. target_assembly
# 11. species
# 12. refgen_name
# 13. mapper_name

# This task will accept the following parameters
# 1. job array file (required)
# 2. tmpdir to use (required)
# 3. step size (required)
# 4. rowidx (optional), if not set then the SGETASKID is used

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The main command that is run for each step of --step
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Pull the columns from the current row, rowidx is pulled by task_init.sh
    region_idx="${PROCESS_LINE[1]}"
    chr_name="${PROCESS_LINE[2]}"
    start_pos="${PROCESS_LINE[3]}"
    end_pos="${PROCESS_LINE[4]}"
    nsites="${PROCESS_LINE[5]}"
    infile="${PROCESS_LINE[6]}"
    outfile="${PROCESS_LINE[7]}"
    source_assembly="${PROCESS_LINE[8]}"
    target_assembly="${PROCESS_LINE[9]}"
    species="${PROCESS_LINE[10]}"
    refgen_name="${PROCESS_LINE[11]}"
    mapper_name="${PROCESS_LINE[12]}"
    samples="${PROCESS_LINE[13]}"

    info_msg "region_idx=$region_idx"
    info_msg "chr_name=$chr_name"
    info_msg "start_pos=$start_pos"
    info_msg "end_pos=$end_pos"
    info_msg "nsites=$nsites"
    info_msg "infile=$infile"
    info_msg "outfile=$outfile"
    info_msg "source_assembly=$source_assembly"
    info_msg "target_assembly=$target_assembly"
    info_msg "species=$species"
    info_msg "refgen_name=$refgen_name"
    info_msg "mapper_name=$mapper_name"
    info_msg "samples=$samples"

    app_args=(
          -v
          -T "$WORKING_DIR"
          --species "$species"
          --source-assembly "$source_assembly"
          --target-assembly "$target_assembly"
          --refgen-name "$refgen_name"
          --chr-name "$chr_name"
          --start-pos "$start_pos"
          --end-pos "$end_pos"
          --mapper-name "$mapper_name"
          --samples "$samples"
      )

    # This will be writing to the tmpdir anyway
    liftover-bgen.sh "${app_args[@]}" "$infile" "$outfile"
}

# This actually initialises the running of the job
. task_init.sh
