# HPC task files
These files are used to run jobs in parallel as part of the [HPC-tools](https://gitlab.com/cfinan/cluster) (if this URL does not work try [this](https://gitlab.com/cfinan/hpc-tools)) framework that I put together.
