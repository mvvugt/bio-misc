#!/bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_vcf_header() {
    local vcf_file="$1"
    local bgi_file="$2"
    local header_file="$3"

    local temp_contigs=$(mktemp -p"$WORKING_DIR")
    chrs=($(sqlite3 "$input_bgi" "select chromosome from Variant group by chromosome order by min(file_start_position) asc;"))
    for i in "${!chrs[@]}"; do
        echo "##contig=<ID="$(echo "${chrs[$i]}" | sed 's/^0//')">"
    done > "$temp_contigs"

    local temp_format=$(mktemp -p"$WORKING_DIR")
    bcftools view -h "$vcf_file" |
        grep '^##fileformat=' -P > "$temp_format"

    local temp_header=$(mktemp -p"$WORKING_DIR")
    bcftools view -h "$vcf_file" |
        grep '^##fileformat=' -v -P |
        grep '^##contig=<ID=' -v -P |
        grep '^##bcftools' -v -P |
        head -n-1 > "$temp_header"
    local temp_samples=$(mktemp -p"$WORKING_DIR")
    bcftools view -h "$vcf_file" |
        tail -n1 > "$temp_samples"

    cat "$temp_format" "$temp_contigs" "$temp_header" "$temp_samples" > "$header_file"
}


USAGE=$(cat <<EOF
= liftover-bgen.sh =

Liftover a bgen file from one genome assembly to another. The basic process is as follows. Convert the BGEN file to a VCF file. Liftover the VCF file and align with the reference genome assembly. Finally, convert the lifted VCF back to a BGEN file.

This allows for the option of lifting over a region of the bgen file so it can be parallelised more easily, these can then be concatenated. If focussing on a region then there is an option to re-map the rsIds using a mapping file.

The input bgen file must be indexed with bgenix.

USAGE: $(basename $0) [flags] <bgen input file> <bgen output file>

EOF
)

. shflags

# configure shflags
DEFINE_boolean 'verbose' false 'give more output' 'v'
# DEFINE_boolean 'keep-vcf' false 'keep any intermediate filtered vcf files' 'k'
# # Make the IDs of the final BGEN file chr_pos_<alleles in sort order>
# DEFINE_boolean 'uni-id' false 'Create universal identifiers' 'u'
# Is also passed through to UNIX sort
DEFINE_string 'tmp-dir' "" 'An alternative location for tmp files, default is to use the system temp dir' 'T'
# Any samples that need to be subset from the bgen file
DEFINE_string 'geno-tag' "GP" 'The name of the genotype format to use in the VCF file, either GP or GT' 'g'
DEFINE_string 'species' "human" 'The species you are working on (for the genomic-config)' 's'
DEFINE_string 'source-assembly' "GRCh37" 'The source genome assembly' 'f'
DEFINE_string 'target-assembly' "GRCh38" 'The target genome assembly' 't'
DEFINE_string 'refgen-name' "local" 'The name of the reference genome assembly in the config file' 'r'
DEFINE_string 'chr-name' "-" 'If lifting over a specific region, this is the chromosome name' 'C'
DEFINE_string 'start-pos' "-" 'If lifting over a specific region, this is the start position' 'S'
DEFINE_string 'end-pos' "-" 'If lifting over a specific region, this is the end position' 'E'
DEFINE_string 'mapper-name' "-" 'The name of the mapping file to use to assign rsIDs (only used if a subset of variants is extracted). This is the name in the config file NOT the path' 'M'
DEFINE_string 'samples' "-" 'File containing sample IDs (1 per line) to include (or exclude if prefixed in ^)' 'I'

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# The positional arguments are a list of files that want concatenating
# Run the initialisation, this creates a bunch of functions some globals that
# are useful for general tasks that the script want to perform
# *** IMPORTANT *** - This must be done after shflags has finished
VERBOSE_TO_STDERR=0
. bh_init.sh
. bh_bio.sh

# The -O parameter in bcftools
# u is uncompressed BCF the quickest (but big)
# v is compressed VCF, the slowest but small
BCF_OUT_OPT="z"

echo_prog_name "$0"
set_verbose_arg
check_at_least_positional_args 2 "$@"

positional_query=0
# Check the positional args to see if we want to select a region
# If we do then we set a positional query flag which will determine how we query out
# from the VCF file using bgenix 0=no positional query 1=yes to positional query
if [[ $FLAGS_chr_name != '-' ]]; then
    # If the chromsome name set then the start/end must be set
    if [[ $FLAGS_start_pos == '-' ]] || [[ $FLAGS_end_pos == '-' ]]; then
        error_msg "the chr-name, start-pos and end-pos must all be set or none be set"
        error_exit "$LINENO"
    fi
    positional_query=1
fi

# Make sure the programs that we need to use are present
info_msg "checking bcftools is in your PATH (error at this point means no)"
bcftools --help >/dev/null 2>&1
info_msg "checking qctool is in your PATH (error at this point means no)"
qctool -help >/dev/null 2>&1
info_msg "checking bgenix is in your PATH (error at this point means no)"
bgenix -help >/dev/null 2>&1
info_msg "checking bgzip is in your PATH (error at this point means no)"
bgzip -help >/dev/null 2>&1
info_msg "checking tabix is in your PATH (error at this point means no)"
tabix --help >/dev/null 2>&1
info_msg "checking flat-lift is in your PATH (error at this point means no)"
bm-flat-lift --help >/dev/null 2>&1
info_msg "checking vcf-id-mapper is in your PATH (error at this point means no)"
bm-vcf-id-mapper --help >/dev/null 2>&1

# info_msg respects the verbosity that will be defined in FLAGS_verbose
# if shflags is being used. If not set the global VERBOSE argument
info_msg "temp directory: $TMPDIR"

# The working directory is a temp directory within TMPDIR created by
# bh_init.sh and deleted upon exit (either clean or error)
info_msg "working temp directory: $WORKING_DIR"

# Grab the output file and then remove it from $@
input_bgen=$(readlink -f "$1")
info_msg "input file: $input_bgen"
shift
check_file_readable "$input_bgen" "bgen input not readable"

# Make sure the bgenix index is available
input_bgi="$input_bgen".bgi
check_file_readable "$input_bgi" "bgen index file not readable (same as bgen file with .bgi ext)"

output_bgen=$(readlink -f "$1")
info_msg "output file: $output_bgen"
shift
check_file_writable "$output_bgen" "bgen output location not writable"

# Make sure that the input/output file paths are not the same
if [[ "$input_bgen" == "$output_bgen" ]]; then
   error_msg "input file and output file the same"
   error_exit "$LINENO"
fi

# Get the output directory as we will need it for copying over the
# liftover failures
outdir="$(dirname "$output_bgen")"

# Create a file name that will hold all the liftover failures, we use the
# output filename file for this
fail_base_file=$(basename "$output_bgen" | sed 's/\.bgen$//')".liftfail.txt.gz"
fail_temp_file="${WORKING_DIR}"/"$fail_base_file"

# This will hold the BGEN->VCF converted file
temp_vcf=$(mktemp --suffix ".vcf.gz" -p"$WORKING_DIR")
info_msg "converting bgen to vcf: $temp_vcf"

if [[ $positional_query -eq 1 ]]; then
    range="${FLAGS_chr_name}:${FLAGS_start_pos}-${FLAGS_end_pos}"
    info_msg "converting (${range}) bgen to vcf: $temp_vcf"
    bgenix -incl-range "$range" \
           -g "$input_bgen" -vcf |
        sed 's/^0\+//' |
        bgzip > "$temp_vcf"
    tabix -p"vcf" "$temp_vcf"
else
    info_msg "converting whole file bgen to vcf: $temp_vcf"
    bgenix -g "$input_bgen" -vcf |
        sed 's/^0\+//' |
        bgzip > "$temp_vcf"
    tabix -p"vcf" "$temp_vcf"
fi

# Get the locations of the chain files required for the liftover and the
# reference genome assembly required for the alignment of the lifted variants
# to the reference genome.
chain_file="$(genomic-config "chain" "$FLAGS_species" "$FLAGS_source_assembly" "$FLAGS_target_assembly")"
ref_genome="$(genomic-config "refgen" "$FLAGS_species" "$FLAGS_target_assembly" "$FLAGS_refgen_name")"
info_msg "using chain file: $chain_file"
info_msg "using reference genome: $ref_genome"

# Build a header for the extracted VCF file that we drop in before the lift
# over
temp_header=$(mktemp -p"$WORKING_DIR")
build_vcf_header "$temp_vcf" "$input_bgi" "$temp_header"
info_msg "split to bi-allelics, lift, align and normalise"

## temp_norm="${TEST_DIR}/test_bgen_convert.norm.vcf.gz"
temp_norm=$(mktemp --suffix ".vcf.gz" -p"$WORKING_DIR")

# Added a VCF sort in here to account for the liftover throwing out the
# positional order
bcftools view -H "$temp_vcf" |
    cat "$temp_header" - |
    bcftools norm -m "-any" |
    bm-flat-lift -d $'\t' \
              -T "$WORKING_DIR" \
              --comment "##" \
              -c "#CHROM" \
              -s "POS" \
              -R "REF" \
              -f "$fail_temp_file" \
              --out-comments \
              --no-index \
              "$chain_file" |
    bcftools norm -c"x" -f"$ref_genome" |
    bcftools sort -T "$WORKING_DIR" |
    bgzip > "$temp_norm"


# This will hold a sample file that will be used when the final bgen file
# is generated
temp_samples="$(mktemp --suffix ".sample" -p"$WORKING_DIR")"

# If we have a sample file
if [[ $FLAGS_samples == "-" ]]; then
    # Create a sample file, we need to give that to QCTool to make sure the
    # samples are integrated into the bgen output file
    info_msg "generating sample file: $temp_samples"
    qctool -g "$input_bgen" -filetype "bgen" -os "$temp_samples"
else
    if [[ "$FLAGS_samples" =~ ^\^ ]]; then
        info_msg "subset samples/excluding samples..."
    else
        info_msg "subset samples/including samples..."
    fi

    ## temp_norm="${TEST_DIR}/test_bgen_convert.norm.vcf.gz"
    temp_subset=$(mktemp --suffix ".vcf.gz" -p"$WORKING_DIR")

	# Using force samples just in case not all samples are in all
	#  chromosomes
    bcftools view --force-samples -S "$FLAGS_samples" "$temp_norm" | bgzip > "$temp_subset"
    tabix -p"vcf" "$temp_subset"
    rm "$temp_norm"
    temp_norm="$temp_subset"

    info_msg "generating sample file: $temp_samples"
    qctool -g "$temp_norm" -os "$temp_samples"
fi


# If we are doing a positional query and gave provided the name for the
# mapping file then we map the VCF to get the latest rsIDs
if [[ $positional_query -eq 1 ]] && [[ $FLAGS_mapper_name != '-' ]]; then
    temp_mapped=$(mktemp --suffix ".vcf.gz" -p"$WORKING_DIR")
    mapping_file="$(genomic-config "mapping" "$FLAGS_species" "$FLAGS_target_assembly" "$FLAGS_mapper_name")"
    info_msg "using mapping file: $mapping_file"
    info_msg "writing to: $temp_mapped"
    bm-vcf-id-mapper -T "$WORKING_DIR" \
                  -i "$temp_norm" \
                  -o "$temp_mapped" \
                  "$mapping_file"
    temp_norm="$temp_mapped"
    # echo "press ENTER to continue> "
    # read enter
fi


info_msg "creating bgen using: $temp_norm"
# echo "press ENTER to continue> "
# read enter

# Conver the VCF file back to a bgen and index with bgenix
temp_bgen=$(mktemp --suffix ".bgen" -p"$WORKING_DIR")
qctool -vcf-genotype-field "GP" \
       -s "$temp_samples" \
       -g "$temp_norm" \
       -filetype "vcf" \
       -og "$temp_bgen"
bgenix -clobber -g "$temp_bgen" -index

# Move the bgen, liftover fails and index to their final locations
mv "$fail_temp_file"  "$outdir"/"$fail_base_file"
mv "$temp_bgen".bgi "$output_bgen".bgi
mv "$temp_bgen" "$output_bgen"
