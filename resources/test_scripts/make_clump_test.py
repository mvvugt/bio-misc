#!/usr/bin/env python
"""A throwaway script to generate some test data for LD clumping
"""
from genomic_config import genomic_config
from bio_misc.ipd import common, corr_matrix
from tqdm import tqdm
import gzip
import re
import argparse
import csv
import os
import pprint as pp
import pysam


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_infile(in_filename, pvalue_cut, delimiter, pval_col, colmap):
    data_rows = 0
    all_vars = []
    with gzip.open(in_filename, 'rt') as infile:
        reader = csv.DictReader(infile, delimiter=delimiter)
        for row in tqdm(reader):
            try:
                row[pval_col] = float(row[pval_col])
            except ValueError:
                continue

            if row[pval_col] <= pvalue_cut:
                row = dict([(n, row[o]) for o, n in colmap])
                v = common.Variant(
                    row['chr_name'], row['start_pos'],
                    row['effect_allele'],
                    row['other_allele']
                    )
                v.row = row
                all_vars.append(v)
                data_rows += 1
    return all_vars


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def read_filtered(in_filename, delimiter):
#     variants = []
#     with gzip.open(in_filename, 'rt') as infile:
#         reader = csv.DictReader(infile, delimiter=delimiter)
#         for row in reader:
#             variants.append(common.Variant(
#                 row['chr_name'], row['start_pos'], row['effect_allele'],
#                 row['other_allele']
#             ))
#     return variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_vars(out_filename, header, variants, delimiter):
    with gzip.open(out_filename, 'wt') as infile:
        writer = csv.DictWriter(infile, header, delimiter=delimiter)
        writer.writeheader()
        for v in variants:
            row = v.row
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_corr_matrix(config, corr_file, nobs_file, delimiter):
    cohort = common.Cohort(
        config, "1KG_SCRATCH", "human", "b37", "vcf", samples="1KG_EUR"
    )
    try:
        cm, nobs = corr_matrix.get_corr_matrix(
            variants, [cohort], verbose=2, batch_size=50,
            processes=1
        )
    finally:
        cohort.close()

    cm.to_csv(corr_file, sep=delimiter, index=True, header=True)
    nobs.to_csv(nobs_file, sep=delimiter, index=True, header=True)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_vcf(config, variants, outfile, header, buffer=0):
    var_file = re.sub(r".vcf", ".txt.gz", outfile)
    write_vars(var_file, header, variants, delimiter)

    variants.sort(key=lambda x: x.start_pos)
    variants.sort(key=lambda x: int(x.chr_name))
    # pp.pprint(variants)
    provider = config.get_cohort_region_provider(
        'human', 'b37', '1KG_SCRATCH', 'vcf'
    )
    v = variants[0]
    header_file = provider.get_files(v.chr_name, v.start_pos, v.end_pos)[0]

    extract_vars = []
    with open(outfile, 'wt') as outvcf:
        with gzip.open(header_file, 'rt') as header_vcf:
            for row in header_vcf:
                outvcf.write(row)
                if row.startswith("#CHROM"):
                    break

        prev_var = ""
        vcffile = None
        for v in variants:
            if v.uni_id != prev_var:
                try:
                    vcffile.close()
                except AttributeError:
                    pass
                vcffile = pysam.TabixFile(
                    provider.get_files(v.chr_name, v.start_pos, v.end_pos)[0]
                )

            for row in vcffile.fetch(
                    v.chr_name, v.start_pos - buffer, v.end_pos + buffer
            ):
                extract_vars.append(row)
            #     print(row[:30])
            # print(v)
        extract_vars.sort(key=lambda x: int(x.split("\t")[1]))
        extract_vars.sort(key=lambda x: int(x.split("\t")[0]))
        prev_row = ""
        for i in extract_vars:
            if i == prev_row:
                continue
            outvcf.write(f"{i}\n")
            prev_row = i
    pysam.tabix_compress(outfile, f"{outfile}.gz", force=True)
    pysam.tabix_index(f"{outfile}.gz", force=True, preset="vcf")
    os.unlink(outfile)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_samples(config, outfile):
    """Write the samples
    """
    samples = config.get_samples("1KG_EUR")
    with open(outfile, 'wt') as outsamples:
        for i in samples:
            outsamples.write(f"{i}\n")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == "__main__":
    out_variants = "clump_combined_vars.txt.gz"
    out_corr = "clump_corr.txt.gz"
    out_nobs = "clump_nobs.txt.gz"

    combined_vcf = "cohort_combined.vcf"
    cohort_c1_vcf = "cohort_c2.vcf"
    cohort_c2_vcf = "cohort_c1.vcf"
    outsamples = "cohort_eur_samples.txt"

    pval_col = "P"
    pval_cut = 5E-06
    delimiter = "\t"
    colmap = [
        ('CHR', 'chr_name'),
        ('SNP', 'var_id'),
        ('POS', 'start_pos'),
        ('A1', 'effect_allele'),
        ('A2', 'other_allele'),
        ('N', 'sample_size'),
        ('AF1', 'effect_allele_freq'),
        ('BETA', 'effect_size'),
        ('SE', 'standard_error'),
        ('P', 'pvalue'),
        ('INFO', 'imputation_info')
    ]

    config = genomic_config.open()
    parser = argparse.ArgumentParser(description="make clump test data")
    parser.add_argument('infile', type=str, help="input GWAS sum stats")
    args = parser.parse_args()

    # Read the config file, if not present will go to the defaults
    variants = filter_infile(
        args.infile, pval_cut, delimiter, pval_col, colmap
    )
    # variants = read_filtered(out_variants, delimiter)
    data_rows = len(variants)
    print(f"data rows {data_rows}")

    var_header = [v for k, v in colmap]
    get_corr_matrix(config, out_corr, out_nobs, delimiter)
    write_vcf(config, variants, combined_vcf, var_header, buffer=100)
    write_vcf(
        config,
        [variants[i] for i in range(0, len(variants), 2)],
        cohort_c1_vcf,
        var_header,
        buffer=10
    )
    write_vcf(
        config,
        [variants[i] for i in range(1, len(variants), 2)],
        cohort_c2_vcf,
        var_header,
        buffer=10
    )
    write_samples(config, outsamples)
