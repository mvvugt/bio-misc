"""Map uniprot IDs to ensembl gene IDs using a package mapping file
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.example_data import examples
from tqdm import tqdm
import argparse
import csv
import os
import sys
# import pprint as pp


_SCRIPT_NAME = 'bm-map-ensembl-gene-offline'
"""The name of the script that is implemented in this module (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_ids(infile, mapper, outfile=None, verbose=False,
                 tmp_dir=None, no_mapping=False,
                 uniprot_id=con.UNIPROT_PROT_ID,
                 delimiter=con.DEFAULT_DELIMITER,
                 encoding=con.DEFAULT_ENCODING,
                 comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    mapper : `dict`
        The keys of the `dict` are uniprot IDs and the values are lists
        of tuples the [0] ensembl gene ID, [1] gene version, [2] the mapping
        type i.e. ``DIRECT`` or ``SEQUENCE_MAP``.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    uniprot_id : `str`, optional, default: `bio_misc.common.constants.UNIPROT_PROT_ID`
        The uniprot ID column that contains the IDs to map to ensembl gene IDs.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_gene_ids(infile, mapper, uniprot_id=uniprot_id,
                               no_mapping=no_mapping, yield_header=True,
                               encoding=encoding, delimiter=delimiter,
                               comment=comment),
                desc="[progress] fetching gene IDs: ",
                unit=" returned IDs",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_gene_ids(infile, mapper, uniprot_id=con.UNIPROT_PROT_ID,
                   delimiter=con.DEFAULT_DELIMITER, no_mapping=False,
                   encoding=con.DEFAULT_ENCODING, yield_header=True,
                   comment=con.DEFAULT_COMMENT):
    """Yield uniprot mappings to Ensembl gene identifiers.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    mapper : `dict`
        The keys of the `dict` are uniprot IDs and the values are lists
        of tuples the [0] ensembl gene ID, [1] gene version, [2] the mapping
        type i.e. ``DIRECT`` or ``SEQUENCE_MAP``.
    uniprot_id : `str`, optional, default: `bio_misc.common.constants.UNIPROT_PROT_ID`
        The uniprot ID column that contains the IDs to map to ensembl gene IDs.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    TypeError
        If any search terms are not `str` or `re.Pattern` objects.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    with utils.BaseParser(infile, **kwargs) as parser:
        uniprot_id_idx = parser.get_col_idx(uniprot_id)
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                [
                    'ensembl_gene_id_mapping_input_idx',
                    'ensembl_gene_id',
                    'ensembl_gene_version',
                    'mapping_type',
                    'bad_uniprot_id'
                ]
            )

        for idx, row in enumerate(parser, 1):
            has_mapping = False
            flag = False

            try:
                for mapping in mapper[row[uniprot_id_idx]]:
                    has_mapping = True
                    yield row + [idx] + list(mapping) + [int(flag)]
            except KeyError:
                flag = True

            if no_mapping is True and has_mapping is False:
                yield row + [idx] + ([None] * 3) + [int(flag)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    mapper = examples.get_data('uniprot_to_gene', args.assembly)

    try:
        get_gene_ids(
            args.infile, mapper, outfile=args.outfile, no_mapping=args.no_map,
            delimiter=args.delimiter, verbose=args.verbose,
            encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, uniprot_id=args.prot_id
        )
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Map uniprot IDs identifiers to ensembl gene IDs. This "
        "uses a package mapping file for human mappings only covering either"
        " GRCh37 or GRCh38."
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, ensembl_id=False)
    parser.add_argument('--assembly', choices=['b37', 'b38'], default='b38')
    parser.add_argument(
        '--no-map', action="store_true",
        help="Include rows that have no mapping in the output file."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
