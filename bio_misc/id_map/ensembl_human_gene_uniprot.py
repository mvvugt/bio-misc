"""Map ensembl gene IDs to uniprot IDs using a package mapping file
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.example_data import examples
from tqdm import tqdm
import argparse
import csv
import os
import sys
# import pprint as pp


_SCRIPT_NAME = 'bm-map-uniprot-offline'
"""The name of the script that is implemented in this module (`str`)
"""
_SWISSPROT_NAME = "Uniprot/SWISSPROT"
"""The name of the UniProt/SwissProt database that Ensembl uses (`str`)
"""
_TREMBL_NAME = "Uniprot/SPTREMBL"
"""The name of the UniProt/Trembl database that Ensembl uses (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_uniprot_ids(infile, mapper, outfile=None, verbose=False,
                    tmp_dir=None, no_mapping=False, include_trembl=False,
                    ensembl_gene_id=con.ENSEMBL_GENE_ID,
                    delimiter=con.DEFAULT_DELIMITER,
                    encoding=con.DEFAULT_ENCODING,
                    comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    mapper : `dict`
        The keys of the `dict` are ensembl gene IDs and the values are lists
        of tuples that contain the ``[0]`` uniprot IDs they map to ``[1]`` the
        database (either ``Uniprot/SPTREMBL`` or ``"Uniprot/SWISSPROT") and the
        type of mapping, i.e. ``DIRECT`` or ``SEQUENCE_MAP``.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    include_trembl : `bool`, optional, default: `False`
        Include mappings to UniProt/TrEMBL in addition to UniProt/SwissProt.
    ensembl_gene_id : `str`, optional, default: `con.ENSEMBL_GENE_ID`
        The Ensembl Gene ID column that contains the gene IDs to map to
        uniprot.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_uniprot_ids(infile, mapper,
                                  ensembl_gene_id=ensembl_gene_id,
                                  no_mapping=no_mapping, yield_header=True,
                                  encoding=encoding, delimiter=delimiter,
                                  comment=comment,
                                  include_trembl=include_trembl),
                desc="[progress] fetching uniprot IDs: ",
                unit=" returned IDs",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_uniprot_ids(infile, mapper, ensembl_gene_id=con.ENSEMBL_GENE_ID,
                      delimiter=con.DEFAULT_DELIMITER, no_mapping=False,
                      encoding=con.DEFAULT_ENCODING, yield_header=True,
                      comment=con.DEFAULT_COMMENT, include_trembl=False):
    """Yield uniprot mappings to Ensembl gene identifiers.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    mapper : `dict`
        The keys of the `dict` are ensembl gene IDs and the values are lists
        of tuples that contain the ``[0]`` uniprot IDs they map to ``[1]`` the
        database (either ``Uniprot/SPTREMBL`` or ``"Uniprot/SWISSPROT") and the
        type of mapping, i.e. ``DIRECT`` or ``SEQUENCE_MAP``.
    ensembl_gene_id : `str`, optional, default: `con.ENSEMBL_GENE_ID`
        The Ensembl Gene ID column that contains the gene IDs to map to uniprot.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    include_trembl : `bool`, optional, default: `False`
        Include mappings to UniProt/TrEMBL in addition to UniProt/SwissProt.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    db_names = [_SWISSPROT_NAME]
    if include_trembl is True:
        db_names.append(_TREMBL_NAME)

    with utils.BaseParser(infile, **kwargs) as parser:
        ensembl_gene_id_idx = parser.get_col_idx(ensembl_gene_id)
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                [
                    'ensembl_uniprot_id_mapping_input_idx',
                    'uniprot_id',
                    'uniprot_database',
                    'mapping_type',
                    'bad_ensembl_gene_id'
                ]
            )

        for idx, row in enumerate(parser, 1):
            has_mapping = False
            flag = False

            try:
                for mapping in mapper[row[ensembl_gene_id_idx]]:
                    if mapping[1] in db_names:
                        has_mapping = True
                        yield row + [idx] + list(mapping) + [int(flag)]
            except KeyError:
                # Invalid IDs
                flag = True

            if no_mapping is True and has_mapping is False:
                yield row + [idx] + ([None] * 3) + [int(flag)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    mapper = examples.get_data('gene_to_uniprot', args.assembly)

    try:
        get_uniprot_ids(
            args.infile, mapper, outfile=args.outfile, no_mapping=args.no_map,
            delimiter=args.delimiter, verbose=args.verbose,
            encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, ensembl_gene_id=args.gene_id,
            include_trembl=args.trembl
        )
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Map ensembl gene IDs to uniprot IDs. This uses a package"
        " mapping file for human mappings only covering either GRCh37 or "
        "GRCh38."
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, uniprot_id=False)
    parser.add_argument('--assembly', choices=['b37', 'b38'], default='b38')
    parser.add_argument(
        '--no-map', action="store_true",
        help="Include rows that have no mapping in the output file."
    )
    parser.add_argument(
        '--trembl', action="store_true",
        help="Include UniProt/TrEMBL IDs, in addition to UniProt/SwissProt."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
