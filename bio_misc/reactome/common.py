"""Common functions for processing some reactome data
"""
from bio_misc.reactome import common as rcommon
from multiprocessing import Pool
from py2neo import Graph
from py2neo.data import Path
import itertools
import numpy as np
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def connect(host, port, user, pw):
    """Connect to a Neo4j database.

    Parameters
    ----------
    host : `str`
        The Neo4j hostname
    port : `int`
        The Neo4j port
    user : `str`
        The Neo4j username
    pw : `str`
        The Neo4j password

    Returns
    -------
    graph : `py2neo.database.Graph`
        A connection to the reactome graph database
    """
    # print(host, port, user, pw)
    # Connect to graph 7687 if locally
    reactome_conn = Graph(
        "bolt://{0}:{1}".format(host, port), auth=(user, pw)
    )
    return reactome_conn


_USER = None
_HOST = None
_PORT = None
_PASSWORD = None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_path_dist(user, password, host, port, compare_this, with_this,
                    processes=1, batch_size=100):
    """
    """
    if processes == 1:
        reactome_conn = rcommon.connect(
            host, port, user, password
        )

        for prot1, prot2 in itertools.product(compare_this, with_this):
            if prot1 == prot2:
                yield prot1, prot2, None
                continue

            pl = get_shortest_interactor_path(reactome_conn, prot1, prot2)

            if isinstance(pl, Path):
                pathlen = len(pl)
                yield prot1, prot2, pathlen
            else:
                yield prot1, prot2, None
    else:
        global _USER
        global _HOST
        global _PORT
        global _PASSWORD

        _USER = host
        _HOST = port
        _PORT = user
        _PASSWORD = password

        args = []
        for chunk in chunks(
                list(itertools.product(compare_this, with_this)), batch_size
        ):
            args.append(chunk)
        #     chunk.append((host, port, user, password))
        #     # args.append(((host, port, user, password), chunk))
        #     args.append(tuple(chunk))
        # pp.pprint(args)
        # pp.pprint(args)
        # args = list(itertools.product(compare_this, with_this))
        with Pool(processes=processes) as pool:
            # results = pool.starmap_async(_run_path_dist_pool, args)
            results = pool.imap_unordered(_run_path_dist_pool, args)
            for r in results:
                for row in r:
                    yield row

                # pp.pprint(r)
                # break

            # print(type(results))
            # while True:
            #     print("looping")
            #     r = results.next()
            #     pp.pprint(r)
            #     break

        # _USER = None
        # _HOST = None
        # _PORT = None
        # _PASSWORD = None

            # for batch in results:
            #     for row in batch:
            #         yield row


        # while True:
        #     args = []
        #     for i in range(processes):
        #         try:
        #             args.append(((host, port, user, password), next(comparisons)))
        #         except StopIteration:
        #             break

        #     if len(args) == 0:
        #         break

        #     with Pool(processes=processes) as pool:
        #         results = pool.starmap(_run_path_dist_pool, args)
        #         for batch in results:
        #             for row in batch:
        #                 yield row
        # comparisons = chunks(
        #     list(itertools.product(compare_this, with_this)), batch_size
        # )

        # while True:
        #     args = []
        #     for i in range(processes):
        #         try:
        #             args.append(((host, port, user, password), next(comparisons)))
        #         except StopIteration:
        #             break

        #     if len(args) == 0:
        #         break

        #     with Pool(processes=processes) as pool:
        #         results = pool.starmap(_run_path_dist_pool, args)
        #         for batch in results:
        #             for row in batch:
        #                 yield row


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _run_path_dist_pool(batch):
    # pp.pprint(batch)
    # print("CALLED!!!")
    # pp.pprint(job)
    reactome_conn = rcommon.connect(
        _USER, _HOST, _PORT, _PASSWORD
    )
    results = []
    for prot1, prot2 in batch:
        if prot1 == prot2:
            results.append((prot1, prot2, None))
            continue

        pl = get_shortest_interactor_path(reactome_conn, prot1, prot2)

        if isinstance(pl, Path):
            pathlen = len(pl)
            results.append((prot1, prot2, pathlen))
        else:
            results.append((prot1, prot2, None))
    # print("returning")
    return results

# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def _run_path_dist_pool(conn_args, batch):
#     print("CALLED!!!")
#     # pp.pprint(job)
#     reactome_conn = rcommon.connect(
#         *conn_args
#     )
#     results = []
#     for prot1, prot2 in batch:
#         if prot1 == prot2:
#             results.append((prot1, prot2, None))
#             continue

#         pl = get_shortest_interactor_path(reactome_conn, prot1, prot2)

#         if isinstance(pl, Path):
#             pathlen = len(pl)
#             results.append((prot1, prot2, pathlen))
#         else:
#             results.append((prot1, prot2, None))
#     print("returning")
#     return results


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_shortest_interactor_path(graph, prot1, prot2):
    """Get the shortest path via ``interactor`` relationship edges between two
    uniprot protein IDs.

    No relationship direction is inferred during the query. A good test for
    this function is: ``Q9Y6Q6`` vs. ``O14788``

    Parameters
    ----------
    graph : `py2neo.database.Graph`
        A connection to the reactome graph database
    prot1 : `str`
        The first uniprot ID to test
    prot2 : `str`
        The second uniprot ID to test

    Returns
    -------
    shortest_interactor_path : `int` or `numpy.nan`
        The shortest path between the two uniprot IDs that goes via interactor
        edges. If no such path exists then a `numpy.nan` will be returned
    """
    cql = """
    MATCH x=shortestPath((re1:ReferenceGeneProduct{identifier:$prot1})-[:interactor*]-(re2:ReferenceGeneProduct{identifier:$prot2}))
    RETURN x as sp
    ORDER BY LENGTH(x) ASC
    LIMIT 1
    """
    # Run the query and get the cursor
    c = graph.run(
        cql,
        parameters={'prot1': prot1, 'prot2': prot2}
    )

    try:
        return next(c)['sp']
    except StopIteration:
        return np.nan
