"""Determine the protein-protein interaction path distances between two sets of
proteins
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
from bio_misc.reactome import common as rcommon
# from bio_misc.drug_lookups import chembl_queries as cq
from tqdm import tqdm
# from sqlalchemy import text
import argparse
import getpass
import csv
import os
# import sys
# import re
# import pprint as pp


_SCRIPT_NAME = 'bm-set-interact'
"""The name of the script that is implemented in this module (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_set_interactions(
        infile1, infile2, reactome_user, reactome_pass, reactome_host,
        reactome_port, outfile=None, verbose=False, processes=1,
        prot_id1=con.UNIPROT_PROT_ID, tmp_dir=None, comment=con.DEFAULT_COMMENT,
        delimiter=con.DEFAULT_DELIMITER, encoding=con.DEFAULT_ENCODING,
        prot_id2=con.UNIPROT_PROT_ID):
    """The main high level API function that takes an input file and generates
    path distances between proteins in each file.
    """
    unique_prot1 = get_unique_proteins(
        infile1, prot_id1, comment=comment,
        delimiter=delimiter, encoding=encoding
    )
    unique_prot2 = get_unique_proteins(
        infile2, prot_id2, comment=comment,
        delimiter=delimiter, encoding=encoding
    )
    ncomparisons = len(unique_prot1) * len(unique_prot2)
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        writer.writerow(
            ["source_uniprot_id", "target_uniprot_id", "path_length"]
        )
        for row in tqdm(
                rcommon.yield_path_dist(
                    reactome_user, reactome_pass, reactome_host,
                    reactome_port, unique_prot1, unique_prot2,
                    processes=processes
                ),
                desc="[progress] fetching path lengths: ",
                unit=" comparisons",
                disable=not verbose,
                total=ncomparisons
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_unique_proteins(infile, prot_id_col, **kwargs):
    """
    """
    unique_prot = []
    with utils.BaseParser(infile, **kwargs) as parser:
        prot_id_idx = parser.get_col_idx(prot_id_col)

        for row in parser:
            prot_id = row[prot_id_idx]

            if prot_id is not None and prot_id not in unique_prot:
                unique_prot.append(prot_id)

    return unique_prot


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Make sure the password does not appear on screen, (a bit of a hack)
    neo_password = args.neo_pass
    args.neo_pass = '*****'

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    get_set_interactions(
        args.infile1, args.infile2, args.neo_user, neo_password, args.neo_host,
        args.neo_port, outfile=args.outfile, delimiter=args.delimiter,
        verbose=args.verbose, encoding=args.encoding, processes=args.processes,
        comment=args.comment, tmp_dir=args.tmp_dir, prot_id1=args.prot_id1,
        prot_id2=args.prot_id2
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Get the P-P interaction path distance between proteins "
        "in two sets. Both files must have same delimiter etc.. but can have"
        " different prot ID column names"
    )
    parser.add_argument('infile1', type=str,
                        help="an input file with a column of uniprot IDs")
    parser.add_argument('infile2', type=str,
                        help="an input file with a column of uniprot IDs")
    arguments.add_input_output(
        parser, infile=False, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        "--prot-id1", type=str, help="the protein ID column for file 1"
    )
    parser.add_argument(
        "--prot-id2", type=str, help="the protein ID column for file 2"
    )
    arguments.add_neo4j_arguments(parser)
    parser.add_argument('--processes', type=int, default=1,
                        help="The number of processes to use")
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it.

    Returns
    -------
    args : `Namespace`
        The parsed command line arguments.
    """
    # Now parse the arguments
    args = parser.parse_args()

    if args.neo_pass is None:
        args.neo_pass = getpass.getpass(prompt='Input Neo4j password > ')
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
