"""Determine the interaction path distances between proteins within drug
targets and a set of input proteins
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
from bio_misc.reactome import common as rcommon
from tqdm import tqdm
import argparse
import getpass
import csv
import os
# import pprint as pp


_SCRIPT_NAME = 'bm-pairwise-interact'
"""The name of the script that is implemented in this module (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_pairwise_interactions(infile, reactome_user, reactome_pass,
                              reactome_host, reactome_port, outfile=None,
                              verbose=False, processes=1, tmp_dir=None,
                              prot_id=con.UNIPROT_PROT_ID,
                              comment=con.DEFAULT_COMMENT,
                              delimiter=con.DEFAULT_DELIMITER,
                              encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings.
    """
    unique_prot = utils.get_unique_entries(
        infile, prot_id, comment=comment,
        delimiter=delimiter, encoding=encoding
    )
    # unique_prot = unique_prot[:10]
    ncomparisons = len(unique_prot) * len(unique_prot)

    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        writer.writerow(
            ["source_uniprot_id", "target_uniprot_id", "path_length"]
        )
        for row in tqdm(
                rcommon.yield_path_dist(
                    reactome_user, reactome_pass, reactome_host,
                    reactome_port, unique_prot, unique_prot,
                    processes=processes
                ),
                desc="[progress] fetching path lengths: ",
                unit=" comparisons",
                disable=not verbose,
                total=ncomparisons
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Make sure the password does not appear on screen, (a bit of a hack)
    neo_password = args.neo_pass
    args.neo_pass = '*****'

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    get_pairwise_interactions(
        args.infile, args.neo_user, neo_password, args.neo_host,
        args.neo_port, outfile=args.outfile, delimiter=args.delimiter,
        verbose=args.verbose, encoding=args.encoding, processes=args.processes,
        comment=args.comment, tmp_dir=args.tmp_dir, prot_id=args.prot_id
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Query the pairwise protein-protein interaction path"
        "distances between input proteins"
    )
    arguments.add_input_output(
        parser, infile_optional=False, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, ensembl_id=False)
    arguments.add_neo4j_arguments(parser)
    arguments.add_multitask_args(parser)
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()

    if args.neo_pass is None:
        args.neo_pass = getpass.getpass(prompt='Input Neo4j password > ')
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
