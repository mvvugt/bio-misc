"""Some common classes and functions for the IPD sub-package
"""
from bio_misc.common import (
    arguments,
)
from bio_misc.ipd import readers
from bio_misc.format import uni_id
from numba import jit
import pandas as pd
import numpy as np
import numpy.ma as ma
import math


# _UNI_ID_COL = 'uni_id'
# """The name for the universal ID column in the variant dict (`str`)
# """
# _IDX_COL = 'idx'
# """The name for the index position column in the variant dict (`str`)
# """
# _CHR_NAME_COL = 'chr_name'
# """The name for the universal ID column in the variant dict (`str`)
# """
# _START_POS_COL = 'start_pos'
# """The name for the universal ID column in the variant dict (`str`)
# """
# _END_POS_COL = 'end_pos'
# """The name for the universal ID column in the variant dict (`str`)
# """
# _EFFECT_ALLELE_COL = 'effect_allele'
# """The name for the universal ID column in the variant dict (`str`)
# """
# _OTHER_ALLELE_COL = 'other_allele'
# """The name for the universal ID column in the variant dict (`str`)
# """
# _IS_UNKNOWN_COL = 'is_unknown'
# """The name for the is unknown column in the variant dict. This is where a
# variant has no LD data defined in the reference, so it can't be determined if
# it is independent (`str`)
# """
_COHORT_READ_CLASS = 'read_class'
"""The cohort read class key (`str`)
"""
_COHORT_PROVIDER = 'provider'
"""The cohort provider object key (`str`)
"""
_COHORT_SAMPLES = 'samples'
"""The cohort sample ids key (`str`)
"""
_COHORT_READER = 'reader'
"""The cohort reader object key (`str`)
"""
_COHORT_KWARGS = 'kwargs'
"""The cohort reader kwargs key (`str`)
"""
_COHORT_ZERO_FUNC = 'zero_func'
"""The cohort reader kwargs key (`str`)
"""
_COHORT_SHAPE = 'shape'
"""The key containing shape of the cohort of all alleles sounds for all
 requested samples were placed into an array (`str`)
"""
_COHORT_NAME = 'cohort_name'
"""The key name of the cohort (`str`)
"""

SPECIES = 'human'
"""Default species in the config file
"""
FILE_FORMATS = ['bgen', 'vcf', 'bcf']
"""Allowed formats, please note, only bgens are supported at the moment
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Cohort(object):
    """A basic representation of a cohort.

    Parameters
    ----------
    name : `str`
        The name of the cohort.
    species : `str`
        The species of the cohort.
    assembly : `str`
        The genome assembly for the cohort.
    file_format : `str`
        The file format for the cohort.
    leading_zero : `bool`, optional, default: `False`
        Should leading zeros be added onto the autosomes of the cohort.
    samples : (`list` of `str`) or `str`, optional, default: `NoneType`
        Either a list of sample IDs to use from the cohort, or the name of a
        sample section. If `NoneType` then all samples will be used.
    **kwargs
        Keyword arguments that are passed to the provider reader class that
        is created by the cohort.
    """
    _COHORT_IDX = 0
    """A counter for the Cohort, so every cohort that is created will get
    a unique ID (`int`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, config, name, species, assembly, file_format,
                 leading_zero=False, samples=None, **kwargs):
        self.cohort_idx = Cohort._COHORT_IDX
        Cohort._COHORT_IDX += 1
        self.config = config
        self.name = name
        self.species = species
        self.assembly = assembly
        self.file_format = file_format
        self.leading_zero = leading_zero
        self._reader_kwargs = kwargs

        # Set the bitwise value for the cohort, this is a mechanism by which
        # we can track cohorts in a session
        self.bits = 1 << self.cohort_idx

        # Will be set when opened
        self.reader = None
        self.reader_class = None
        self.zero_func = None
        self.unzero_func = None

        self._sample_section = None
        self._init_samples = None
        self.samples = []
        self._init_samples = self.samples.copy()

        self.samples = None
        if isinstance(samples, str):
            self.samples = self.config.get_samples(samples)
        elif isinstance(samples, (list, tuple)):
            self.samples = samples.copy()

        if self.samples is not None and len(self.samples) == 0:
            self.samples = None

        self._is_open = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        atts = []
        for i in ['cohort_idx', 'name', 'assembly', 'file_format',
                  'leading_zero']:
            atts.append(f"{i}={getattr(self, i)}")

        if len(self.samples) == 0:
            atts.append("samples=NA")
        else:
            atts.append(f"samples={len(self.samples)}")
        return "<{0}({1})>".format(
            self.__class__.__name__,
            ", ".join(atts)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Initialise the cohort with the reader and zero/unzero functions.

        Returns
        -------
        reader : `bio_misc.ipd.readers.VcfProviderReader` or
        `bio_misc.ipd.readers.BgenProviderReader`
            The opened reader object, this is also available from
            Cohort.reader.
        """
        if self._is_open is True:
            raise RuntimeError("cohort is already open")

        self.zero_func, self.unzero_func = set_zero_func(
            self.leading_zero
        )

        if self.file_format == 'bgen':
            # For some reason bgen reader is defaulting to verbose True
            # which we want to turn off if the user has not explicitly set
            # it
            if 'verbose' not in self._reader_kwargs:
                self._reader_kwargs['verbose'] = False
            self.reader_class = readers.BgenProviderReader
        elif self.file_format == 'vcf':
            self.reader_class = readers.VcfProviderReader
        elif self.file_format == 'bcf':
            self.reader_class = readers.VcfProviderReader
        else:
            raise NotImplementedError(
                f"format not available: {self.file_format}"
            )

        provider = self.config.get_cohort_region_provider(
            self.species, self.assembly, self.name, self.file_format
        )

        # Open all the cohorts
        self.reader = self.reader_class(
            provider,
            sample_ids=self.samples,
            **self._reader_kwargs
        )
        self.reader.open()
        self._is_open = True
        return self.reader

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the cohort and reader and unset zero/unzero functions.
        """
        try:
            self.reader.close()
        except AttributeError:
            # It has probably been closed by another processes
            if self.reader is not None:
                raise

        # Unset
        self.reader = None
        self.reader_class = None
        self.zero_func = None
        self.unzero_func = None
        self._is_open = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def is_open(self):
        """Return the open state of the cohort (`bool`)
        """
        return self._is_open


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Variant(object):
    """A representation of a variant.

    Parameters
    ----------
    chr_name : `str`
        The chromosome name.
    start_pos : `int`
        The start position (in bp).
    effect_allele : `str`
        The effect allele for the variant.
    other_allele : `str`
        The non-effect allele for the variant.
    var_id : `str`, optional, default: `NoneType`
        Any variant identifier for the variant.
    p_value : `float`, optional, default: `NoneType`
        Any GWAS association p-value associated with the variant.
    p_value_logged : `bool`, optional, default: `False`
        A flag to indicate if the p-value for the variant is -log10
        transformed.
    """
    _VARIANT_IDX = 0
    """A counter for the Variant, so every variant that is created will get
    a unique ID (`int`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chr_name, start_pos, effect_allele, other_allele,
                 var_id=None, pvalue=None, pvalue_logged=False):
        self.variant_idx = Variant._VARIANT_IDX
        Variant._VARIANT_IDX += 1
        self.chr_name = chr_name
        self.start_pos = int(start_pos)
        self.effect_allele = effect_allele
        self.other_allele = other_allele.split(",")[0]
        self.end_pos = self.start_pos + len(self.effect_allele) - 1
        self.var_id = var_id
        self.pvalue = pvalue
        self.pvalue_logged = pvalue_logged

        # This will hold cohort bitwise integers for which this this
        # variant is unknown to
        # TODO: change this to cohort IDs if possible as I am not sure what
        #  the point of storing bits in a set is
        self.is_unknown = set()
        self.is_unknown_bits = 0

        self.uni_id = uni_id.make_uni_id(
            self.chr_name, self.start_pos,
            self.effect_allele, self.other_allele
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        atts = []
        for i in self.print_atts():
            atts.append(f"{i}={getattr(self, i)}")
        return "<{0}({1})>".format(
            self.__class__.__name__,
            ", ".join(atts)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def print_atts(self):
        """Object attribute names to print using __repr__
        """
        return ['variant_idx', 'chr_name', 'start_pos', 'effect_allele',
                'other_allele', 'uni_id', 'is_unknown']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_unknown(self, cohort):
        """Add a cohort for which this variant is unknown.

        Parameters
        ----------
        cohort : `bio_misc.ipd.common.Cohort`
            The cohort to add.
        """
        self.is_unknown.add(cohort.bits)
        self.is_unknown_bits = self.is_unknown_bits | cohort.bits

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def remove_unknown(self, cohort):
        """Remove a cohort for which this variant is now known.

        Parameters
        ----------
        cohort : `bio_misc.ipd.common.Cohort`
            The cohort to remove.
        """
        self.is_unknown.remove(cohort.bits)
        self.is_unknown_bits = self.is_unknown_bits ^ cohort.bits

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_unknown(self, bits):
        """Create an is unknown set by extracting data bits.

        Parameters
        ----------
        bits : `int`
            The bits to extract from.
        """
        self.is_unknown_bits = bits
        if bits == 0:
            self.is_unknown = set([])
            return

        try:
            max_bits = math.floor(math.log(bits)/math.log(2))
        except ValueError:
            print(bits)
            raise
        self.is_unknown = set(
            [i for i in range(max_bits+1) if bool(bits & (1 << i)) is True]
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClumpVariant(Variant):
    """A representation of a variant used in a clumping process.

    Parameters
    ----------
    *args
        Arguments to `bio_misc.ipd.common.Variant`
    is_clumped : `bool`, optional, default: `False`
        An flag to indicate if the variant has been clumed.
    is_index : `bool`, optional, default: `False`
        An flag to indicate if the variant is in index variant.
    **kwargs
        Keyword arguments to `bio_misc.ipd.common.Variant`
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, is_clumped=False, is_index=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_clumped = is_clumped
        self.is_index = is_index

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def print_atts(self):
        """Object attribute names to print using __repr__
        """
        return super().print_atts() + ['is_clumped', 'is_index', 'pvalue']


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseRunCorrMatrix(object):
    """Base class for running a correlation matrix, do not use directly

    Parameters
    ----------
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The data type for the resulting correlation matrix. The accepted
        choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
        set according to the size of the matrix, available memory and the
        required precision. Lower values will save a lot of memory if
        memory is tight.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, cohorts, dtype=np.float64):
        self.cohorts = cohorts
        self.dtype = dtype

        # Make sure the cohorts have been defined
        have_cohorts(cohorts)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_empty_corr_matrix(cls, invars, dtype=np.float64):
        """Get an empty correlation coefficient matrix.

        Parameters
        ----------
        invars : `list`
            The input variables, the length of this will determine the output
            matrix size.
        dtype : `numpy.float`, optional, default: `numpy.float64`
            The data type for the resulting correlation matrix. The accepted
            choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
            set according to the size of the matrix, available memory and the
            required precision. Lower values will save a lot of memory if
            memory is tight.

        Returns
        -------
        empty_matrix : `numpy.array`
            A numpy 2d array of shape (len(invars), len(invars)) that is full
            of ``np.nan``.
        """
        return np.full(
            (len(invars), len(invars)),
            np.nan,
            dtype=dtype
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_empty_nobs_matrix(cls, invars, dtype=np.uint32):
        """Get an empty number of observations matrix.

        Parameters
        ----------
        invars : `list`
            The input variables, the length of this will determine the output
            matrix size.
        dtype : `numpy.int`, optional, default: `numpy.uint32`
            The data type for the resulting number of observations matrix.

        Returns
        -------
        empty_matrix : `numpy.array`
            A numpy 2d array of shape (len(invars), len(invars)) that is
            initialised to zero (``0``).
        """
        return np.zeros((len(invars), len(invars)), dtype=dtype)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run(self, invars, verbose=False, **kwargs):
        """Generate a correlation matrix across all input cohorts.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        # Make sure the variants that have been supplied are unique
        invars = unique_variants(invars)

        # Now initialise using our first higher priority cohort
        corr_matrix, nobs_matrix = self.run_cohort(
            invars, self.cohorts[0], verbose=verbose, **kwargs
        )

        # Follow up using any secondary cohorts to fill the gaps after the
        # first cohort pass
        for i in range(1, len(self.cohorts)):
            # Get the row/column locations of missing pairwise comparisons
            # these are the only ones that will be updated
            a, b = np.where(np.isnan(corr_matrix))

            # Get the index positions of the variants involved in the
            # missing data comparisons
            missing_vars = np.unique(np.hstack((a, b)))
            test_vars = [invars[i] for i in missing_vars]

            # If there are none then we do not need to do anything else
            if len(test_vars) == 0:
                break

            # Now fill any values for the subset of variants we are
            # interested in
            cohort_corr_matrix, cohort_nobs_matrix = self.run_cohort(
                test_vars,
                self.cohorts[i],
                verbose=verbose,
                **kwargs
            )

            # Now create a mapping between the infill matrix and the main
            # matrix so the new data can be updated
            # credit here: https://stackoverflow.com/questions/16992713/
            coord_map = dict([(i, idx) for idx, i in enumerate(missing_vars)])
            chrt_a = np.vectorize(coord_map.__getitem__)(a)
            chrt_b = np.vectorize(coord_map.__getitem__)(b)

            # Add the values from the current cohort to the missing positions
            # of the master matrix
            corr_matrix[a, b] = cohort_corr_matrix[chrt_a, chrt_b]
            nobs_matrix[a, b] = cohort_nobs_matrix[chrt_a, chrt_b]
        return corr_matrix, nobs_matrix, invars

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run_cohort(self, invars, cohort, verbose=False, **kwargs):
        """Generate a correlation matrix for a single cohort.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        cohort : `bio_misc.ipd.common.Cohort`
            Description of the cohort that will be used for as the source data
            for the correlation matrix.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        # Create the master correlation matrix and the number of
        # observations matrix
        corr_matrix = self.get_empty_corr_matrix(
            invars, dtype=self.dtype
        )
        nobs_matrix = self.get_empty_nobs_matrix(
            invars
        )
        return corr_matrix, nobs_matrix


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_combs(rows, batch_size, processes):
    """
    """
    combs = []
    for r in range(0, rows, batch_size):
        for c in range(0, r+batch_size, batch_size):
            combs.append(((r, r+batch_size), (c, c+batch_size)))
    return set_batches(
        combs, math.ceil(len(combs) / processes)
    )


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def get_process_params(rows, batch_size, processes):
#     """
#     """
#     rc_batch = math.ceil(rows / batch_size)
#     # print(f"rc_batch {rc_batch}")
#     nbatches = ((rc_batch * rc_batch) / 2) + (rc_batch / 2)
#     # print(f"nbatches {nbatches}")

#     batch_per_proc = int(math.floor(nbatches / processes))
#     # print(f"batch_per_proc {batch_per_proc}")

#     proc_atts = []
#     ntasks = [0] * processes
#     # print(f"ntasks {ntasks}")
#     if batch_per_proc >= 1:
#         ntasks = [batch_per_proc] * processes

#     remainder = int(nbatches % processes)
#     # print(f"remainder {remainder}")
#     remainder_per_proc = math.ceil(remainder / processes)
#     # print(f"rremainder_per_proc {remainder_per_proc}")
#     for idx, i in enumerate([remainder_per_proc] * remainder):
#         # print(i)
#         ntasks[idx] += i
#     # print(ntasks)
#     # target = ntasks.pop(0)
#     target = ntasks.pop(0)
#     its = 0
#     r_start = 0
#     c_start = 0
#     proc_atts.append((r_start, c_start, target))
#     stop = False
#     for r in range(0, rows, batch_size):
#         # print(f"r={r} - its={its}")
#         for c in range(0, r+batch_size, batch_size):
#             # print(f"c={c} - its={its}")
#             if its > target-1 and target > 0:
#                 # print(f"storing, {r}, {c}, {its}")
#                 # r_start = r
#                 # c_start = c
#                 try:
#                     target = ntasks.pop(0)
#                 except IndexError:
#                     stop = True
#                     break
#                 proc_atts.append((r, c, target))

#                 # r_start = r
#                 its = 0
#             its += 1
#         if stop is True:
#             break
#     return proc_atts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def batch_corrcoef(a, b):
    mask = np.logical_not((a.mask | b.mask))
    am = a - a.mean(axis=1)[:, None]
    bm = b - b.mean(axis=1)[:, None]
    covar = np.sum(am * bm, axis=1)/np.sum(mask, axis=1)

    var_prod = []
    for idx in range(mask.shape[0]):
        if not np.all(a[idx].mask) and not np.all(b[idx].mask):
            avar = a[idx][mask[idx]].var()
            bvar = b[idx][mask[idx]].var()
            var_prod.append(avar * bvar)
        else:
            var_prod.append(np.nan)
    return covar/np.sqrt(var_prod), np.sum(mask, axis=1)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def block_corrcoef(a, b):
    """Generate a block index pairwise correlation coefficients between
    elements of two numpy 2d masked arrays.

    Parameters
    ----------
    a : `numpy.ma.array`
        A two dimensional array
    b : `numpy.ma.array`
        A two dimensional array

    Returns
    -------
    corrcoef : `numpy.ma.array`
        The pairwise correlations between all rows in a and all rows in b.
        The array will have the shape (a.shape[0], b.shape[0]).
    nobs : `numpy.ma.array`
        The pairwise number of observations  between all rows in a and all rows
        in b. The array will have the shape (a.shape[0], b.shape[0]). This is
        useful as the user can see if some coefficients are based on very few
        observations.

    Raises
    ------
    ValueError
        If the masks have not been defined correctly, the masks should be
        boolean and have the same dimensions as the input array.

    Notes
    -----
    This differs from ``numpy.ma.corrcoef`` in several key ways. The
    ``numpy.ma`` implementation is very slow in cases where there is missing
    data and it also will stack ``a`` and ``b`` before doing the correlations.
    The numpy implementation can also handle complex numbers (I think), where
    as this can't. Also, this only works row wise where the numpy version can
    do either row-wise or column-wise.
    """
    try:
        # Get the yes/no observations for each of the inputs
        aobs = np.logical_not(a.mask).astype(int)
        bobs = np.logical_not(b.mask).astype(int)
    except AttributeError:
        # For non-masked arrays
        # TODO: Make this the default
        aobs = np.logical_not(np.isnan(a)).astype(int)
        bobs = np.logical_not(np.isnan(b)).astype(int)

    # check that the masks have been set correctly
    if aobs.shape != a.shape or bobs.shape != b.shape:
        raise ValueError(
            "masks are different shape to data, "
            "try numpy.ma.masked_invalid on data"
        )

    # Subtract the means row-wise
    am = a - a.mean(axis=1)[:, None]
    bm = b - b.mean(axis=1)[:, None]

    # Get the number of observations in each pair
    denom = np.dot(aobs, bobs.T) * 1.

    # If there is no missing data then we can go for a shortcut and not compute
    # the variance based on individual pairs
    if np.all(aobs) and np.all(bobs):
        am /= np.sqrt(np.sum(am*am, axis=1))[:, None]
        bm /= np.sqrt(np.sum(bm*bm, axis=1))[:, None]
        res = ma.dot(am, bm.T)
        res[res.mask] = np.nan
        return res, denom

    # If there is missing data then generate a pairwise covariance matrix
    covar = ma.dot(am, bm.T)/denom

    # TODO: get the variance rows wise and then adjust for combinations that
    #  do not match the total
    # Calculate the variance product for each pair and divide by stdev
    _denom = _adj_variance(a, b)
    # for i in range(nrow):
    #     for j in range(ncol):
    #         _x = ma.mask_cols(ma.vstack((a[i], b[j]))).var(axis=1)
    #         # print(f"***** {i},{j} *****")
    #         # print(_x)
    #         _denom[i, j] = ma.sqrt(ma.multiply.reduce(_x))
    # Get the cc and also set masked elements to nans
    x = covar/_denom
    x[x.mask] = np.nan
    return x, denom


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@jit(nopython=True)
# @jit
def _adj_variance(a, b):
    nrow = a.shape[0]
    ncol = b.shape[0]
    _denom = np.full((nrow, ncol), np.nan)
    for i in range(nrow):
        for j in range(ncol):
            s = ~(np.isnan(a[i]) | np.isnan(b[j]))
            v = np.nan
            if not np.all(np.logical_not(s)):
                v = np.sqrt(
                    np.multiply(a[i][s].var(), b[j][s].var())
                )
            _denom[i, j] = v
    return _denom


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_data_frame(matrix, variants):
    """Convert a 2D numpy array into a ``pandas.DataFrame``.

    Parameters
    ----------
    matrix : `numpy.array`
        A 2D numpy array must have the shape ``(len(variants),
        len(variants))``.
    variants : `list` of `dict`
        The genetic variants to act as row/column indexes. The order of the
        variants must represent their order in the matrix. Each dict must have
        the key ``uni_id``.

    Returns
    -------
    data_frame : `pandas.DataFrame`
        The matrix converted into a ``pandas.DataFrame``. This will have the
        shape ``(len(variants)), len(variants))``. The row and column
        indexes will be ``uni_id``, that is the chromosome and start position
        and alleles in sort order all concatinated with ``_``.

    """
    index_labels = [i.uni_id for i in variants]
    return pd.DataFrame(
        matrix,
        index=index_labels,
        columns=index_labels
    )


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def get_batch_nobs(has_count):
#     """Get a matrix of number of observations used in each pairwise correlation.

#     Parameters
#     ----------
#     has_count : `list` of `np.array`
#         The list will have length equal to the number of variants that have
#         been compared in the batch, with each sub numpy array representing a
#         variant and being a boolean array indicating if an individual had an
#         allele count for the variant.

#     Returns
#     -------
#     nobs : `numpy.array`
#         A 2D array with shape ``(len(has_count), len(has_count))`` with the
#         number of observations for each pairwise variant comparison.
#     """
#     nobs = np.array([np.sum(i & j) for i in has_count for j in has_count])
#     nobs.shape = (len(has_count), len(has_count))
#     return nobs


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_variant_dosage(reader, chr_name, start_pos, effect_allele,
                       other_allele):
    """Get the variant allele counts/dosage for a variant.

    Parameters
    ----------
    reader : `bio_misc.ipd.common.VcfProviderReader` or
    `bio_misc.ipd.common.BgenProviderReader`
        The reader class to use to extract the variant counts/dosages.
    chr_name : `str`
        The chromosome name to get.
    start_pos : `int`
        The start position to get.
    effect_allele : `str`
        The effect allele/reference allele.
    other_allele : `str`
        The non-effect/alternate allele.

    Returns
    -------
    allele_count : `numpy.MaskedArray`
        A float64 numpy array. Missing values are encoded as ``numpy.nan`` and
        are masked.
    flipped : `bool`
        An indicator if the allele counts have been flipped to match the
        requested alleles. For example, if the ``effect_allele`` matches the
        ALT allele in the reference then the allele counts are flipped so they
        are the counts of the ``effect_allele``.

    Raises
    ------
    IndexError : If there are no entries for the variant.

    Notes
    -----
    If > 1 match is found in the reference population only the first one is
    returned.
    """
    variants = []
    for dosage, flipped in reader.get_variant_dosage(chr_name, start_pos,
                                                     effect_allele,
                                                     other_allele):
        variants.append((dosage, flipped))
    return variants[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_variant_batch_dosage(cohort, batch, nsamples=None):
    """Get the allele counts/dosages for all the variants in the ``batch``.
    Missing data is returned as all ``numpy.nan``.

    Parameters
    ----------
    reader : `bio_misc.ipd.common.VcfProviderReader` or
    `bio_misc.ipd.common.BgenProviderReader`
        The reader class to use to extract the variant counts/dosages.
    zero_func : `function`
        A function that will either pass-though r add a leading zero to the
        chromosome
    batch : `list` of `dict`
        The genetic variants to extract using the reader. Each dict must have
        the keys and value types ``chr_name``, ``start_pos``,
        ``effect_allele``, ``other_allele``, ``is_unknown``, ``idx``.

    Returns
    -------
    variants : `list` of `np.MaskedArray`
        The extracted allele counts/dosages, this may be shorted than the input
        batch if not all variants have been located.
    """
    nsamples = nsamples or len(cohort.reader.sample_order)
    variants = []
    for i in batch:
        try:
            dosage, flipped = get_variant_dosage(
                cohort.reader,
                cohort.zero_func(i.chr_name),
                i.start_pos,
                i.effect_allele,
                i.other_allele
            )
            variants.append(dosage)
        except IndexError:
            variants.append(
                ma.masked_invalid(
                    np.full(nsamples, np.nan)
                )
            )
    return ma.masked_invalid(variants)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_batches(invars, batch_size=2):
    """Convert the input variants into a list of variant batches with max
    size == ``batch_size``.

    Parameters
    ----------
    invars : `list` of `dict`
        The input genetic variants.
    batch_size : `int`, optional, default: `2`
        The size of the sub array batches.

    Returns
    -------
    batches : `list` of `list` of `dict`
        The number of batches is ``len(batches)``, the batch size is
        ``len(batches[IDX])``.
    """
    batches = []
    for i in range(0, len(invars), batch_size):
        batches.append(invars[i:i+batch_size])
    return batches


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_zero_func(zero):
    """Get zero/unzero function function based of the ``zero`` parameter.

    Parameters
    ----------
    zero : `bool`
        Is a leading zero function required?

    Returns
    -------
    zero_func : `function`
        If ``zero`` is ``True`` this will be a function that adds a leading 0
        to single value chromosome names. Otherwise a dummy pass-through
        function is returned.
    unzero_func : `function`
        If ``zero`` is ``True`` this will be a function that removes leading 0s
        from chromosome names. Otherwise a dummy pass-through function is
        returned.
    """
    if zero is True:
        return add_leading_zero, remove_leading_zero
    return dummy, dummy


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dummy(value):
    """A dummy pass-through function that returns the value it is given.

    Parameters
    ----------
    value : `Any`
        A value to be passed-through.

    Returns
    -------
    value : `Any`
        The value that was passed to the function.
    """
    return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_leading_zero(value):
    """Add a leading zero to a value if it has length < 2.

    Parameters
    ----------
    value : `str`
        The value to potentially add a leading zero to.

    Returns
    -------
    value : `str`
        The value with a guaranteed length of at least two potentially with a
        leading zero add to the start.

    Notes
    -----
    Currently this does not distinguish between "integer" chromosome names and
    `string` ones, So ``X`` will become ``0X``, this may not be what is needed.
    """
    return value.zfill(2)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def remove_leading_zero(value):
    """Remove any leading zeros from a value.

    Parameters
    ----------
    value : `str`
        The value to potentially remove leading zeros from.

    Returns
    -------
    value : `str`
        The value with a guaranteed no leading zeros.
    """
    return value.lstrip("0")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def verbose_disable(verbose):
    """A helper function to determine if there is enough verbosity to enable a
    ``tqdm`` progress monitor.

    Parameters
    ----------
    verbose : `bool` or `int`
        The verbose value to test.

    Returns
    -------
    disable : `bool`
        The disable value for ``tqdm``.
    """
    if int(verbose) < 2:
        return True
    return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def have_cohorts(cohorts):
    """Make sure there are some cohorts and error out if not.

    Parameter
    ---------
    cohorts : `list` of `dict`
        Descriptions of the cohorts that will be used for clumping.

    Raises
    ------
    ValueError
        If the length of cohorts is 0
    """
    if len(cohorts) == 0:
        raise ValueError("no cohorts to process")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def unique_variants(invars):
    """Ensure the input variants are unique and their index position is set.

    Parameters
    ----------
    invars : `list` of `bio_misc.ipd.common.Variant`
        The input genetic variants.

    Returns
    -------
    unique_invars : `list` of `bio_misc.ipd.common.Variant`
        Unique input genetic variants.
    """
    # Loop through and get the unique variants based on uni_id
    unique_vars = []
    prev_uni_id = ""
    for i in sorted(invars, key=lambda x: x.uni_id):
        if i.uni_id != prev_uni_id:
            unique_vars.append(i)
        prev_uni_id = i.uni_id

    # Place back into the original order
    unique_vars = sorted(unique_vars, key=lambda x: x.variant_idx)
    return unique_vars


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cohort_data(args, config):
    """Parse all the cohort related arguments.

    Parameters
    ----------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments ``samples``,
        ``cohorts``, ``zero`` which should be lists.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object with the cohorts attribute being a
        ``list`` of ``bio_misc.ipd.common.Cohort`` where each element
        represents a different cohort.
    """
    if len(args.samples) > 0 and len(args.samples) != len(args.cohorts):
        raise ValueError("if sample files are provided then they should be"
                         " the same length as the number of cohorts")

    if len(args.format) > 0 and len(args.format) != len(args.cohorts):
        raise ValueError("if formats are provided then they should be the same"
                         " length as the number of cohorts")

    cohorts = []
    for idx, i in enumerate(args.cohorts):
        open_kwargs = dict()
        samples = []
        if args.samples[idx] != '-':
            try:
                with open(args.samples[idx], 'rt') as sfile:
                    samples = [i.strip() for i in sfile]
            except FileNotFoundError:
                samples = args.samples[idx]

        # print(args.format[idx])
        if args.format[idx] == 'bgen':
            open_kwargs['verbose'] = False
        cohort = Cohort(
            config, i, args.species, args.assembly,
            args.format[idx],
            leading_zero=i in args.zero,
            samples=samples,
            **open_kwargs
        )
        cohorts.append(cohort)
    args.cohorts = cohorts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args(parser, infile_optional=True, outfile_optional=True,
                  end_pos=False):
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser.add_argument(
        'cohort', type=str,
        help="The name for the cohort you want to extract from"
    )
    arguments.add_input_output(
        parser,
        infile_optional=infile_optional,
        outfile_optional=outfile_optional
    )
    arguments.add_file_arguments(parser)
    arguments.add_genomic_coords_arguments(parser, end_pos=end_pos)
    parser.add_argument(
        '--config', type=str,
        help="The path to the genomic-config file, that lists all the"
        " reference populations to use for LD, if not supplied will try to"
        " ID it in ``GENOMIC_CONFIG`` environment variable  or "
        "~/.genomic_data.ini"
    )
    parser.add_argument(
        '--samples', type=str,
        help="The path to a sample ID file (one per line), to use as a"
        "reference for LD if not supplied will extract all the samples"
    )
    parser.add_argument(
        '--format', type=str, choices=FILE_FORMATS, default=FILE_FORMATS[0],
        help="The file formats for the reference genome files to extract "
        "from. At the moment only ``bgen`` is supported"
    )
    parser.add_argument(
        '--assembly', type=str, default="GRCh38",
        help="The assembly for the reference cohort"
    )
    parser.add_argument(
        '-v', '--verbose', action="count",
        help="Output progress, use -vv for progress monitors"
    )
    return parser
