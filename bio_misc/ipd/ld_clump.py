"""An LD clumping program. The input GWAS file should have a
chromosome, start position, effect allele, other allele columns. The
values for these can also be supplied from the command line. This is low
memory and only rows to be clumped will be held in memory at one time.

This will only clump within a chromosome and not across chromosomes. It
also relies on genomic-config package being present along with a valid
config file.
"""
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.ipd import common, ld_factory, ld_handlers
from bio_misc.transforms.log_pvalue import log_transform
from bio_misc.common import (
    utils,
    constants as con,
    arguments,
    log
)
from tqdm import tqdm
from genomic_config import genomic_config
import multiprocessing as mp
import numpy.ma as ma
import shutil
import stdopen
import numpy as np
import argparse
import sys
import os
import csv
import gzip
import warnings
import pprint as pp


_DESC = __doc__
"""The program description also used for argparse. (`str`)
"""
_PROG_NAME = 'bm-ld-clumper'
"""The name of the script that is output when verbose is True (`str`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseClumpRun(object):
    """Base class to run variant clumping over a dataset. Do not use directly.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    handler : `bio_misc.ipd.ld_clump.FileClumpHandler` or
    `bio_misc.ipd.ld_clump.DbClumpHandler`
        A clump handler class must inherit from
        `bio_misc.ipd.ld_clump._BaseClumpHandler`, must be unopened.
    factory : `bio_misc.ipd.ld_factory.LDFactory`
        Provider for pair-wise LD computations, must be unopened..
    *args
        Any arguments to be passed through to the handler.
    rsquared : `float`, optional, default: `0.01`
        The clumping threshold, and variants that have a pairwise LD value >=
        to this are not deemed independent.
    **kwargs
        Any keyword arguments to pass through to the ``handler``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, handler, factory, *args, rsquared=0.01,
                 **kwargs):
        self._args = args
        self.rsquared = rsquared
        self._kwargs = kwargs
        self.handler = handler
        self.factory = factory

        self.is_open = False
        # self._pbar = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Enter the context manager, calls ``open()``.
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Exit the context manager, calls ``close()``.
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the runner, this will call the handler to load the dataset.
        """
        if self.is_open is True:
            raise RuntimeError("already open")
        # Ensure the handler is aware of the cohorts so it can skip completely
        # missing variants
        self.handler.set_missing_value(self.factory.cohorts)
        self.handler.open()
        self.factory.open()
        self.is_open = True
        self.is_loaded = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the runner, this will also close the handler.
        """
        self.handler.close()
        self.factory.close()
        self.is_loaded = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def start_progress(self, verbose=False):
        """
        """
        self.stop_progress()
        self._pbar = self._get_progress_bar(verbose=verbose)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def stop_progress(self):
        """
        """
        try:
            self._pbar.close()
        except AttributeError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update_progress(self, n):
        """
        """
        self._pbar.update(n)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run(self, outfile, verbose=False, index_only=False, use_tmp=True):
        """Run clumping and output to a file.

        Parameters
        ----------
        outfile : `str`, or `NoneType`
            The output file, if ``NoneType`` then output will be via STDOUT, if
            the out file has a ``.gz`` extension then gzip will be used.
        verbose : `bool`, optional, default: `False`
            Output a progress monitor when clumping.
        index_only : `bool`, optional, default: `False`
            Only output index variants (not implemented)
        use_tmp : `bool`, optional, default: `False`
            Do not output directly to ``outfile`` but use a temp file and then
            copy on completion.

        Returns
        -------
        outfile : `str`
            The full path to the output file.

        Notes
        -----
        The temp location and delimiter are taken from the handler.
        """
        # run the clumping
        self._run(verbose, index_only)

        # Set the open method according to the file extension
        open_method = open
        if outfile is not None and outfile.endswith(".gz"):
            open_method = gzip.open

        with stdopen.open(outfile, method=open_method, mode="wt",
                          use_tmp=use_tmp, tmpdir=self.handler.tmpdir) as out:
            # The writer will output the original file contents and the clump
            # columns
            writer = csv.DictWriter(
                out, self.handler.inheader + self.handler.CLUMP_COLS,
                delimiter=self.handler.delimiter,
                extrasaction='ignore'
            )
            writer.writeheader()
            for row in self.handler.output():
                writer.writerow(row)

        return os.path.realpath(outfile)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self):
        """Yield the LD clump data.

        Yields
        ------
        row : `dict`
            Each row represents the contents of the input file and additional
            fields for the clumping these are. ``variant_idx`` (a zero-based
            counter of the variant), ``is_index`` (0=no, 1=yes) ``is_clumped``
            (0=no, 1=yes), ``is_unknown`` (0=no, 1=yes).
        """
        self._run(False, False)
        for row in self.handler.output():
            yield row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _run(self, verbose, index_only):
        """Run LD clumping and write to an output file.

        Parameters
        ----------
        outfile : `str`
            The path to the output file.
        index_only : `bool`, optional, default: `False`
            Only output index variants.
        """
        self.start_progress(verbose=verbose)

        try:
            while True:
                all_vars = list()
                try:
                    idx = self.handler.next_index()
                except IndexError:
                    # clumping finished
                    break

                cohort_idx = self.factory.has_variant(idx)
                if len(cohort_idx) == 0:
                    # idx.is_unknown = len(self.factory.cohorts)
                    self.handler.update([idx])
                    self.update_progress(1)
                    continue

                idx.is_index = 1
                all_vars.append(idx)

                clump_batch = self.handler.get_clump_batch(idx)

                try:
                    pairs, unknown = self.factory.get_one_vs_many(
                        idx, clump_batch, cohorts=cohort_idx
                    )
                except RuntimeError:
                    print(idx)
                    pp.pprint(cohort_idx)
                    raise

#                 pp.pprint(pairs)
                for i, t, ld in pairs:
                    if ld >= self.rsquared:
                        t.is_clumped = 1
                        all_vars.append(t)

                part_unknown = 0
                for u in unknown:
                    part_unknown += (
                        u.is_unknown_bits != self.handler.missing_value
                    )
                    all_vars.append(u)
                self.handler.update(all_vars)
                self.update_progress(len(all_vars) - part_unknown)
        finally:
            self.stop_progress()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_progress_bar(self, verbose=False):
        """Initialise a progress monitor according to the verbose.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Should the progress monitor be visible.

        Returns
        -------
        pbar : `tqdm`
            A progress bar object.
        """
        return tqdm(total=self.handler.get_variant_count(),
                    desc="[info] running LD clumping...",
                    unit=" rows",
                    disable=not verbose)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SingleClumpRun(_BaseClumpRun):
    """Perform variant clumping using a single process.
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ProcessRun(_BaseClumpRun):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def start_progress(self, verbose=False):
        """
        """
        self.verbose = verbose

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def stop_progress(self):
        """
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update_progress(self, n):
        """
        """
        if bool(self.verbose) is True:
            self.queue.put(n)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run(self, outfile, queue, verbose=False, index_only=False,
            use_tmp=True):
        """Run clumping and output to a file.

        Parameters
        ----------
        outfile : `str`, or `NoneType`
            The output file, if ``NoneType`` then output will be via STDOUT, if
            the out file has a ``.gz`` extension then gzip will be used.
        verbose : `bool`, optional, default: `False`
            Output a progress monitor when clumping.
        index_only : `bool`, optional, default: `False`
            Only output index variants (not implemented)
        use_tmp : `bool`, optional, default: `False`
            Do not output directly to ``outfile`` but use a temp file and then
            copy on completion.

        Returns
        -------
        outfile : `str`
            The full path to the output file.

        Notes
        -----
        The temp location and delimiter are taken from the handler.
        """
        self.queue = queue
        super().run(outfile, verbose=verbose, index_only=index_only,
                    use_tmp=use_tmp)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MultiClumpRun(_BaseClumpRun):
    """Perform variant clumping using multiple processes.

    Parameters
    ----------
    *args
        Any arguments to be passed through to the base class/handler.
    processes : `int`, optional, default: `2`
        The number of processes to use. Technically it is still possible to use
        1 process but it would probably be slower than the single threaded
        version.
    **kwargs
        Any keyword arguments to pass through to the base class/handler.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, handler, factory_class, handler_class,
                 cohorts, processes=2, **kwargs):
        self.processes = processes
        self.handler_class = handler_class
        self.factory_class = factory_class
        self.cohorts = cohorts

        super().__init__(handler, factory_class, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the runner, this will call the handler to load the dataset.
        """
        if self.is_open is True:
            raise RuntimeError("already open")
        self.handler.open()
        self.is_loaded = True
        self.is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the runner, this will also close the handler.
        """
        if self.is_open is False:
            raise RuntimeError("not open")
        self.handler.delete()
        self.is_loaded = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def start_progress(self, verbose=False):
        """
        """
        self.stop_progress()
        self._pbar = self._get_progress_bar(verbose=verbose)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def stop_progress(self):
        """
        """
        try:
            self._pbar.close()
        except AttributeError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update_progress(self, n):
        """
        """
        self._pbar.update(n)

    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def run(self, outfile, verbose=False, index_only=False, use_tmp=True):
    #     """Run clumping and output to a file.

    #     Parameters
    #     ----------
    #     outfile : `str`, or `NoneType`
    #         The output file, if ``NoneType`` then output will be via STDOUT, if
    #         the out file has a ``.gz`` extension then gzip will be used.
    #     verbose : `bool`, optional, default: `False`
    #         Output a progress monitor when clumping.
    #     index_only : `bool`, optional, default: `False`
    #         Only output index variants (not implemented)
    #     use_tmp : `bool`, optional, default: `False`
    #         Do not output directly to ``outfile`` but use a temp file and then
    #         copy on completion.

    #     Returns
    #     -------
    #     outfile : `str`
    #         The full path to the output file.

    #     Notes
    #     -----
    #     The temp location and delimiter are taken from the handler.
    #     """
    #     # run the clumping
    #     self._run(verbose, index_only)

        # # Set the open method according to the file extension
        # open_method = open
        # if outfile is not None and outfile.endswith(".gz"):
        #     open_method = gzip.open

        # with stdopen.open(outfile, method=open_method, mode="wt",
        #                   use_tmp=use_tmp, tmpdir=self.handler.tmpdir) as out:
        #     # The writer will output the original file contents and the clump
        #     # columns
        #     writer = csv.DictWriter(
        #         out, self.handler.inheader + self.handler.CLUMP_COLS,
        #         delimiter=self.handler.delimiter,
        #         extrasaction='ignore'
        #     )
        #     writer.writeheader()
        #     for row in self.handler.output():
        #         writer.writerow(row)

        # return os.path.realpath(outfile)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _run(self, verbose, index_only):
        """
        """
        chrs = list(self.handler.chromosomes.items())
        reverse = False
        process_chrs = [list() for i in range(self.processes)]
        loop = True
        while loop:
            chrs.sort(key=lambda x: x[1], reverse=reverse)
            for i in process_chrs:
                try:
                    i.append(chrs.pop(0))
                except IndexError:
                    loop = False
                    break
            reverse = not reverse

        for i in process_chrs:
            total = 0
            for c, j in i:
                total += j

        self.update_queue = mp.Queue()

        # Now setup the processes
        idx = 0
        all_processes = []
        for pc in process_chrs:
            if len(pc) == 0:
                continue

            run_chrs = [(c, self.handler.files[c][2]) for c, nc in pc]
            p = mp.Process(
                target=clump_process,
                args=(
                    idx, self.update_queue, self.handler_class,
                    self.factory_class, self.cohorts, run_chrs
                ),
                kwargs=dict(
                    chr_name=self.handler.chr_name,
                    start_pos=self.handler.start_pos,
                    effect_allele=self.handler.effect_allele,
                    other_allele=self.handler.other_allele,
                    pvalue_filter=self.handler.pvalue_filter,
                    pvalue=self.handler.pvalue,
                    pvalue_logged=True,
                    clump_flank=self.handler.flank,
                    rsquared=self.rsquared,
                    verbose=verbose,
                    delimiter=self.handler.delimiter,
                    tmpdir=self.handler.tmpdir,
                    encoding='utf-8',
                    comment=self.handler.comment,
                    dbpath=None,
                    insert_every=10000,
                    ld_batch_size=500
                )
            )
            p.start()
            all_processes.append(p)
            idx += 1

        total = self.handler.get_variant_count()
        nupdate = 0
        self.start_progress(verbose=verbose)
        try:
            while nupdate < total:
                n = self.update_queue.get()
                try:
                    n = int(n)
                except TypeError:
                    raise n
                self.update_progress(n)
                nupdate += n

            for i in all_processes:
                i.join()
        except Exception:
            for i in all_processes:
                i.terminate()
            raise
        finally:
            self.stop_progress()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self):
        """Yield the LD clump data.

        Yields
        ------
        row : `dict`
            Each row represents the contents of the input file and additional
            fields for the clumping these are. ``variant_idx`` (a zero-based
            counter of the variant), ``is_index`` (0=no, 1=yes) ``is_clumped``
            (0=no, 1=yes), ``is_unknown`` (0=no, 1=yes).
        """
        self._run(False, False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clump_process(idx, proc_queue, handler_class, factory_class, cohorts,
                  chromosomes,
                  chr_name='chr_name', start_pos='start_pos',
                  effect_allele='effect_allele', other_allele='other_allele',
                  pvalue='pvalue', pvalue_logged=True, pvalue_filter=5E-08,
                  clump_flank=1E06, rsquared=0.01, verbose=False,
                  delimiter="\t", tmpdir=None, encoding='utf-8', comment='#',
                  dbpath=None, insert_every=10000, ld_batch_size=500):
    """
    """
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", message="Will mask chromosome:*")
        for chrom, infile in chromosomes:
            delete_db = False
            if dbpath is None:
                delete_db = True
                dbpath = utils.get_tmp_file(dir=tmpdir)
            handler = handler_class(
                infile, dbpath, verbose=False, chr_name=chr_name,
                start_pos=start_pos, effect_allele=effect_allele,
                pvalue_filter=pvalue_filter, other_allele=other_allele,
                pvalue=pvalue, pvalue_logged=pvalue_logged,
                clump_flank=clump_flank, delimiter=delimiter, comment=comment,
                encoding=encoding, overwrite=True
            )
            factory = factory_class(
                cohorts, ld_batch_size=ld_batch_size
            )
            # pp.pprint(cohorts)
            runner = ProcessRun(handler, factory, rsquared=rsquared)
            outfile = utils.get_tmp_file(dir=tmpdir)
            try:
                runner.open()

                # Now start the run
                runner.run(outfile, proc_queue, verbose=verbose)
            except Exception as e:
                # proc_queue.put(
                #     e.__class__(f"exception in process {idx}: {e.args[0]}")
                # )
                raise
            finally:
                runner.close()
                if delete_db is True:
                    os.unlink(dbpath)
            shutil.move(outfile, infile)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    For API usage see
    ``bio_misc.ipd.ld_clump.do_ld_clump`` and
    ``bio_misc.ipd.ld_clump.yield_ld_clump``.
    """
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Read the config file, if not present will go to the defaults
    config = genomic_config.open(config_file=args.config)

    # Parse the cohort data from the command line arguments, the parsed data is
    # stored in args.cohorts
    common.parse_cohort_data(args, config)

    logger = log.init_logger(_PROG_NAME, verbose=bool(args.verbose))
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Use a temp file for the database if the user has not provided a path
    rmdb = False
    if args.db_path is None:
        rmdb = True
        args.db_path = utils.get_tmp_file(dir=args.tmp_dir)

    handler_class = ld_handlers.DbClumpHandler
    factory_class = ld_factory.LDFactory
    try:
        do_ld_clump(
            args.infile, args.cohorts, handler_class, factory_class,
            outfile=args.outfile, chr_name=args.chr_name,
            start_pos=args.start_pos, effect_allele=args.effect_allele,
            other_allele=args.other_allele, pvalue=args.pvalue,
            pvalue_logged=args.pvalue_logged, pvalue_filter=args.pvalue_cut,
            clump_flank=args.clump_flank, rsquared=args.rsquared,
            verbose=args.verbose, delimiter=args.delimiter,
            tmpdir=args.tmp_dir, encoding=args.encoding, comment=args.comment,
            dbpath=args.db_path, insert_every=10000, processes=args.processes,
            overwrite=True
        )

        log.log_end(logger)
    except (OSError, FileNotFoundError):
        # Missing files
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        config.close()
        if rmdb is True:
            os.unlink(args.db_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def do_ld_clump(infile, cohorts, handler_class, factory_class,
                outfile=None,  chr_name='chr_name', start_pos='start_pos',
                effect_allele='effect_allele', other_allele='other_allele',
                pvalue='pvalue', pvalue_logged='pvalue_logged',
                pvalue_filter=5E-08, clump_flank=1E06, rsquared=0.01,
                verbose=False, delimiter="\t", tmpdir=None, encoding='utf-8',
                comment='#', dbpath=None, insert_every=10000, processes=1,
                ld_batch_size=500, overwrite=False, **kwargs):
    """The main high level API function.

    This takes an input file and generates and output file of dosages which may
    have been flipped to represent the input file reference allele.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    handler
    factory
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of ``infile``.
    effect_allele : `str`, optional: default: `effect_allele`
        The name of the effect allele column in the header of ``infile``.
    other_allele : `str`, optional: default: `other_allele`
        The name of the other allele column in the header of ``infile``.
    pvalue : `str`, optional: default: `pvalue`
        The name of the pvalue column in the header of ``infile``.
    pvalue_logged : `str`, optional: default: `pvalue`
        Is the pvalue data in the ``pvalue`` column -log10 transformed.
    pvalue_filter : `float`, optional: default: `5E-08`
        Only variants below this cut point will be considered for clumping
        Please note, even if ``pvalue_logged`` is True, supply this value
        untransformed.
    clump_flank : `float`, optional: default: `1E06`
        Only variants within this flank (up and down-stream) will be considered
        for clumping, this is in base pairs.
    rsquared : `float`, optional, default: `0.01`
        The clumping threshold, and variants that have a pairwise LD value >=
        to this are not deemed independent.
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    tmpdir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    dbpath : `str`, optional, default: ``NoneType``
        The path to the output database file, if provided it must not exist. If
        ``NoneType``, then a tempfile will be used to house the database. If it
        is ``:memory:``, then an in-memory database will be used.
    insert_every : `int`, optional, default: `10000`
        Commit to the database every ``insert_every`` rows.
    processes : `int`, optional, default: `1`
        The number of processes to use.
    """

    logger = log.retrive_logger(_PROG_NAME, verbose=verbose)
    if processes == 1:
        handler = handler_class(
            infile, dbpath, verbose=verbose, chr_name=chr_name,
            start_pos=start_pos, effect_allele=effect_allele,
            pvalue_filter=pvalue_filter, other_allele=other_allele,
            pvalue=pvalue, pvalue_logged=pvalue_logged,
            clump_flank=clump_flank, delimiter=delimiter, comment=comment,
            encoding=encoding, overwrite=overwrite
        )
        # handler = FileClumpHandler(
        #     infile, verbose=verbose,
        #     **kwargs
        # )
        factory = factory_class(
            cohorts, ld_batch_size=ld_batch_size
        )

        runner = SingleClumpRun(handler, factory, rsquared=rsquared)
    elif processes > 1:
        handler = ld_handlers.FileSplitHandler(
            infile, verbose=verbose, chr_name=chr_name,
            start_pos=start_pos, effect_allele=effect_allele,
            pvalue_filter=pvalue_filter, other_allele=other_allele,
            pvalue=pvalue, pvalue_logged=pvalue_logged,
            clump_flank=clump_flank, delimiter=delimiter, comment=comment,
            encoding=encoding
        )
        runner = MultiClumpRun(
            handler, factory_class, handler_class, cohorts, processes=processes,
            rsquared=rsquared
        )
    try:
        logger.info(
            f"using {runner.handler.__class__.__name__} and {processes}"
            " processes..."
        )
        runner.open()

        nvars = runner.handler.get_variant_count()
        logger.info(f"loaded {nvars} variants...")
        logger.info(f"there are {runner.handler.nerrors} error variants...")
        logger.info(f"there are {runner.handler.nexcludes} excluded variants...")
        logger.info(f"chromosomes {','.join(runner.handler.get_chromosomes())}...")

        # Now start the run
        runner.run(outfile, verbose=verbose)
        # runner.fetch()
        # for row in runner.fetch():
        #     pp.pprint(row)
    finally:
        runner.close()
    # batch_len = 500

    # # NoneType means create the database in a temp file
    # if dbpath is None:
    #     dbpath = utils.get_tmp_file(dir=tmpdir)

    # # The keyword arguments for initial filtering of the input GWAS summary
    # # file
    # kwargs = dict(
    #     chr_name=chr_name,
    #     start_pos=start_pos,
    #     effect_allele=effect_allele,
    #     other_allele=other_allele,
    #     pvalue=pvalue,
    #     pvalue_logged=pvalue_logged,
    #     pvalue_cut=pvalue_cut,
    #     verbose=verbose,
    #     delimiter=delimiter,
    #     encoding=encoding,
    #     comment=comment,
    #     insert_every=insert_every
    # )

    # logger.info("filtering/loading variants...")
    # # TODO: Remove in final
    # # db, nerrors, nexcludes = dbpath, 0, 0
    # db, nerrors, nexcludes = load_infile(infile, dbpath, **kwargs)
    # logger.info(f"data loaded into {dbpath}")
    # logger.info(f"there are {nerrors} error variants...")
    # logger.info(f"there are {nexcludes} excluded variants...")

    # if not isinstance(db, sqlite3.Connection):
    #     db = sqlite3.connect(db)
    # cur = db.cursor()
    # chr_names = get_chromosomes(cur)
    # total_vars = get_variant_count(cur)

    # data_queue = mp.Queue()
    # results_queue = mp.Queue()

    # process_obj = []
    # for i in range(processes):
    #     p = mp.Process(
    #         target=ld_worker,
    #         args=(i, cohorts, data_queue, results_queue, rsquared),
    #         kwargs=dict(dtype=dtype)
    #     )
    #     p.start()
    #     process_obj.append(p)
    # pbar = tqdm(
    #     total=total_vars,
    #     desc="[info] LD clumping",
    #     unit=" variants",
    #     disable=common.verbose_disable(verbose)
    # )
    # for i in cohorts:
    #     i.open()
    # # pp.pprint(chr_names)
    # try:
    #     for i in chr_names:
    #         prev_uni_id = ""
    #         while True:
    #             batch_idx = 0
    #             batches = {}
    #             try:
    #                 # Get the next index variant, will only return index
    #                 # variants that are known in at least one of the cohorts
    #                 index_var, cohort_idx = get_index_variant(
    #                     db, cur, cohorts, i
    #                 )
    #                 # print(index_var)
    #             except IndexError:
    #                 # print("finished chromosome {i}")
    #                 # No more index variants for chromosome
    #                 break

    #             if prev_uni_id == index_var.uni_id:
    #                 raise ValueError(f"repeat variant: {prev_uni_id}")

    #             # Now get all the variants that form a batch with the index
    #             # variant these will be rolled up in bite size batches and
    #             # placed on the data queue to be processed by the ld workers
    #             batch_vars = []
    #             for v in get_clump_batch(cur, index_var, clump_flank):
    #                 batch_vars.append(v)
    #                 if len(batch_vars) == batch_len:
    #                     # pp.pprint(batch_vars)
    #                     data_queue.put(
    #                         (batch_idx, index_var, cohort_idx, batch_vars)
    #                     )
    #                     batches[batch_idx] = len(batch_vars)
    #                     batch_vars = []
    #                     batch_idx += 1

    #             # Processes any residual variants
    #             if len(batch_vars) > 0:
    #                 data_queue.put(
    #                     (batch_idx, index_var, cohort_idx, batch_vars)
    #                 )
    #                 batches[batch_idx] = len(batch_vars)

    #             pbar.update(1)
    #             # The index variant had no batches, i.e. it is on it's own
    #             # so we just update that and move on
    #             if len(batches) == 0:
    #                 # Update index only and move on
    #                 update_database(cur, [index_var])
    #                 db.commit()
    #                 # Update the progress bar
    #                 continue

    #             # Now pull in the results and update
    #             got = 0
    #             while got < len(batches):
    #                 # print(got, len(batches))
    #                 batch_idx, variants = results_queue.get()
    #                 # Update the database
    #                 update_database(cur, variants)
    #                 got += 1
    #                 # print(len(variants))
    #                 # if len(variants) == 1:
    #                 #     print(variants[0])
    #                 # Update the progress bar
    #                 pbar.update(len(variants) - 1)
    #             db.commit()
    # finally:
    #     db.close()
    #     pbar.close()
    #     for worker in process_obj:
    #         if worker.is_alive():
    #             # print("terminate")
    #             worker.terminate()
    #             worker.join()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ld_worker(pidx, cohorts, data_queue, results_queue, rsquared,
              dtype=np.float64):
    """Will run in the background and calculate LD between index variants and
    batches of clump variants.

    Processes
    ---------
    pidx : `int`
        The process number.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the LD calculations.
    data_queue : `multiprocessing.Queue`
        A queue that the worker will receive data from.
    results_queue : `multiprocessing.Queue`
        A queue that the worker will place results in.
    rsquared : `float`
        The r-squared LD cut off, variant >= to this will be clumped.
    dtype : `numpy.float`, optional, default: `np.float64`
        The precision to use for the calculations.
    """
    # Open the cohorts
    for i in cohorts:
        i.open()

    try:
        while True:
            try:
                batch_id, index_var, cohort_idx, test_vars = data_queue.get()
            except TypeError as e:
                if e.args[0] == "cannot unpack non-iterable int object":
                    break
                else:
                    raise
            index_unknown = False
            all_vars = []
            all_vars.append(index_var)
            for idx in cohort_idx:
                i = cohorts[idx]
                try:
                    clumped_vars, unknown_vars = clump_batch(
                        i, index_var, test_vars, rsquared
                    )
                    index_unknown = False
                    all_vars.extend(clumped_vars)
                    test_vars = unknown_vars
                except IndexError:
                    index_unknown = True

            index_var.is_unknown = index_unknown
            if index_unknown is True:
                results_queue.put((batch_id, all_vars))
                continue

            for i in unknown_vars:
                i.is_unknown = True
            all_vars.extend(unknown_vars)
            results_queue.put((batch_id, all_vars))
    finally:
        for i in cohorts:
            i.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clump_batch(cohort, index_var, test_vars, rsquared):
    """
    """
    try:
        idx_dose, idx_flipped = common.get_variant_dosage(
            cohort.reader, index_var.chr_name, index_var.start_pos,
            index_var.effect_allele, index_var.other_allele
        )
    except IndexError as e:
        raise RuntimeError("no index") from e

    test_dose = []
    known_vars = []
    unknown_vars = []
    for i in test_vars:
        try:
            dose, flipped = common.get_variant_dosage(
                cohort.reader, i.chr_name, i.start_pos,
                i.effect_allele, i.other_allele
            )
            test_dose.append(dose)
            known_vars.append(i)
        except IndexError:
            unknown_vars.append(i)

    clumped_vars = []
    if len(test_dose) > 0:
        cc, nobs = common.block_corrcoef(
            ma.masked_invalid([idx_dose]),
            ma.masked_invalid(test_dose)
        )
        # print(cc.shape)
        # print(len(known_vars))
        is_clumped = np.square(cc) >= rsquared
        # print(is_clumped.shape)
        for idx in range(is_clumped.shape[1]):
            known_vars[idx].is_clumped = is_clumped[0, idx]
            if known_vars[idx].is_clumped is True:
                clumped_vars.append(known_vars[idx])
    return clumped_vars, unknown_vars


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def split_infile(infile, dbdir, chr_name=con.CHR_NAME, start_pos=con.START_POS,
                 effect_allele='effect_allele', other_allele='other_allele',
                 pvalue='pvalue', pvalue_logged=False, pvalue_cut=5E-08,
                 verbose=False, delimiter=con.DEFAULT_DELIMITER,
                 encoding=con.DEFAULT_ENCODING, comment=con.DEFAULT_COMMENT,
                 insert_every=10000):
    """Filter the input file for variants that are below the cut off point.

    This takes an input file and filters all it for all the variants that are
    more significant or equal to the significance cut off.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    dbpath : `str`
        The path to the output database file, must not exist. If it is
        ``:memory:``, then an in-memory database will be used.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of ``infile``.
    effect_allele : `str`, optional: default: `effect_allele`
        The name of the effect allele column in the header of ``infile``.
    other_allele : `str`, optional: default: `other_allele`
        The name of the other allele column in the header of ``infile``.
    pvalue : `str`, optional: default: `pvalue`
        The name of the pvalue column in the header of ``infile``.
    pvalue_logged : `str`, optional: default: `pvalue`
        Is the pvalue data in the ``pvalue`` column -log10 transformed.
    pvalue_cut : `float`, optional: default: `5E-08`
        Only variants below this cut point will be considered for clumping
        Please note, even if ``pvalue_logged`` is True, supply this value
        untransformed.
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    insert_every : `int`, optional, default: `10000`
        Commit to the database every ``insert_every`` rows.

    Returns
    -------
    con : `sqlite3.Connection`
        The database connection.
    nerrors : `int`
        The number of error rows detected.
    nexcludes : `int`
        The number of excluded rows detected.
    """
    pval_trans_func = float
    pvalue_cut = log_transform(pvalue_cut)
    if pvalue_logged is False:
        pval_trans_func = log_transform

    if pvalue_cut is None or pvalue_cut < 0:
        raise ValueError("bad pvalue cutoff: -log10(pvalue)={pvalue_cut}")

    open_method = open
    if infile is not None and infile != '-' and utils.is_gzip(infile):
        open_method = gzip.open

    nerrors = 0
    nexcludes = 0

    fobj = dict()

    with stdopen.open(
            infile, method=open_method, encoding=encoding
    ) as inobj:
        row = comment
        while row.startswith(comment):
            row = next(inobj)

        header = row.strip().split(delimiter)
        reader = csv.DictReader(
            inobj, fieldnames=header, delimiter=delimiter
        )
        file_idx = 0
        for idx, row in enumerate(
                tqdm(reader,
                     desc="[info] filtering input file...",
                     unit=" rows",
                     disable=common.verbose_disable(verbose))
        ):
            try:
                if row[chr_name] is not None:
                    fobj[row[chr_name]]
            except KeyError:
                pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_coords(variant, chr_name, start_pos, effect_allele, clump_flank):
    """Initialise coordinate flanks for deciding if a variant should be
    included in an LD clumping batch.

    Parameters
    ----------
    variant : `dict`
        The data row. The ``dict`` represents an input variant that is more
        significant than the cut off value. The keys are columns and should
        match the names for ``chr_name``, ``start_pos``, ``effect_allele``.
    chr_name : `str`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`
        The name of the start position column in the header of ``infile``.
    effect_allele : `str`
        The name of the effect allele column in the header of ``infile``.
    clump_flank : `float`
        Only variants within this flank (up and down-stream) will be considered
        for clumping, this is in base pairs.

    Returns
    -------
    chr_name_value : `str`
        The chromosome value for the flank.
    flank_start_pos : `int`
        The start position of the flank.
    flank_end_pos : `int`
        The end position of the flank.
    """
    end_pos = variant[start_pos] + len(variant[effect_allele]) - 1
    flank_start = variant[start_pos] - clump_flank
    flank_end = end_pos + clump_flank
    return variant[chr_name], flank_start, flank_end


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _unique_col(header, colname):
    """Make sure the column name is unique with respect to existing columns in
    the header.

    Index values are added to the end of the column name until it does not
    exist in the header.

    Parameters
    ----------
    header : `list` of `str`
        The header to test against
    colname : `str`
        The starting column name that will adjusted to be unique with respect
        to the header.

    Returns
    -------
    unique_colname : `str`
        A guaranteed unique column name.
    """
    idx = 1
    while colname in header:
        colname = f"{colname}_{idx}"
        idx += 1
    return colname


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        'cohorts', type=str, nargs="+",
        help="The name for the cohort(s) you want to extract from"
    )
    arguments.add_input_output(
        parser,
        infile_optional=True,
        outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_genomic_coords_arguments(parser, end_pos=True)
    parser.add_argument(
        '--config', type=str,
        help="The path to the genomic-config file, that lists all the"
        " reference populations to use for LD, if not supplied will try to"
        " ID it in ``GENOMIC_CONFIG`` environment variable  or "
        "~/.genomic_data.ini"
    )
    parser.add_argument(
        '--samples', type=str, nargs="+", default=['-'],
        help="The path any sample ID file (one sample listed per line), of a"
        " sample name to use in the config file. These will be used as a "
        "reference for LD and if not supplied will extract all the samples. If"
        " you only want samples from a single cohort then you should use a dash"
        " to represent any other cohorts (i.e. no file) so if you have three "
        "cohorts and you anly want samples for cohort to then you would use "
        "--samples - /path/to/cohort2/samples -"
    )
    parser.add_argument(
        '--format', type=str, nargs="+", choices=common.FILE_FORMATS,
        default=[common.FILE_FORMATS[0]],
        help="The file formats for the reference genome files to extract "
        "from. There should be one for each cohort"
    )
    parser.add_argument(
        '--assembly', type=str, default="GRCh38",
        help="The assembly for the reference cohort"
    )
    parser.add_argument(
        '-v', '--verbose', action="count",
        help="Output progress, use -vv for progress monitors"
    )
    arguments.add_allele_arguments(
        parser, ref=True, alt=True, use_effect_other=True
    )
    parser.add_argument(
        '--clump-flank', type=float, default=1E06,
        help="Flanking region size around the lead variants, where"
        " variants will be considered for clumping."
    )
    parser.add_argument(
        '--pvalue', type=str, default="pvalue",
        help="The name of the pvalue column in the input GWAS file."
    )
    parser.add_argument(
        '--pvalue-cut', type=float, default=5E-08,
        help="The pvalue cuttoff value. Only variants below this will be "
        "considerred for clumping. Note, even if your GWAS pvalue is "
        "logged, please use an unlogged value here."
    )
    parser.add_argument(
        '--rsquared', type=float, default=0.01,
        help="The rsquared to clump at, i.e. variants with r2 >= to this a"
        " not deemed independent."
    )
    parser.add_argument(
        '--pvalue-logged', action="store_true",
        help="Is the pvalue column -log10 transformed"
    )
    parser.add_argument(
        '--zero', type=str, nargs="+", default=[],
        help="The names of the cohorts that you want to add a leading zero to"
        " the chromosome name that have a length < 2 this is to deal with "
        "some UK BioBank bgen implementations."
    )
    parser.add_argument(
        '--db-path', type=str, default=None,
        help="The clumping uses an SQLite database to keep track of state, if"
        " you want this to be maintained after then give a file name here. You"
        " can also put ``:memory:`` to use an in memory SQLite database for"
        " faster operations."
    )
    parser.add_argument(
        '--species', type=str, default=common.SPECIES,
        help="The species for the reference cohort"
    )
    parser.add_argument(
        '--processes', type=int, default=1,
        help="The number of processes to use."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def yield_ld_clumps(data, config, cohorts, chr_name='chr_name',
#                     start_pos='start_pos', effect_allele='effect_allele',
#                     other_allele='other_allele', pvalue='pvalue',
#                     pvalue_logged=False, pvalue_cut=5E-08, rsquared=0.01,
#                     verbose=False, clump_flank=1E06, assembly="GRCh38",
#                     nopen=1):
#     """The main high level API function.

#     This takes an input file and generates and output file of dosages which may
#     have been flipped to represent the input file reference allele.

#     Parameters
#     ----------
#     data : `list` of `dict`
#         The data to clump. Each ``dict`` represents an input variant. The
#         keys are columns and should match the names for ``chr_name``,
#         ``start_pos``, ``effect_allele``, ``other_allele`` and ``pvalue``.
#     config : `genomic_config.ini_config.IniConfig`
#         The configuration file object that will provide the files based on
#         genomic location.
#     cohorts : `list` of `dict`
#         Descriptions of the cohorts that will be used for clumping. Each
#         dict should have the keys, ``cohort_no`` (int), ``cohort_name`` (str),
#         ``leading_zero`` (bool), ``samples`` (``list`` of ``str``, empty for no
#         samples), ``sample_section`` (``str`` or ``NoneType`` - for no sample
#         config section).
#     chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
#         The name of the chromosome column in the header of ``infile``.
#     start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
#         The name of the start position column in the header of ``infile``.
#     effect_allele : `str`, optional: default: `effect_allele`
#         The name of the effect allele column in the header of ``infile``.
#     other_allele : `str`, optional: default: `other_allele`
#         The name of the other allele column in the header of ``infile``.
#     pvalue : `str`, optional: default: `pvalue`
#         The name of the pvalue column in the header of ``infile``.
#     pvalue_logged : `str`, optional: default: `pvalue`
#         Is the pvalue data in the ``pvalue`` column -log10 transformed.
#     pvalue_cut : `float`, optional: default: `5E-08`
#         Only variants below this cut point will be considered for clumping
#         Please note, even if ``pvalue_logged`` is True, supply this value
#         untransformed.
#     rsquared : `float`, optional, default: `0.01`
#         The clumping threshold, and variants that have a pairwise LD value >=
#         to this are not deemed independent.
#     verbose : `bool`, optional, default: `False`
#         Report progress.
#     clump_flank : `float`, optional: default: `1E06`
#         Only variants within this flank (up and down-stream) will be considered
#         for clumping, this is in base pairs.
#     assembly : `str`, optional: default: `GRCh38`
#         The genome assembly in the config file.
#     nopen : `int`, optional, default `1`
#         The number of bgen files that will be opened at the same time.
#     """
#     logger = log.retrive_logger(_PROG_NAME, verbose=verbose)

#     cohorts = _init_cohorts(cohorts, config, assembly)
#     # pp.pprint(cohorts)
#     indep_vars = 0
#     nunknown = 0
#     try:
#         # Open all the cohorts
#         for i in cohorts:
#             reader = i['read_class'](i['provider'], sample_ids=i['samples'])
#             reader.open()
#             reader.check_samples(verbose=False)
#             i['reader'] = reader

#         # Keyword arguments for the batch generator
#         kwargs = dict(
#             chr_name=chr_name,
#             start_pos=start_pos,
#             pvalue=pvalue,
#             pvalue_logged=pvalue_logged,
#             effect_allele=effect_allele,
#             clump_flank=clump_flank,
#             verbose=verbose
#         )
#         for batch in yield_clump_batch(data, **kwargs):
#             index_var = batch[0]
#             index_dose = None
#             if len(batch) == 1:
#                 indep_vars += 1
#                 index_var[_SINGLE_BATCH] = 1
#                 yield index_var
#                 continue
#             else:
#                 # Initialise the index variant in each cohort
#                 all_index_vars = []
#                 for c in cohorts:
#                     try:
#                         index_dose, flipped = next(
#                             c['reader'].get_variant_dosage(
#                                 c['zero_func'](index_var[chr_name]),
#                                 index_var[start_pos],
#                                 index_var[effect_allele],
#                                 index_var[other_allele]
#                             )
#                         )
#                         all_index_vars.append((index_dose, c))
#                     except StopIteration:
#                         # No LD data
#                         continue
#                 if len(all_index_vars) == 0:
#                     # No LD data
#                     index_var[_IS_UNKNOWN_COL] = 1
#                     yield index_var
#                     continue
#             index_cohorts = set()
#             for i in range(1, len(batch), 1):
#                 test_has_data = False
#                 for idxdose, cohort in all_index_vars:
#                     test_var = batch[i]

#                     try:
#                         test_dose, flipped = next(
#                             cohort['reader'].get_variant_dosage(
#                                 cohort['zero_func'](test_var[chr_name]),
#                                 test_var[start_pos],
#                                 test_var[effect_allele],
#                                 test_var[other_allele]
#                             )
#                         )
#                         cc = np.square(np.corrcoef(idxdose, test_dose)[0][1])
#                         if cc > rsquared:
#                             test_var[_IS_IDX_COL] = False
#                             test_var[_IS_CLUMPED_COL] = True
#                             test_var[_INDEX_VAR] = index_var
#                             test_var[_R2_VS_INDEX] = cc
#                             test_var[_COHORTS] = cohort['cohort_name']
#                             index_var[_NCLUMPED] += 1
#                         # if test_var[_IS_UNKNOWN_COL] == 1 and len(test_var[_COHORTS]) != 0:
#                         #     pp.pprint(test_var)
#                         #     raise RuntimeError("test var fucked")

#                         index_cohorts.add(cohort['cohort_name'])
#                         test_has_data = True
#                         break
#                     except StopIteration:
#                         # No LD data
#                         continue
#                 if test_has_data is False:
#                     # if test_var['var_id'] == 'rs186657692':
#                     #     pp.pprint(test_var)
#                     #     pp.pprint(len(all_index_vars))
#                     test_var[_IS_UNKNOWN_COL] = 1
#             indep_vars += 1
#             index_var[_COHORTS] = "|".join(index_cohorts)
#             yield index_var
#     finally:
#         # close all the cohorts
#         for i in cohorts:
#             i['reader'].close()
#     logger.info(f"independent variants at r2={rsquared}: {indep_vars}")


