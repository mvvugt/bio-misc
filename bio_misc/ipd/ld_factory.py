"""Classes for handling and caching pairwise LD data.
"""
from bio_misc.ipd import common
import numpy as np
import numpy.ma as ma


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class LDFactory(object):
    """Ad-hoc pair-wise LD queries.

    Parameters
    ----------
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    ld_batch_size : `int, optional, default: `500`
        How many LD calculations that will be performed at once, higher will
        utilise more memory. This has not been benchmarked properly.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, cohorts, ld_batch_size=500):
        self.cohorts = cohorts
        self.ld_batch_size = ld_batch_size
        self.is_open = False

        # Store the indexes of the available cohorts
        self._cohorts = list(range(len(cohorts)))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Enter the context manager
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Exit the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the factory, this will load infile. This is called on
        __enter__.
        """
        if self.is_open is True:
            raise RuntimeError("already open")
        for i in self.cohorts:
            i.open()
        self.is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the factory, this is called on __exit__.
        """
        for i in self.cohorts:
            i.close()
        self.is_open = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_variant(self, variant):
        """Return the cohort indexes that contain the variant coordinates and
        alleles.

        Parameters
        ----------
        variant : `bio_misc.ipd.common.ClumpVariant`
            The variant to check.

        Returns
        -------
        cohort_idx : `list` of `int`
            The index values for the cohorts that contain the variant. If this
            is length 0 then the variant is not in any cohorts.
        """
        cohort_idx = []
        for idx, i in enumerate(self.cohorts):
            is_present = i.reader.has_variant(
                i.zero_func(variant.chr_name), variant.start_pos,
                variant.effect_allele, variant.other_allele
            )
            if is_present is True:
                cohort_idx.append(idx)
            else:
                variant.add_unknown(i)
        return cohort_idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_one_vs_many(self, index_var, test_vars, cohorts=None):
        """Get pairwise LD between a single variant and many other variants.
        """
        cohorts = cohorts or self._cohorts
        for idx in cohorts:
            i = self.cohorts[idx]

            try:
                return self._cohort_one_vs_many(i, index_var, test_vars)
            except RuntimeError:
                continue
        raise RuntimeError("no index")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _cohort_one_vs_many(self, cohort, index_var, test_vars):
        """Get pairwise LD between a single variant and many other variants.
        """
        try:
            idx_dose, idx_flipped = common.get_variant_dosage(
                cohort.reader,
                cohort.zero_func(index_var.chr_name),
                index_var.start_pos,
                index_var.effect_allele,
                index_var.other_allele
            )
        except IndexError as e:
            index_var.add_unknown(cohort)
            raise RuntimeError("no index") from e

        test_dose = []
        known_vars = []
        unknown_vars = []
        for i in test_vars:
            if i.is_unknown_bits > 0 and \
               bool(cohort.bits & i.is_unknown_bits) is True:
                # print(i)
                # print(cohort)
                # No point querying stuff that is unknown for the cohort
                # raise ValueError("this should not happen")
                unknown_vars.append(i)
                continue

            try:
                dose, flipped = common.get_variant_dosage(
                    cohort.reader,
                    cohort.zero_func(i.chr_name),
                    i.start_pos,
                    i.effect_allele,
                    i.other_allele
                )
                test_dose.append(dose)
                known_vars.append(i)
            except IndexError:
                i.add_unknown(cohort)
                unknown_vars.append(i)

        pairs = []
        if len(test_dose) > 0:
            ld, nobs = common.block_corrcoef(
                ma.masked_invalid([idx_dose]),
                ma.masked_invalid(test_dose)
            )
            ld = np.square(ld)
            # print(is_clumped.shape)
            for idx in range(ld.shape[1]):
                pairs.append(
                    (index_var, known_vars[idx], ld[0, idx])
                )
        return pairs, unknown_vars
