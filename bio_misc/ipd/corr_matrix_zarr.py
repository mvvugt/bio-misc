"""Build a pairwise correlation matrix using zarr
"""
from bio_misc.ipd import common, dosage_zarr
from tqdm import tqdm
import shutil
import numpy as np
import numpy.ma as ma
import os
import warnings
import multiprocessing as mp
import tempfile
# import zarr
# import pprint as pp
from numcodecs import blosc

# For use in zarr with multiprocessing, to stop blocking
blosc.use_threads = False
# np.set_printoptions(threshold=sys.maxsize)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RunZarrCorrMatrix(common._BaseRunCorrMatrix):
    """Generate pairwise correlations and numbers of observations for a
    specific set of variants.

    This implementation queries variants in batches and is designed to run in
    parallel. This increases performance substantially.

    Parameters
    ----------
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The data type for the resulting correlation matrix. The accepted
        choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
        set according to the size of the matrix, available memory and the
        required precision. Lower values will save a lot of memory if
        memory is tight.

    Notes
    -----
    This is based from allele dosages that have been extracted into a zarr
    array. So the first step is to extract the dosages for the variants from
    the current cohort. Then, these are used to build the correlation matrix.
    Note both dosage extraction and correlation matrix generation are able to
    use multiple processes, this speeds things up considerably. Missing data
    will also be represented in the final correlation matrix.

    This also treats the cohorts hierarchically, so the first cohort is used to
    form the built of the matrix then the second one is use for gap filling
    etc...
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.zarr_dosage_arrays = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run(self, *args, **kwargs):
        """Generate a correlation matrix across all input cohorts.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        self.zarr_dosage_arrays = []
        return super().run(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run_cohort(self, invars, cohort, batch_size=50, processes=2,
                   verbose=False, chunk_rows=1000, lock=False, load_size=100,
                   file_prefix=None, tmpdir=None):
        """Generate a correlation matrix for a single cohort.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        cohort : `bio_misc.ipd.common.Cohort`
            Description of the cohort that will be used for as the source data
            for the correlation matrix.
        batch_size : `int`, optional, default: `1`
            The number of variant allele counts in each batch that will be
            stored in memory at one time. So the total number in each
            comparison will be twice this. This can be used to increased speed
            but at the expense of additional memory usage. The memory usage
            will also depend on the sample count in your reference population.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.
        chunk_rows : `int`, optional, default: `1000`
            The allele dosage zarr array will be split into chunks containing a
            max of ``chunk_rows``. Each chunk will be a separate file in the zarr
            array and the separate processes will be aligned with the chunk_row
            boundaries to enable parallel loading with no synchronisation.
            Although, synchronisation can still be applied using ``lock=True``.
        processes : `int`, optional, default: `2`
            The number of separate processes that will be used to load the data
            into the zarr array and generate the correlation matrix. Please note,
            that depending on the chunk size the desired process count may not be
            reached when building the zarr dosage array.
        lock : `bool`, optional, default: `False`
            Apply process synchronisation when loading the allele dosage zarr array
            in parallel.
        file_name : `str`, optional, default: `NoneType`
            The path and prefix of the zarr_array. A separate zarr array will be
            created for each cohort, with the cohort name and the cohort index,
            i.e. ``/<file_prefix>/<NAME>_c<IDX>.zarr``. Please note, that are
            actually a directories that will contain the individual zarr chunks.
            If they do not exist it will be created and if it is already present it
            will be wiped and overridden. A ``NoneType`` value, will generate a
            temp_directory (so make sure tmp is big enough) in which the array
            data will be located. The temp directory will persist after this
            function exists.
        dtype : `numpy.float`, optional, default: `numpy.float64`
            The precision of the zarr array. Must be one of `numpy.float16`,
            `numpy.float32`, `numpy.float64`.
        load_size : `int`, optional, default:
            Within each allele dosage load process, the data is loaded into the
            zarr array in ``load_size`` batches.
        tmp_dir : `str`, optional, default: `NoneType`
            The location of tmp, this is used if ``file_prefix`` is ``NoneType``.
            ``NoneType``, defaults to the system temp location.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        # Make sure the variants that have been supplied are unique
        invars = common.unique_variants(invars)

        use_temp = False
        if file_prefix is None:
            use_temp = True
            file_prefix = tempfile.mkdtemp(dir=tmpdir)

        file_name = os.path.join(
            file_prefix,
            f"{cohort.name}_C{cohort.cohort_idx}.zarr"
        )
        dosage_array = dosage_zarr.get_dosage_array(
            invars, file_name, cohort, verbose=verbose, chunk_rows=chunk_rows,
            processes=processes, lock=lock, dtype=self.dtype,
            load_size=load_size
        )
        # dosage_array.read_only = True
        nrows = dosage_array.shape[0]
        total = nrows*nrows
        process_combs = common.get_combs(nrows, batch_size, processes)

        queue = mp.JoinableQueue()

        corr_matrix, nobs_matrix = super().run_cohort(
            invars, cohort, verbose=verbose
        )

        try:
            pbar = tqdm(
                total=total,
                desc=f"[info] running cohort {cohort.name}",
                unit=" correlations",
                disable=common.verbose_disable(verbose)
            )
            # Initialise the processes
            run_processes = []
            for i in range(len(process_combs)):
                p = mp.Process(
                    target=_corr_matrix_process,
                    args=(i, queue, dosage_array, process_combs[i]),
                    kwargs=dict(dtype=self.dtype)
                )
                p.start()
                run_processes.append(p)

            ncorrs = 0
            while ncorrs < total:
                nproc, r, c, cc, nobs = queue.get()
                rstart, rend = r
                cstart, cend = c
                corr_matrix[rstart:rend, cstart:cend] = cc
                corr_matrix[cstart:cend, rstart:rend] = cc.T
                nobs_matrix[rstart:rend, cstart:cend] = nobs
                nobs_matrix[cstart:cend, rstart:rend] = nobs.T
                updates = (1+bool(rstart - cstart))*(cc.shape[0]*cc.shape[1])
                pbar.update(updates)
                ncorrs += updates
                queue.task_done()
        finally:
            pbar.close()
            queue.join()
            for i in run_processes:
                i.join()

            if use_temp is True:
                shutil.rmtree(file_name)
            else:
                self.zarr_dosage_arrays.append(file_name)

        return corr_matrix, nobs_matrix


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _corr_matrix_process(proc_idx, queue, arr, combs, dtype=np.float64):
    """Generate pairwise correlation blocks with in a separate process.

    This is designed to be used in multiprocessing and places the correlation
    calculations in a queue.

    Parameters
    ----------
    proc_idx : `int`
        The process ID (number) of this process.
    queue : `multiprocessing.Queue`
        The shared queue that this process will use to pass the data back to
        the main process.
    arr : `zarr.array`
        The zarr array that contains the pre-queried allele dosage information.
    combs : `list` of `tuple` of `tuple`
        The slices of the zarr array that we want to generate the correlation
        matrix for. i.e. pairwise over row/column slices. The nested tuples have
        the structure (`row_start`, `row_end`), (`col_start`, `col_end`).
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The precision of the correlation matrix.

    Notes
    -----
    For each row/column combination, this adds the a tuple of the following to
    the queue.
    ``[0]`` size if the matrix (int).
    ``[1]`` tuple of ``(row_start, row_end)``.
    ``[2]`` tuple of ``(col_start, col_end)``.
    ``[3]`` correlation matrix.
    ``[4]`` number of observations matrix.
    """
    # I am setting this here, but it still looks like it is operating
    # multi-threaded? Maybe pass the file name and open a fresh array?
    blosc.use_threads = False

    with warnings.catch_warnings():
        # Annoying numpy warnings
        warnings.filterwarnings(
            "ignore", message="Warning: converting a masked element to nan."
        )
        for r, c in combs:
            rstart, rend = r
            cstart, cend = c
            row_vars = ma.masked_invalid(
                arr[rstart:rend],
                copy=False
            )
            col_vars = ma.masked_invalid(
                arr[cstart:cend],
                copy=False
            )
            try:
                cc, nobs = common.block_corrcoef(row_vars, col_vars)
            except Exception as e:
                print(f"[info] exception {proc_idx} {e}!!!")
                raise
            queue.put(
                (
                    cc.size,
                    (rstart, rend),
                    (cstart, cend),
                    cc,
                    nobs
                )
            )
    # print(f"[info] process {proc_idx} finished!!!")
