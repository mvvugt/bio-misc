from bgen_reader import open_bgen
from scipy.stats import rankdata
import pysam
import cyvcf2
import warnings
import itertools
import numpy as np
import numpy.ma as ma
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseGenoReader(object):
    """A base genotype reader class that provides some general purpose methods.

    Parameters
    ----------
    infile : `str`
        The path to the input genotype file.
    sample_ids : `NoneType` or `list` of `str`
        The sample IDs we are interested in. ``NoneType`` means all of them.
    dtype : `numpy.float64` or `numpy.float64` or `numpy.float64`, optional,
    default: `numpy.float64`
        The data types for the returned dosages. If you know the precision of
        your input file you can make ths smaller to save memory.
    **kwargs
        Any keyword arguments to be passed to the underlying reader class,
        when opening the file.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, dtype=np.float64, sample_ids=None, **kwargs):
        self._infile = infile
        self._sample_ids = sample_ids
        self._kwargs = kwargs
        self._opened = False
        self._is_paused = False
        self._reader = None
        self.dtype = dtype

        # Make sure the sample IDs are unique
        if self._sample_ids is not None:
            if len(set(self._sample_ids)) != len(self._sample_ids):
                prev_sid = ""
                reps = set([])
                for i in sorted(self._sample_ids):
                    if prev_sid == i:
                        reps.append(i)
                raise ValueError(
                    f"requested sample IDs must be unique: {'j'.join(reps)}"
                )

        # The default is to extract all of them
        self._sample_idx = slice(None)
        self._opened = False
        self._is_paused = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An opener for the bgen file. If called on an open file, it will
        ignore the call and not error out.

        Raises
        ------
        ValueError
            If there are multi-allelic sites or if the organsim is not diploid
            or if none of the requested samples are in the bgen file.

        Notes
        -----
        This will issue user warnings for subsets of samples that are not in
        the bgen file.
        """
        if self._opened is False:
            self._opened = True
            self.set_samples()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        if self._opened is True:
            self._opened = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def pause(self):
        """Close the reader but keep initialisation
        """
        self._is_paused = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def resume(self):
        """Close the reader but keep initialisation
        """
        self._is_paused = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_samples(self):
        """Set the index values for the sample IDs that we are interested in.

        Parameter
        ---------
        samples : `list` of `str`
            A list of sample IDs that we want to extract.

        Raises
        ------
        ValueError
            If none of the samples are resent in the file
        IOError
            If the input file is not open

        Notes
        -----
        The samples are extracted in the order of the given IDs. A warning is
        issued if only a subset of samples are found.
        """
        # We only attempt to do this is the input file is open
        if self._opened is True:
            # If some sample IDs have been provided, then we attempt to ID
            # them and get their index values
            if self._sample_ids is not None and len(self._sample_ids) > 0:
                self._sample_idx = np.flatnonzero(
                    np.isin(self.samples, self._sample_ids)
                )
                # If we have not found any then that is an error
                if len(self._sample_idx) == 0:
                    raise ValueError(
                        "0 requested samples in the file: '{0}'".format(
                            self.infile
                        )
                    )
                # If we have only found a subset then we just warn
                elif len(self._sample_idx) != len(self._sample_ids):
                    missing = len(self._sample_ids) - len(self._sample_idx)
                    warnings.warn(
                        "{0} sample(s) not in file: '{1}'".format(
                            missing, self._infile
                        )
                    )
        else:
            raise IOError("file not open: {0}".format(self._infile))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def infile(self):
        """Return the input file name.
        """
        return self._infile

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples(self):
        """Return all the samples in the file being read (`list` of `str`).
        """
        return np.array([])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sample_idx(self):
        """Return the sample index positions of the any requested samples
        (or the whole lot of none are requested) (`list` of `int`).
        """
        return self._sample_idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples_requested(self):
        """Return all the samples that the user has requested even if they are
        not in the reader. If none have been request then this will be an empty
        list (`list` of `str`).
        """
        return self._sample_ids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sample_order(self):
        """Return the samples that have been requested and are in the file, if
        none have been requested then all the samples will be returned.
        """
        if self.sample_idx is None:
            return self.samples
        return self.samples[self.sample_idx]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def reader(self):
        """Return the API reader object used to interact with the specific
        file type.
        """
        return self._reader

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def is_open(self):
        """Is the reader open ``True`` if yes ``False`` if no.
        """
        return self._opened

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def check_variant_alleles(effect_allele, other_allele, ref_allele,
                              alt_alleles, match_alleles=False,
                              var_object=None):
        """Check the required alleles are compatible with the alleles of a
        potential match variant.

        Parameters
        ----------
        effect_allele : `str`
            The effect allele of the variant that is being searched in the
            input file.
        other_allele : `str`
            The non-effect allele of the variant that is being searched in the
            input file.
        ref_allele : `str`
            The reference allele of a potential match variant in the input
            file.
        alt_alleles : `list` of `str`
            The alterate alleles of the potential match variant in the input
            file.
        match_alleles : `bool`, optional, default: `False`
            Only consider a match if ``effect_allele`` == ``ref_allele``,
            otherwise allele flipping is allowed. Note that, the reference
            allele is evaluated as the first allele of a bi-alleilic set that
            are in the allele order. So, if the user wants A/G and the match
            has the possible alleles T,C,A,G (representing genotypes 0,1,2,3)
            - then these have the bi-alleilic combinations of (T,C), (T, A),
            (T, G), (C, A), (C, G), (A, G). So if match_alleles is True, then
            this would match on A/G. However, if the user matched G/A and match
            alleles is True then this would result in no match. In effect,
            irrespective of the number of alleles, all combinations are treated
            as if they are a pair of bi-alleilic alleles in the genotype file.
        var_object : `any`, optional, default: `NoneType`
            An optional variant object to store alongside a matching variant
            set.

        Returns
        -------
        allele_matches : `list` of `tuple`
            Match alleles, each tuple has the structure (match-pair,
            is_flipped, var_object), where match_pair is a bi-allelic tuple of
            ((idx, allele),(idx, allele)) where idx is the position in the
            match variant alleles string, i.e. if the match variant was A
            (ref),T,C,G (alts), then the idxs are 0, (ref) 1,2,3 (alts). If
            there are no matches then this will be an empty list.
        """
        max_combs = 2
        alleles = (effect_allele, other_allele)
        alleles_sort = tuple(sorted(alleles))

        match_pos = []
        gen_alleles = [
            (idx, a) for idx, a in enumerate([ref_allele] + alt_alleles)
        ]
        for j in itertools.combinations(gen_alleles, max_combs):
            match_key = tuple([k[1] for k in sorted(j, key=lambda x: x[1])])
            if alleles_sort == match_key:
                j = list(j)
                is_flipped = j[0][1] != alleles[0]
                allele_order = [
                    idx for idx, i in enumerate(j) if i[1] == effect_allele
                ][0]
                o = [j.pop(allele_order)] + j
                if match_alleles is False:
                    match_pos.append([o, is_flipped, var_object])
                elif match_alleles is True and is_flipped is False:
                    match_pos.append([o, is_flipped, var_object])
        return match_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def proc_genotype(gtypes, alleles, samples=slice(None), dtype=np.float64,
                      rerank=False):
        """Extract the genotypes that match the required alleles and set those
        that don't to numpy.nan.

        Parameters
        ----------
        gtypes : `numpy.array`
            The variant containing the genotypes. Must have shape
            (nsamples, ploidy).
        alleles : `tuple` of `tuple`
            Alleles that we want to extract from the variant
            genotypes (which could be multi-alleilic). The nested tuples
            contain the allele number at ``0`` and the allele string at ``1``.
        samples : `list` or `numpy.array` or `slice`, optional, default:
        `slice(NoneType)`
            An array of index positions for the samples that we want to
            extract. The default is to keep all of them.
        dtype : `numpy.float`, optional, default: `numpy.float64`
            The datatype for the processed genotypes.
        rerank : `bool`, optional, default: `False`
            Re-rank the genotypes, so they are consecutive order.

        Returns
        -------
        genotypes : `numpy.array`
            An array with the shape (nsamples, ploidy). Missing genotypes
            (i.e. missing or other unwanted allele combinations) are set to
            ``numpy.nan``. The if rerank is True, alleles are standardised
            to 0,1 (i.e. you might have required 1,2 originally but you get
            0,1 back).

        Notes
        -----
        Please note this has only been tested with diploid genotypes
        """
        wanted_alleles = np.array([idx for idx, i in alleles])
        gtypes = np.array(
            gtypes[samples, ],
            dtype=dtype
        )
        no_match_genos = ~np.all(np.isin(gtypes, wanted_alleles), axis=1)
        gtypes[no_match_genos] = np.full(gtypes.shape[1], np.nan)

        if rerank is True:
            gtype_shape = gtypes.shape
            gtypes = np.array(
                (rankdata(gtypes, method="dense") - 1), dtype=dtype
            )
            gtypes[gtypes >= wanted_alleles.size] = np.nan
            gtypes.shape = gtype_shape
        return gtypes


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BgenReader(_BaseGenoReader):
    """A wrapper around ``bgen_reader`` to provide a simpler interface for
    queries.

    Parameters
    ----------
    infile : `str`
        The path to the bgen file.
    sample_ids : `NoneType` or `list` of `str`
        The sample IDs we are interested in. ``NoneType`` means all of them.
    dtype : `numpy.float64` or `numpy.float64` or `numpy.float64`, optional,
    default: `numpy.float64`
        The data types for the returned dosages. If you know the precision of
        your input file you can make ths smaller to save memory.
    **kwargs
        Any keyword arguments to be passed to ``cyvcf2.VCF``, when opening
        the file.

    Notes
    -----
    Currently, the bgen files must only have bi-allelic sites and be from
    diploid organisms.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples(self):
        """Return all the samples in the file being read (`list` of `str`).
        """
        return np.array(self._reader.samples)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An opener for the bgen file. If called on an open file, it will
        ignore the call and not error out.

        Raises
        ------
        ValueError
            If there are multi-allelic sites or if the organsim is not diploid
            or if none of the requested samples are in the bgen file.

        Notes
        -----
        This will issue user warnings for subsets of samples that are not in
        the bgen file.
        """
        if self._opened is False:
            self._reader = open_bgen(self._infile, **self._kwargs)
            super().open()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close an open bgen, if the bgen is not open then this will just be
        ignored and no error will be generated.
        """
        if self._opened is True:
            self._reader.close()
            del(self._reader)
            super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def pause(self):
        """Close the reader but keep initialisation
        """
        if self._is_paused is False:
            self._reader.close()
            del(self._reader)
            self._is_paused = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def resume(self):
        """Close the reader but keep initialisation
        """
        if self._is_paused is True:
            self._reader = open_bgen(self._infile, **self._kwargs)
            self._is_paused = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_phased_dosage(genotypes, alleles, nalleles, ncombs, ploidy=2):
        """

        """
        wanted = [i[0] for i in alleles]
        check = ~np.isin(np.arange(0, nalleles), wanted)
        ploidy = int(ncombs / nalleles)
        genotypes = genotypes[:, :ncombs]
        # genotypes.shape = (
        #     genotypes.shape[0], int(ncombs/ploidy), ploidy
        # )
        genotypes.shape = (
            genotypes.shape[0], ploidy, int(ncombs/ploidy)
        )
        ac = np.sum(genotypes, axis=1)
        missing = np.any(ac[:, check] > 0, axis=1)
        ac = ma.array(ac[:, alleles[0][0]])
        ac[missing] = np.nan
        ac[np.isnan(ac)] = ma.masked
        return ac

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_unphased_dosage(genotypes, alleles, nalleles, ncombs, ploidy):
        """

        """
        wanted = [i[0] for i in alleles]
        check = ~np.isin(np.arange(0, nalleles), wanted)
        geno_order = get_genotypes([ploidy], nalleles)[0]
        count_classes = genotypes_to_allele_counts(
            geno_order
        )
        genotypes = genotypes[:, :ncombs]
        ac = genotypes.dot(count_classes)
        missing = np.any(ac[:, check] > 0, axis=1)
        ac = ma.array(ac[:, alleles[0][0]])
        ac[missing] = np.nan
        ac[np.isnan(ac)] = ma.masked
        return ac

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_variant(self, chr_name, start_pos, effect_allele,
                    other_allele, match_alleles=False):
        """Determine if a variant is present in the data without getting any
        genotypes.

        Parameters
        ----------
        chr_name : `str`
            The chromosome for the reested variant
        start_pos : `int`
            Te start position for the requested variant
        ref_allele : `str`
            The reference allele for the requested variant
        alt_allele : `str`
            The alternate allele for the requested variant
        match_alleles : `bool`, optional, default: `False`
            Should the ref/alt alleles match exactly? The default behaviour is
            to allow the ref/alt alleles to be flipped (see notes).

        Returns
        -------
        is_present : `bool`
            Is the variant present in the data ``True`` is yes ``False`` is no.
        """
        idx = np.flatnonzero(
            (self._reader.chromosomes == chr_name) &
            (self._reader.positions == start_pos)
        )
        match_pos = []
        for idx_pos, v in enumerate(self._reader.allele_ids[idx]):
            alleles = v.split(',')
            phased = self._reader.phased[idx[idx_pos]]
            ncombs = self._reader.ncombinations[idx[idx_pos]]
            match_pos.extend(
                self.check_variant_alleles(
                    effect_allele, other_allele, alleles[0], alleles[1:],
                    match_alleles=match_alleles,
                    var_object=(
                        idx[idx_pos], ncombs, len(alleles), bool(phased)
                    )
                )
            )
        return len(match_pos) > 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_dosage(self, chr_name, start_pos, effect_allele,
                           other_allele, match_alleles=False):
        """Get the allele dosage of the ref_allele for a specific variant.

        Parameters
        ----------
        chr_name : `str`
            The chromosome for the reested variant
        start_pos : `int`
            Te start position for the requested variant
        ref_allele : `str`
            The reference allele for the requested variant
        alt_allele : `str`
            The alternate allele for the requested variant
        match_alleles : `bool`, optional, default: `False`
            Should the ref/alt alleles match exactly? The default behaviour is
            to allow the ref/alt alleles to be flipped (see notes).

        Notes
        -----
        By default, this will allow the ref/alt alleles to be flipped to match
        the alleles. For example, if the user requests ref=A,alt=G and cohort
        has ref=G,alt=A, then this is a match and the data is extracted. The
        allele counts are then flipped so that the count of the alt allele from
        the cohort is used.
        """
        idx = np.flatnonzero(
            (self._reader.chromosomes == chr_name) &
            (self._reader.positions == start_pos)
        )
        match_pos = []
        for idx_pos, v in enumerate(self._reader.allele_ids[idx]):
            alleles = v.split(',')
            phased = self._reader.phased[idx[idx_pos]]
            ncombs = self._reader.ncombinations[idx[idx_pos]]
            match_pos.extend(
                self.check_variant_alleles(
                    effect_allele, other_allele, alleles[0], alleles[1:],
                    match_alleles=match_alleles,
                    var_object=(
                        idx[idx_pos], ncombs, len(alleles), bool(phased)
                    )
                )
            )

        match_idx = [i[2][0] for i in match_pos]
        genos, ploidies = self._reader.read(
            (self._sample_idx, match_idx), num_threads=1, return_ploidies=True
        )
        for i in range(genos.shape[1]):
            g = genos[:, i]
            p = ploidies[i]
            alleles, is_flipped, var_obj = match_pos[i]
            idx, ncombs, nalleles, phased = var_obj
            if phased is True:
                ac = self.get_phased_dosage(
                    g, alleles, nalleles, ncombs
                )
            else:
                ac = self.get_unphased_dosage(
                    g, alleles, nalleles, ncombs, p[0]
                )
            yield ac, is_flipped


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VcfReader(object):
    """A wrapper around ``pysam.VariantFile`` to provide a simpler interface
    for queries.

    Parameters
    ----------
    infile : `str`
        The path to the bgen file.
    sample_ids : `NoneType` or `list` of `str`
        The sample IDs we are interested in. ``NoneType`` means all of them.
    **kwargs
        Any keyword arguments to be passed to ``bgen_reader.open_bgen``

    Notes
    -----
    Currently, the VCF files must be from diploid organisms.
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, sample_ids=None, **kwargs):
        self._infile = infile
        self._sample_ids = sample_ids
        self._kwargs = kwargs
        self._opened = False
        self._reader = None

        # The genotype formats that we can currently deal with in the order that
        # we want them (`list` of `str`)
        self.formats = {'GP': self.extract_gp, 'GT': self.extract_gt}
        self.available_formats = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def extract_gp(self, genos):
        """
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def extract_gt(self, genos, combos):
        """
        """
        gt = (
            (combos[0][0], combos[0][0]),
            (combos[0][0], combos[1][0]),
            (combos[1][0], combos[0][0]),
            (combos[1][0], combos[1][0])
        )
        matches = []
        for i in genos:
            try:
                gts = i['GT']
                matches.append(gts) if gts in gt else matches.append(np.nan)
            except KeyError:
                matches.append((np.nan, np.nan))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An opener for the bgen file. If called on an open file, it will
        ignore the call and not error out.

        Raises
        ------
        ValueError
            If there are multi-allelic sites or if the organsim is not diploid
            or if none of the requested samples are in the bgen file.

        Notes
        -----
        This will issue user warnings for subsets of samples that are not in
        the bgen file.
        """
        if self._opened is False:
            self.available_formats = {}
            self._reader = pysam.VariantFile(self._infile, **self._kwargs)

            for i in self.formats:
                try:
                    self.available_formats[i] = self.formats[i]
                except KeyError:
                    pass

            if len(self.available_formats) == 0:
                raise TypeError("no recognised genotype formats")

            self._opened = True
            print("CALLING SET SAMPLES")
            self.set_samples(self._sample_ids)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_samples(self, samples):
        self._sample_ids = samples
        print(len(self._sample_ids))
        if self._opened is True:
            self._sample_idx = None
            if self._sample_ids is not None:
                self._sample_idx = np.flatnonzero(
                    np.isin(self.samples, self._sample_ids)
                )
                if len(self._sample_idx) == 0:
                    raise ValueError(
                        "0 requested samples in the file: '{0}'".format(
                            self.infile
                        )
                    )
                elif len(self._sample_idx) != len(self._sample_ids):
                    missing = len(self._sample_ids) - len(self._sample_idx)
                    warnings.warn(
                        "{0} sample(s) not in file: '{1}'".format(
                            missing, self.infile
                        )
                    )
                print(len(self._sample_idx))
        else:
            raise IOError("file not open: {0}".format(self.infile))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close an open bgen, if the bgen is not open then this will just be
        ignored and no error will be generated.
        """
        if self._opened is True:
            self._reader.close()
            self._reader = None
            self._opened = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def infile(self):
        """Return the input file name.
        """
        return self._infile

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples(self):
        """Return all the samples in the file being read (`list` of `int`).
        """
        return np.array(self._reader.header.samples)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sample_idx(self):
        """Return the sample index positions of the any requested samples
        (or the whole lot of none are requested) (`list` of `int`).
        """
        return self._sample_idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sample_order(self):
        """TODO: fill docstring
        """
        if self.sample_idx is None:
            return self.samples
        return self.samples[self.sample_idx]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def reader(self):
        """Return the bgen reader object used to interact with the bgen file
        (`bgen_reader`).
        """
        return self._reader

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def is_open(self):
        """Return the bgen reader object used to interact with the bgen file.
        """
        return self._opened

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_dosage(self, chr_name, start_pos, ref_allele, alt_allele,
                           match_alleles=False):
        """Get the allele dosage of the ref_allele for a specific variant.

        Parameters
        ----------
        chr_name : `str`
            The chromosome for the reested variant
        start_pos : `int`
            Te start position for the requested variant
        ref_allele : `str`
            The reference allele for the requested variant
        alt_allele : `str`
            The alternate allele for the requested variant
        match_alleles : `bool`, optional, default: `False`
            Should the ref/alt alleles match exactly? The default behaviour is
            to allow the ref/alt alleles to be flipped (see notes).

        Notes
        -----
        By default, this will allow the ref/alt alleles to be flipped to match
        the alleles. For example, if the user requests ref=A,alt=G and cohort
        has ref=G,alt=A, then this is a match and the data is extracted. The
        allele counts are then flipped so that the count of the alt allele from
        the cohort is used.
        """
        alleles = (ref_allele, alt_allele)
        alleles_sort = tuple(sorted(alleles))

        match_pos = []
        for i in self._reader.fetch(
            contig=chr_name,
            start=start_pos - 1,
            stop=start_pos + max(len(ref_allele), len(alt_allele)) - 1
        ):
            gen_alleles = [
                (idx, j) for idx, j in enumerate([i.ref] + list(i.alts))
            ]
            for j in itertools.combinations(gen_alleles, 2):
                match_key = tuple(
                    [k[1] for k in sorted(j, key=lambda x: x[1])]
                )
                if alleles_sort == match_key:
                    if match_alleles is False:
                        match_pos.append([j, i])
                    elif match_alleles is True and j[0][1] == alleles[0]:
                        match_pos.append([j, i])

        for combos, var_obj in match_pos:
            matching_samples = [
                i for idx, i in enumerate(var_obj.samples.values())
                if idx in self._sample_idx
            ]
            for var_format, parser in self.available_formats.items():
                try:
                    var_obj.format[var_format]
                    parser(matching_samples, combos)
                    break
                except KeyError:
                    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CyVcfReader(_BaseGenoReader):
    """A wrapper around ``cyvcf2`` to provide a simpler interface
    for VCF queries.

    Parameters
    ----------
    infile : `str`
        The path to the bgen file.
    sample_ids : `NoneType` or `list` of `str`
        The sample IDs we are interested in. ``NoneType`` means all of them.
    dtype : `numpy.float64` or `numpy.float64` or `numpy.float64`, optional,
    default: `numpy.float64`
        The data types for the returned dosages. If you know the precision of
        your input file you can make ths smaller to save memory.
    **kwargs
        Any keyword arguments to be passed to ``cyvcf2.VCF``, when opening
        the file.

    Notes
    -----
    Currently, the VCF files must be from diploid organisms.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # The genotype formats that we can currently deal with in the order
        # that we want them (`list` of `str`)
        self.formats = {
            'GP': self.extract_ad_from_gp,
            'GT': self.extract_ac_from_gt
        }
        self.available_formats = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples(self):
        """Get all the samples in the VCF file
        """
        return np.array(self._reader.samples)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def extract_ad_from_gp(self, variant, alleles, samples=slice(None),
                           dtype=np.float64):
        """Extract the reference allele dosages from the GP format fields.

        Parameters
        ----------
        variant : `cyvcf2.cyvcf2.Variant`
            The variant containing the genotypes.
        samples : `numpy.array`
            An array of index positions for the samples that we want to
            extract
        combos : `tuple` of `tuple`
            Biallelic alleles that we want to extract from the variant
            genotypes (which could be multi-alleilic). The nested tuples
            contain the allele number at ``0`` and the allele at ``1``.

        Returns
        -------
        allele_counts : `numpy.ma.core.MaskedArray`
            Allele counts for the dosage of reference alleles. This is a
            float 16 masked array with the shape (nsamples, 1). Missing counts
            (i.e. missing or other unwanted allele combinations) are set to
            ``numpy.nan``. The dosage will be between 0-2.
        """
        nalleles = len(variant.ALT) + 1
        genotypes = self.extract_gp(
            variant, alleles, samples=samples,
        )
        wanted = [i[0] for i in alleles]
        check = ~np.isin(np.arange(0, nalleles), wanted)
        geno_order = get_genotypes([variant.ploidy], nalleles)[0]
        count_classes = genotypes_to_allele_counts(
            geno_order
        )
        genotypes = genotypes[:, :len(count_classes)]
        ac = genotypes.dot(count_classes)
        missing = np.any(ac[:, check] > 0, axis=1)
        ac = ma.array(ac[:, alleles[0][0]], dtype=dtype)
        # ac = ma.array(ac[:, alleles[0][0]])
        ac[missing] = np.nan
        ac[np.isnan(ac)] = ma.masked
        return ac
        #
        # nans = np.any(np.isnan(genos), axis=1)
        # ac = geno_to_ac(genos)
        # ac[nans] = np.nan
        # return ma.array(ac, mask=nans)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def extract_gp(self, variant, alleles, samples=slice(None),
                   dtype=np.float64):
        """Extract the genotypes probabilities from the GP format fields.

        Parameters
        ----------
        variant : `cyvcf2.cyvcf2.Variant`
            The variant containing the genotypes.
        samples : `numpy.array`
            An array of index positions for the samples that we want to
            extract
        combos : `tuple` of `tuple`
            Biallelic alleles that we want to extract from the variant
            genotypes (which could be multi-alleilic). The nested tuples
            contain the allele number at ``0`` and the allele at ``1``.

        Returns
        -------
        genotypes : `numpy.array`
            An float 16 array with the shape (nsamples, 3). The 3 columns are
            p(AA), p(AB), p(BB).

        Notes
        -----
        """
        genos = np.array(
            variant.format('GP')[samples],
            dtype=dtype
        )
        # if genos.shape[1] != 3:
        #     raise TypeError("can't handle multi-alleilic GP fields")
        return genos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def extract_ac_from_gt(self, variant, alleles, samples=slice(None),
                           dtype=np.float64):
        """Extract the genotypes from a GT field that match the required
        alleles and set those that don't to numpy.nan. Then get counts of each
        allele type.

        Parameters
        ----------
        variant : `cyvcf2.cyvcf2.Variant`
            The variant containing the genotypes.
        alleles : `tuple` of `tuple`
            Alleles that we want to extract from the variant
            genotypes (which could be multi-alleilic). The nested tuples
            contain the allele number at ``0`` and the allele string at ``1``.
        samples : `list` or `numpy.array` or `slice`, optional,
        default: `slice(NoneType)`
            An array of index positions for the samples that we want to
            extract. The default is to keep all of them.
        dtype : `numpy.float`, optional, default: `numpy.float64`
            The datatype for the processed genotypes.

        Returns
        -------
        allele_counts : `numpy.ma.core.MaskedArray`
            Allele counts for the numbers of reference alleles. This is a
            float 16 masked array with the shape (nsamples, 1). Missing counts
            (i.e. missing or other unwanted allele combinations) are set to
            ``numpy.nan``. The counts will be between 0-2.
        """
        genos = self.extract_gt(variant, alleles, samples=samples,
                                dtype=dtype)
        nans = np.all(np.isnan(genos), axis=1)
        ac = np.array(
            np.count_nonzero(genos == alleles[0][0], axis=1),
            dtype=dtype
        )
        ac[nans] = np.nan
        return ma.array(ac, mask=nans)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def extract_gt(self, variant, alleles, samples=slice(None),
                   dtype=np.float64, rerank=False):
        """Extract the genotypes from the GT format fields.

        Parameters
        ----------
        variant : `cyvcf2.cyvcf2.Variant`
            The variant containing the genotypes.
        alleles : `tuple` of `tuple`
            Alleles that we want to extract from the variant
            genotypes (which could be multi-alleilic). The nested tuples
            contain the allele number at ``0`` and the allele string at ``1``.
        samples : `list` or `numpy.array` or `slice`, optional, default:
        `slice(NoneType)`
            An array of index positions for the samples that we want to
            extract. The default is to keep all of them.
        dtype : `numpy.float`, optional, default: `numpy.float64`
            The datatype for the processed genotypes.
        rerank : `bool`, optional, default: `False`
            Re-rank the genotypes, so they are consecutive order.

        Returns
        -------
        genotypes : `numpy.array`
            An array with the shape (nsamples, ploidy). Missing genotypes
            (i.e. missing or other unwanted allele combinations) are set to
            ``numpy.nan``. If rerank is True the alleles are standardised
            to 0,1 (i.e. you might have required 1,2 originally but you get
            0,1 back).

        Notes
        -----
        Please note this has only been tested with diploid genotypes
        """
        ploidy = variant.ploidy
        # gtypes = variant.genotype.array()
        # gtypes = np.array(
        #     variant.genotype.array()[samples, :ploidy],
        #     dtype=dtype
        # )
        gtypes = np.array(
            variant.genotype.array()[:, :ploidy],
            dtype=dtype
        )
        # print(gtypes)
        # print(gtypes.shape)
        # raise
        return self.proc_genotype(
            gtypes, alleles, samples=samples, dtype=dtype, rerank=rerank
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def pause(self):
        """Close the reader but keep initialisation
        """
        self._reader.close()
        self._reader = None
        self._is_paused = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def resume(self):
        """Close the reader but keep initialisation
        """
        self._reader = cyvcf2.VCF(self.infile)
        self._is_paused = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An opener for the bgen file. If called on an open file, it will
        ignore the call and not error out.

        Raises
        ------
        ValueError
            If there are multi-allelic sites or if the organsim is not diploid
            or if none of the requested samples are in the bgen file.

        Notes
        -----
        This will issue user warnings for subsets of samples that are not in
        the bgen file.
        """
        if self._opened is False:
            self.available_formats = {}
            self._reader = cyvcf2.VCF(self.infile)
            super().open()
            formats = [
                i['ID'] for i in self._reader.header_iter()
                if i['HeaderType'] == 'FORMAT'
            ]

            for format_code, parser in self.formats.items():
                if format_code in formats:
                    self.available_formats[format_code] = parser

            if len(self.available_formats) == 0:
                raise TypeError("no recognised genotype formats")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close an open bgen, if the bgen is not open then this will just be
        ignored and no error will be generated.
        """
        if self._opened is True and self._is_paused is False:
            self._reader.close()
            self._reader = None
            super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_variant(self, chr_name, start_pos, effect_allele,
                    other_allele, match_alleles=False):
        """Determine if a variant is present in the data without getting any
        genotypes.

        Parameters
        ----------
        chr_name : `str`
            The chromosome for the reested variant
        start_pos : `int`
            Te start position for the requested variant
        ref_allele : `str`
            The reference allele for the requested variant
        alt_allele : `str`
            The alternate allele for the requested variant
        match_alleles : `bool`, optional, default: `False`
            Should the ref/alt alleles match exactly? The default behaviour is
            to allow the ref/alt alleles to be flipped (see notes).

        Returns
        -------
        is_present : `bool`
            Is the variant present in the data ``True`` is yes ``False`` is no.
        """
        end_pos = start_pos + max(
            len(effect_allele),
            len(other_allele)) - 1

        match_pos = []
        for v in self._reader(f"{chr_name}:{start_pos}-{end_pos}"):
            match_pos.extend(
                self.check_variant_alleles(
                    effect_allele, other_allele, v.REF, v.ALT,
                    match_alleles=match_alleles, var_object=v
                )
            )
        return len(match_pos) > 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_dosage(self, chr_name, start_pos, effect_allele,
                           other_allele, match_alleles=False):
        """Get the allele dosage of the ref_allele for a specific variant.

        Parameters
        ----------
        chr_name : `str`
            The chromosome for the reested variant
        start_pos : `int`
            Te start position for the requested variant
        ref_allele : `str`
            The reference allele for the requested variant
        alt_allele : `str`
            The alternate allele for the requested variant
        match_alleles : `bool`, optional, default: `False`
            Should the ref/alt alleles match exactly? The default behaviour is
            to allow the ref/alt alleles to be flipped (see notes).

        Notes
        -----
        By default, this will allow the ref/alt alleles to be flipped to match
        the alleles. For example, if the user requests ref=A,alt=G and cohort
        has ref=G,alt=A, then this is a match and the data is extracted. The
        allele counts are then flipped so that the count of the alt allele from
        the cohort is used.
        """
        alleles = (effect_allele, other_allele)

        end_pos = start_pos + max(
            len(effect_allele),
            len(other_allele)) - 1

        match_pos = []
        for v in self._reader(f"{chr_name}:{start_pos}-{end_pos}"):
            match_pos.extend(
                self.check_variant_alleles(
                    effect_allele, other_allele, v.REF, v.ALT,
                    match_alleles=match_alleles, var_object=v
                )
            )
        for alleles, is_flipped, var_obj in match_pos:
            if var_obj.ploidy > 2:
                warnings.warn("ploidy >2 has not been tested")
            for var_format, parser in self.available_formats.items():
                if var_format in var_obj.FORMAT:
                    ac = parser(var_obj, alleles, samples=self.sample_idx,
                                dtype=self.dtype)
                    yield ac, is_flipped
                    break


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ProviderReader(object):
    """A file handler that is specific for VCF files. This handles the basic
    interaction with the vcf files used to extract genotype data.

    Parameters
    ----------
    provider
    sample_ids
    **kwargs

    Notes
    -----
    A pre-requisite for using this is that your sample IDs must be encoded in
    your bgen file.
    """
    READER_CLASS = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, provider, sample_ids=None, nopen=1, **kwargs):
        if nopen <= 0:
            raise ValueError("nopen should be > 0")
        # The number of files that are allowed to be open at one time
        self._nopen = nopen

        # The keyword arguments that will be passed to the specific reader
        # class everytime it is opened
        self._open_kwargs = kwargs

        # Will hold all of the readers that are currently opened with their
        # file name as the key and the reader object as a value.
        self._open = dict()

        # Holds the provider class from genomic config, that will provide file
        # paths based on requested coorinates
        self._provider = provider

        # Holds any specific samples that the user has requested
        self._sample_ids = sample_ids

        # Make sure the sample IDs are unique
        # Make sure the sample IDs are unique
        if self._sample_ids is not None:
            if len(set(self._sample_ids)) != len(self._sample_ids):
                prev_sid = ""
                reps = set([])
                for i in sorted(self._sample_ids):
                    if prev_sid != i:
                        reps.add(i)
                raise ValueError(
                    f"requested sample IDs must be unique: {','.join(reps)}"
                )

        # Will hold all readers added to the provider reader
        self._all_readers = dict()

        # This will hold the sample IDs that will act as a reference that all
        # new sample IDs in a file will be validated against for each new file
        # opened, this will be initialised to the sample order of the first
        # file opened. Currently it is designed to error out if they are
        # different
        self._ref_sample_order = None

        # This will hold the sample IDs that we have requested that are
        # actually available in the readers
        self._available_samples = None

        # A flag to indicate if the provider has been opened
        self._opened = False

        # Will hold files that do not have the same sample sets as the lead
        # file tested, these are masked
        self._masked_files = set([])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the provider, if validate samples is True, this will also open
        all the readers to get a list of the samples they contain.
        """
        # TODO: Make sample validation optional, i.e. just validate one and
        # assume all files have the same sample set (but check when they are
        # opened for the first time)
        if self._opened is False:
            self._validate_samples()
            self._opened = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close all open files.
        """
        if self._opened is True:
            for k, v in self._open.items():
                reader = self.close_reader(v)
                if reader is not None:
                    reader.close()
            self._open = dict()
            self._opened = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def provider(self):
        """Return the bgen file provider
        """
        return self._provider

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples(self):
        """Return all the samples that are in the files.
        """
        return self._ref_sample_order

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sample_order(self):
        """Return the samples IDs that the user will get, this could be all of
        them (if none are requested) or the subset of requested samples that
        are in all of the files.
        """
        return self._available_samples

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples_requested(self):
        """Return the samples that the user has requested. This will be an
        empty list if non have been requested.
        """
        return self._sample_ids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _validate_samples(self):
        """When this is called nothing has been set up.
        """
        for pos, file_path in self._provider.to_list():
            chr_name, start_pos, end_pos = pos
            try:
                # Not seen the file before, then open it
                reader = self.READER_CLASS(
                    file_path, sample_ids=self._sample_ids,
                    **self._open_kwargs
                )

                # Check the samples to make sure they are the same has the
                # index sample set
                self.check_reader_samples(reader)
                self._all_readers[file_path] = reader
            except IndexError as e:
                allowed = "the order of samples is different in the cohort"
                if e.args[0] != allowed:
                    raise

                if start_pos is not None:
                    warnings.warn(
                        "Will mask coordinates: "
                        "{0}:{1}-{2}".format(chr_name, start_pos, end_pos)
                    )
                else:
                    warnings.warn(
                        "Will mask chromosome:"
                        " {0}".format(chr_name)
                    )
                self._masked_files.add(file_path)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def check_reader_samples(self, reader):
        """Check the readers samples.

        Parameter
        ---------
        reader : `bio_misc.ipd.readers._BaseReader`
            The reader object to check. Note, that the reader should be closed
            when passed and will always be closed when this has finished.
        """
        err_msg = "the order of samples is different in the cohort"

        # Open the reader
        reader.open()
        try:
            # Will raise a TypeError if ref sample order not initialised
            # (i.e. on first call)
            len(self._ref_sample_order)

            if np.any(self._ref_sample_order != reader.samples):
                raise IndexError(err_msg)
        except ValueError as e:
            # One of the arrays is shorted/longer then the other
            if "shape mismatch" in e.args[0]:
                raise IndexError(err_msg) from e
            else:
                # Not sure what the error is so we raise on and do not handle
                raise
        except TypeError:
            # When called for the first time the reference sample order will
            # not be initialised, this handles that even and initialises
            if self._ref_sample_order is not None:
                raise
            self._ref_sample_order = reader.samples.copy()
            self._available_samples = reader.sample_order.copy()
        finally:
            reader.pause()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open_reader(self, file_path):
        """Open a reader object that is associated with the file path.

        Parameters
        ----------
        file_path : `str`
            The path to the file you want to open.

        Returns
        -------
        reader : `bio-misc.ipd.readers._BaseReader`
            The reader that uses the file path.

        Notes
        -----
        The reader object is also stored internally so it can be closed
        automatically if the allowed number of open files is exceeded.
        """
        # if already open then just return
        if file_path in self._open:
            return self._open[file_path]

        # If we have reached the open limit
        if len(self._open) > self._nopen:
            for i in self._open.keys():
                self.close_reader(i)
                break

        try:
            # Attempt to get it, all reader should have been created when the
            # provider reader was opened
            reader = self._all_readers[file_path]
        except KeyError as e:
            if self._opened is False:
                raise IOError("reader not open")

            # Could be masked (i.e. different sample order), if not then we
            # raise
            if file_path not in self._masked_files:
                raise KeyError(f"unknown reader for: {file_path}") from e

        reader.resume()
        self._open[file_path] = reader
        return reader

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close_reader(self, file_path):
        """Close a reader object that is associated with the file path.

        Parameters
        ----------
        file_path : `str`
            The path to the file you want to open.

        Returns
        -------
        reader : `bio-misc.ipd.readers._BaseReader`
            The closed reader that uses the file path, this could also return
            ``NoneType`` if the reader is not registered as open.
        """
        try:
            reader = self._open.pop(file_path)
            reader.pause()
            return reader
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_variant(self, chr_name, start_pos, ref_allele, alt_allele):
        """Get variant dosages for the requested region.

        Parameters
        ----------
        chr_name : `str`
            The chromosome name.
        start_pos : `int`
            The start position.
        ref_allele : `str`
            The reference allele.
        alt_allele
            The alternate allele.

        Yields
        ------
        row : `numpy.ndarray`
            A 1D array of allele dosage data. Where the dosage is represented
            as the dosage of the reference allele.
        """
        infiles = self._provider.get_files(chr_name, start_pos, start_pos)
        for f in infiles:
            reader = self.open_reader(f)
            if reader.has_variant(
                chr_name, start_pos, ref_allele, alt_allele
            ) is True:
                return True
        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_dosage(self, chr_name, start_pos, ref_allele, alt_allele):
        """Get variant dosages for the requested region.

        Parameters
        ----------
        chr_name : `str`
            The chromosome name.
        start_pos : `int`
            The start position.
        ref_allele : `str`
            The reference allele.
        alt_allele
            The alternate allele.

        Yields
        ------
        row : `numpy.ndarray`
            A 1D array of allele dosage data. Where the dosage is represented
            as the dosage of the reference allele.
        """
        infiles = self._provider.get_files(chr_name, start_pos, start_pos)
        for f in infiles:
            reader = self.open_reader(f)
            for row in reader.get_variant_dosage(
                chr_name, start_pos, ref_allele, alt_allele
            ):
                yield row


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VcfProviderReader(ProviderReader):
    """A file handler that is specific for VCF files. This handles the basic
    interaction with the vcf files used to extract genotype data.

    Parameters
    ----------
    provider
    sample_ids
    **kwargs

    Notes
    -----
    A pre-requisite for using this is that your sample IDs must be encoded in
    your bgen file.
    """
    READER_CLASS = CyVcfReader


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BgenProviderReader(ProviderReader):
    """A file handler that is specific for BGEN files. This handles the basic
    interaction with the bgen files used to extract genotype data.

    Parameters
    ----------
    provider
    sample_ids
    **kwargs

    Notes
    -----
    A pre-requisite for using this is that your sample IDs must be encoded in
    your bgen file.
    """
    READER_CLASS = BgenReader


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_colex_order(genotypes):
    """Get the colex order for a set of genotype combinations. This is the
    order found in bgen files.

    Parameters
    ----------
    genotypes : `iterable` of `iterable`
        The genotypes to put into colex order.

    Returns
    -------
    colex_genotypes : `iterable` of `iterable`
        The genotypes in colex order.

    Notes
    -----
    For more information on the colex order
    `see here
    <https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order>`_
    """
    return sorted(
        genotypes,
        key=lambda x: tuple(reversed(x))
    )


def get_genotypes(ploidy, nalleles):
    g = [get_colex_order(_make_genotype(p, 1, nalleles)) for p in ploidy]
    # g = sorted([list(reversed(i)) for i in g])
    # g = [list(reversed(i)) for i in g]
    return g


def genotypes_to_allele_counts(genotypes):
    nalleles = genotypes[-1][0]
    counts = []
    for g in genotypes:
        count = [0] * nalleles
        for gi in g:
            count[gi - 1] += 1
        counts.append(count)
    return counts


def _make_genotype(ploidy, start, end):
    tups = []
    if ploidy == 0:
        return tups
    if ploidy == 1:
        return [[i] for i in range(start, end + 1)]
    for i in range(start, end + 1):
        t = _make_genotype(ploidy - 1, i, end)
        for ti in t:
            tups += [[i] + ti]
    return tups
