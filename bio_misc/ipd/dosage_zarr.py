"""Module to load allele dosage/count data into a zarr array
"""
from bio_misc.ipd import common
# from bio_misc.common import (
#     utils,
#     constants as con,
#     arguments,
#     log
# )
# from bio_misc.format import uni_id
from tqdm import tqdm
# from genomic_config import genomic_config
# import itertools as it
import numpy as np
import numpy.ma as ma
# import pandas as pd
# import argparse
# import sys
# import os
import math
# import warnings
import multiprocessing as mp
import zarr
# import pprint as pp
# import tempfile
from numcodecs import blosc

# For use in zarr with multiprocessing, to stop blocking
blosc.use_threads = False
# np.set_printoptions(threshold=sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_dosage_array(invars, file_name, cohort, verbose=False, processes=2,
                     chunk_rows=1000, lock=False, dtype=np.float64,
                     load_size=100, return_missing=False):
    """Load allele dosages from the ``invars`` into a zarr array.

    Parameters
    ----------
    invars : `list` of `dict`
        The input variants. Each dict should have the keys ``chr_name``,
        ``start_pos``, ``effect_allele``, ``other_allele``.
    file_name : `str`
        The location of the zarr_array. Please note, that although this is
        referred to as file name it actually represents a directory that will
        contain the individual zarr chunks. If it does not exist it will be
        created and if it is already present it will be wiped and overridden.
    cohort : `dict`
        Description of the cohort that will be used for the allele dosages. The
        dict should have the keys, ``cohort_no`` (int), ``cohort_name`` (str),
        ``leading_zero`` (bool), ``samples`` (``list`` of ``str``, empty for no
        samples), ``sample_section`` (``str`` or ``NoneType`` - for no sample
        config section).
    verbose : `int` or `bool`, optional, default: `False`
        If verbose is > 1, then a progress monitor will be displayed.
    assembly : `str`, optional, default: `GRCh38`
        The genome assembly of the cohort.
    chunk_rows : `int`, optional, default: `1000`
        The zarr array will be split into chunks containing a max of
        ``chunk_rows``. Each chunk will be a separate file in the zarr array
        and the separate processes will be aligned with the chunk_row
        boundaries to enable parallel loading with no synchronisation.
        Although, synchronisation can still be applied using ``lock=True``.
    processes : `int`, optional, default: `2`
        The number of separate processes that will be used to load the data
        into the zarr array. Please note, that depending on the chunk size the
        desired process count may not be reached.
    lock : `bool`, optional, default: `False`
        Apply process synchronisation when loading the zarr array in parallel.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The precision of the zarr array. Must be one of `numpy.float16`,
        `numpy.float32`, `numpy.float64`.
    load_size : `int`, optional, default:
        Within each process, the data is loaded into the zarr array in
        ``load_size`` batches.
    return_missing : `bool`, optional, default: `False`
        Return missing variants, these are variants that will be represented in
        the array but with missing data values.

    Notes
    -----
    Missing data is loaded as empty rows, so the array is guaranteed to have
    the shape (len(invars, len(samples)). The number of samples depends on the
    cohort and how it is specified.
    """
    # Make sure the variants that have been supplied are unique, this also
    # assigns an index value to each variant, that runs from 0 to len(invars)
    # - 1. Note that this is very important as it corresponds to how the
    # variants are loaded into the zarr array, so it should not be altered
    invars = common.unique_variants(invars)

    # Determine the shape of the final zarr array
    shape = get_array_shape(invars, cohort)
    # print(shape)
    # Now we get the invar boundaries for each processes, such that only whole
    # chunks will be in a single process, so no chunks are split over
    # processes, this enables unsynchronised parallel loading
    row_map = get_process_boundaries(invars, chunk_rows, processes)

    # This just sanity checks to make sure nothing is wrong, however, it is
    # possible for len(row_map) to be less than processes, depending on chunk
    # size and number of rows.
    if len(row_map) > processes:
        raise RuntimeError(
            "more boundaries than processes, this is probably a bug"
        )
    arr = zarr.open(
        file_name,
        mode='w',
        shape=shape,
        chunks=(chunk_rows, ),
        dtype=dtype,
        fill_value=np.nan,
        write_empty_chunks=True
    )

    # This queue is used to update the progress monitor with what is happening
    # in the processes (i.e. number of rows loaded)
    prog_queue = mp.Queue()
    proc_obj = []
    idx = 0
    # Now kick off the processes giving then the required variant batches
    # print("INVARS AND ROWMAP")
    # print(len(invars))
    # pp.pprint(row_map)
    # Add an index position for where the variant will be in the array
    invars = [(idx, i) for idx, i in enumerate(invars)]
    # sys.exit(1)
    for start, end in row_map:
        p = mp.Process(
            target=_load_process,
            args=(prog_queue, arr, invars[start:end], cohort),
            kwargs=dict(dtype=dtype, proc_idx=idx, load_size=load_size)
        )
        proc_obj.append(p)
        p.start()
        idx += 1

    pbar = tqdm(
        total=len(invars),
        desc="[info] loading variant dosage",
        unit=" variants",
        disable=common.verbose_disable(verbose)
    )

    missing_data = list()
    try:
        # Now we loop to we have the expected number of variants loaded
        expected = len(invars)
        got = 0
        while got < expected:
            nloaded, missing = prog_queue.get()
            missing_data.extend(missing)
            got += nloaded
            pbar.update(nloaded)

        # Everything should be loaded but we join just be be sure.
        # TODO: check the queue to make sure it is empty as if not I think
        #  this could block
        for i in proc_obj:
            i.join()
    finally:
        pbar.close()

    if return_missing is False:
        return arr
    return arr, missing_data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _load_process(prog_queue, arr, batch, cohort, dtype=np.float64,
                  load_size=100, proc_idx=0):
    """Load a batch of allele dosages into a zarr array.

    This is designed to be run as a separate process.

    Parameters
    ----------
    prog_queue : `multiprocessing.Queue`
        A progress queue that this process will send the number of loaded
        records to be displayed in the main process.
    arr : `zarr.array`
        A zarr array that will be the recipient of the extracted allele dosage
        data.
    invars : `list` of `dict`
        The input variants. Each dict should have the keys ``chr_name``,
        ``start_pos``, ``effect_allele``, ``other_allele``. Additionally (and
        important), these should also have an ``idx`` key that holds an integer
        index corresponding to the zarr array row that the dosage of that
        variant will be loaded. The batch should be sorted from lowest idx to
        highest and max(idx) - min(idx) should equal len(batch).
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohort : `dict`
        Description of the cohort that will be used for the allele dosages. The
        dict should have the keys, ``cohort_no`` (int), ``cohort_name`` (str),
        ``leading_zero`` (bool), ``samples`` (``list`` of ``str``, empty for no
        samples), ``sample_section`` (``str`` or ``NoneType`` - for no sample
        config section).
    assembly : `str`, optional, default: `GRCh38`
        The genome assembly of the cohort.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The precision of the zarr array. Must be one of `numpy.float16`,
        `numpy.float32`, `numpy.float64`.
    load_size : `int`, optional, default:
        Within each process, the data is loaded into the zarr array in
        ``load_size`` batches.
    proc_idx : `int`, optional, default: `0`
        The process index (i.e. the process number), not 100% required but
        might be useful for debugging.

    Raises
    ------
    IndexError
        If the end - start of a load batch is not equal to it's length, in
        short, something has gone wrong with the variant index values.
    """
    blosc.use_threads = False
    # Initialise the cohorts
    cohort.open()

    # Missing data variants
    missing = list()

    # Carve up the batch into sub batches of load_size
    load_batches = common.set_batches(batch, batch_size=load_size)
    # print("LOAD_BATCHES")
    # for i in load_batches:
    #     print(f"BATCH SIZE={len(i)}")
    nsamples = arr.shape[1]
    try:
        # Loop through the load size batches, so b is a list of (max)
        # load_size variants
        for b in load_batches:
            # Get the start and end indexes of where we are loading into the
            # zarr array
            start = b[0][0]
            end = b[-1][0] + 1
            # print("BATCH COORDS")
            # print(start)
            # print(end)
            # print(len(b))
            # Sanity check that nothing has gone wrong with the indexing as if
            # it does, this will really screw everything
            if (end - start) != len(b):
                raise IndexError(
                    "unexpected batch len: process '{proc_idx}'"
                    " '{end - start}' vs 'len(b)'"
                )

            # Will hold the extracted allele dosages for the load batch
            to_load = []
            for idx, v in b:
                try:
                    # Get the dosage for a variant
                    dose, flipped = common.get_variant_dosage(
                        cohort.reader, cohort.zero_func(v.chr_name),
                        v.start_pos,
                        v.effect_allele,
                        v.other_allele
                    )
                    to_load.append(dose)
                except IndexError:
                    # No data
                    to_load.append(
                        ma.array(
                            np.full(nsamples, np.nan, dtype=dtype),
                            mask=np.full(nsamples, True)
                        )
                    )
                    missing.append(v)
            arr[start:end] = to_load
            prog_queue.put((len(to_load), missing))
    finally:
        cohort.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_process_boundaries(tasks, rows_per_chunk, processes):
    """Get the boundary coordinates for the tasks such that they are split over
    processes and chunk boundaries are contained within the same process.

    Parameters
    ----------
    tasks : `list`
        The tasks, must have length > 0.
    rows_per_chunk : `int`
        The max number of data rows in each chunk.
    processes : `int`
        The max number of processes to be used to process the ``tasks``.

    Returns
    -------
    row_bounds : `list` of `tuple` of (`int`, `int`)
        The boundaries in tasks that will be processed by each process.
        ``row_bounds`` can be <= processes depending on number of ``processes``
        , ``tasks`` and ``rows_per_chunk``.
    """
    rows = len(tasks)
    rpp = math.ceil(rows / processes)
    proc_bound = rpp + rows_per_chunk - (rpp % rows_per_chunk)
    return [(i, min(rows, i+proc_bound)) for i in range(0, rows, proc_bound)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_array_shape(invars, cohort):
    """Get the shape of the zarr array for a cohort.

    Parameters
    ----------
    invars : `list` of `dict`
        The input genetic variants. This must have the keys and value types:
        ``uni_id``, ``chr_name``, ``start_pos``, ``end_pos``,
        ``effect_allele``, ``other_allele``, ``is_unknown``.
    cohorts : `list` of `dict`
        Descriptions of the cohorts that will be used for clumping. Each
        dict should have the keys, ``cohort_no`` (int), ``cohort_name`` (str),
        ``leading_zero`` (bool), ``samples`` (``list`` of ``str``, empty for no
        samples), ``sample_section`` (``str`` or ``NoneType`` - for no sample
        config section).

    Returns
    -------
    nrows : `int`
        The number of rows (variants) in the zarr array.
    ncols : `int`
        The number of columns (samples) in the zarr array.
    """
    cohort.open()
    array_shape = (len(invars), len(cohort.reader.sample_order))
    cohort.close()

    if array_shape[0] == 0:
        raise IndexError("no rows in dosage array shape")

    if array_shape[1] == 0:
        raise IndexError("no columns in dosage array shape")

    return array_shape
