"""Some general functions for adding common argparse arguments
"""
from bio_misc.common import constants as con


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_input_output(parser, infile=True, infile_optional=False, outfile=True,
                     outfile_optional=False):
    """Add generic input/output arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the input output arguments to.
    infile : `bool`, optional, default: `False`
        If True then an input file argument is added.
    infile_optional : `bool`, optional, default: `False`
        If True then the input file is added as a flag i.e. ``--infile``. If
        `False` then it is aded as a positional argument. This only applies if
        ``infile=True``.
    outfile : `bool`, optional, default: `False`
        If True then an output file argument is added.
    outfile_optional : `bool`, optional, default: `False`
        If True then the input file is added as a flag i.e. ``--outfile``. If
        `False` then it is aded as a positional argument. This only applies if
        ``outfile=True``.

    Notes
    -----
    The input file argument is always added before the output file argument.
    """
    if infile is True:
        if infile_optional is True:
            parser.add_argument(
                '-i', '--infile', type=str,
                help="An input file, if not supplied or - then input is"
                "assumed to be from STDIN. All input must have a header."
            )
        else:
            parser.add_argument(
                'infile', type=str,
                help="An input file. All input must have a header."
            )

    if outfile is True:
        if outfile_optional is True:
            parser.add_argument(
                '-o', '--outfile', type=str,
                help="An output file, if not supplied then output is assumed"
                " to be to STDOUT."
            )
        else:
            parser.add_argument(
                'outfile', type=str,
                help="An output file."
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_file_arguments(parser, delimiter=True, tmp_dir=True, comment=True,
                       encoding=True):
    """Add generic file parameter arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the input output arguments to.
    delimiter : `bool`, optional, default: `True`
        If ``True`` then a ``-d``, ``--delimiter`` flag is added with the
        default ``bio_misc.common.constants.DEFAULT_DELIMITER``.
    tmp_dir : `bool`, optional, default: `True`
        If ``True`` then a ``-T``, ``--tmp-dir`` flag is added with the default
        ``NoneType`` - i.e. defaults to system tmp directory.
    comment : `bool`, optional, default: `True`
        If ``True`` then a ``-C``, ``--comment`` flag is added with the default
        ``bio_misc.common.constants.DEFAULT_COMMENT``.
    encoding : `bool`, optional, default: `True`
        If ``True`` then a ``-E``, ``--encoding`` flag is added with the
        default ``bio_misc.common.constants.DEFAULT_ENCODING``.

    Notes
    -----
    The arguments are added as optional flags.
    """
    if delimiter is True:
        parser.add_argument(
            '-d', '--delimiter', type=str, default=con.DEFAULT_DELIMITER,
            help="The delimiter of the input and output (default: {}).".format(
                con.DEFAULT_DELIMITER
            )
        )
    if tmp_dir is True:
        parser.add_argument(
            '-T', '--tmp-dir', type=str,
            help="The tmp directory to use (default: system temp)."
        )
    if comment is True:
        parser.add_argument(
            '-C', '--comment', type=str, default=con.DEFAULT_COMMENT,
            help="The comment character in the file. (default: {})".format(
                con.DEFAULT_COMMENT
            )
        )
    if encoding is True:
        parser.add_argument(
            '-E', '--encoding', type=str, default=con.DEFAULT_ENCODING,
            help="The default encoding of the file (default: {}).".format(
                con.DEFAULT_ENCODING
            )
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_rest_arguments(parser, url=True, species=True):
    """Add arguments to the `argparse.ArgumentParser` that are specific to the
    Ensembl REST client.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the input output arguments to.
    url : `bool`, optional, default: `True`
        If `True` then a ``-r``, ``--rest`` flag is added with the default
        ``bio_misc.common.constants.DEFAULT_REST``.
    species : `bool`, optional, default: `True`
        If `True` then a ``--species`` flag is added with the default
        `bio_misc.common.constants.DEFAULT_SPECIES`.

    Notes
    -----
    The arguments are added as optional flags.
    """
    if url is True:
        parser.add_argument(
            '-r', '--rest', type=str, default=con.ENSEMBL_REST,
            help="The Ensembl REST endpoint to use. (default: {})".format(
                con.ENSEMBL_REST
            )
        )
    if species is True:
        parser.add_argument(
            '--species', type=str, default=con.DEFAULT_SPECIES,
            help="The species for the gene lookup (default: {}).".format(
                con.DEFAULT_SPECIES
            )
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_verbose_arguments(parser):
    """Add verbose arguments to the `argparse.ArgumentParser`

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the verbose arguments to.
    """
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_genomic_coords_arguments(parser, chr_name=True, start_pos=True,
                                 end_pos=True):
    """Add arguments to the `argparse.ArgumentParser` that are specific to the
    Ensembl genomic coordinates.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the input output arguments to.
    chr_name : `bool`, optional, default: `True`
        If `True` then a ``-r``, ``--rest`` flag is added with the default
        `bio_misc.common.constants.CHR_NAME`.
    start_pos : `bool`, optional, default: `True`
        If `True` then a ``-s``, ``--start-pos`` flag is added with the default
        ``bio_misc.common.constants.START_POS``.
    end_pos : `bool`, optional, default: `True`
        If `True` then an ``-e``, ``--end-pos`` flag is added with the default
        ``NoneType``.

    Notes
    -----
    The arguments are added as optional flags.
    """
    if chr_name is True:
        parser.add_argument(
            '-c', '--chr-name', type=str, default=con.CHR_NAME,
            help="The name of the chromosome column (default: {}).".format(
                con.CHR_NAME
            )
        )

    if start_pos is True:
        parser.add_argument(
            '-s', '--start-pos', type=str, default=con.START_POS,
            help="The start position column (default: {}).".format(
                con.START_POS
            )
        )

    if end_pos is True:
        parser.add_argument(
            '-e', '--end-pos', type=str,
            help="The end position column if not supplied then start position"
            " is used as end position."
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_allele_arguments(parser, ref=True, alt=True, use_effect_other=False):
    """Add arguments to the `argparse.ArgumentParser` that are specific to the
    Alleles.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the input output arguments to.
    ref : `bool`, optional, default: `True`
        If `True` then a ``-R``, ``--ref-allele`` flag is added with the
        default ``ref``.
    alt : `bool`, optional, default: `True`
        If `True` then a ``-A``, ``--alt-allele`` flag is added with the
        default ``alt``.
    use_effect_other : `bool`, optional, default: `False`
        If `True` then use effect/other-allele instead of ref/alt-allele.

    Notes
    -----
    The arguments are added as optional flags. If ``use_effect_other`` is True
    then reference allele ``--ref-allele`` will become ``--effect-allele`` and
    ``--alt-allele`` will become ``--other-allele``.
    """
    ref_allele = '--ref-allele'
    alt_allele = '--alt-allele'
    ref_default = 'ref'
    alt_default = 'alt'
    if use_effect_other is True:
        ref_allele = '--effect-allele'
        alt_allele = '--other-allele'
        ref_default = 'effect_allele'
        alt_default = 'other_allele'

    if ref is True:
        parser.add_argument(
            '-R', ref_allele, type=str, default=ref_default,
            help="The name of the {0} column (default: {0}).".format(
                ref_default
        )
    )
    if alt is True:
        parser.add_argument(
            '-A', alt_allele, type=str, default=alt_default,
            help="The name of the {0} column (default: {0}).".format(
                alt_default
        )
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_id_column_arguments(parser, ensembl_id=True, uniprot_id=True):
    """Add generic file parameter arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the input output arguments to.
    ensembl_id : `bool`, optional, default: `True`
        If `True` then a ``-g``, ``--gene-id`` flag is added with the default
        `bio_misc.common.constants.ENSEMBL_GENE_ID`.
    uniprot_id : `bool`, optional, default: `True`
        If `True` then a ``-p``, ``--prot-id`` flag is added with the default
        `bio_misc.common.constants.UNIPROT_PROT_ID`.

    Notes
    -----
    The arguments are added as optional flags.
    """
    if ensembl_id is True:
        parser.add_argument(
            '-g', '--gene-id', type=str,
            default=con.ENSEMBL_GENE_ID,
            help="The gene ID column. (default: {})".format(
                con.ENSEMBL_GENE_ID
            )
        )
    if uniprot_id is True:
        parser.add_argument(
            '-p', '--prot-id', type=str, default=con.UNIPROT_PROT_ID,
            help="The protein ID column. (default: {})".format(
                con.UNIPROT_PROT_ID
            )
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_neo4j_arguments(parser):
    """Add generic file parameter arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the Neo4j arguments to.

    Notes
    -----
    This adds ``--neo-host``, ``--neo-port``, ``--neo-user``, ``--neo-pass``.
    The arguments are added as optional flags.
    """
    parser.add_argument(
        '--neo-host', type=str, default="localhost",
        help="The hostname of the graph database (default:localhost)"
    )
    parser.add_argument(
        '--neo-port', type=int, default=7687,
        help="The port of the graph database (default:7687)"
    )
    parser.add_argument(
        '--neo-user', type=str, default='neo4j',
        help="The neo4j username (default: neo4j)"
    )
    parser.add_argument(
        '--neo-pass', type=str,
        help="The password if you are brave enough to put it in as a"
        " cmd-line arg. Remember this will be visible as a running process and"
        " will be in your history! If not given then you will be asked for it"
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_drug_data_arguments(parser, chembl=True, bnf=True):
    """Add chembl and or bnf database connection arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the ChEMBL arguments to.
    chembl : `bool`, optional, default: `True`
        If `True` then a ``--chembl`` flag is added with the default
        ``chembl.db``.
    bnf : `bool`, optional, default: `True`
        If `True` then a ``--bnf`` flag is added with the default
        ``bnf.db``.
    """
    if chembl is True:
        parser.add_argument(
            '--chembl', type=str, default=None,
            help="The name of the ChEMBL SQLite database (required)"
        )

    if bnf is True:
        parser.add_argument(
            '--bnf', type=str, default=None,
            help="The name of the BNF SQLite database"
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_multitask_args(parser):
    """Add arguments applicable for multiprocessing.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the ChEMBL arguments to.

    Notes
    -----
    Adds a ``--processes`` argument with a default of 1.
    """
    parser.add_argument('--processes', type=int, default=1,
                        help="The number of processes to use")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_missing_args(parser, help="include output that does not apply"):
    """Add arguments applicable for including data that is "missing" in the
    output. This is common among many of the scripts.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser to add the ChEMBL arguments to.
    help : `str`, optional, default: `include output that does not apply`
        The help string to use for the ``--missing`` argument.

    Notes
    -----
    Adds a ``--missing`` (``-M``) argument.
    """
    parser.add_argument(
        '-M', '--missing', action="store_true",
        help=help
    )
