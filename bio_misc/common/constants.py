import sys

# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_INFO = '[info] '
"""This is prefixed onto verbose massages (you do not have to use though). This
is also how you docstring a constant so sphinx will include in the docs
(`str`)
"""
MSG_LOC = sys.stderr
"""The location of verbose messages
"""

# Default column names
CHR_NAME = 'chr_name'
START_POS = 'start_pos'
END_POS = 'end_pos'
CHR_POS = 'chrpos'
REF_ALLELE = 'ref_allele'
ALT_ALLELE = 'alt_allele'
EFFECT_ALLELE = 'effect_allele'
OTHER_ALLELE = 'other_allele'
EFFECT_SIZE = 'effect_size'
PVALUE = 'pvalue'
ENSEMBL_GENE_ID = 'ensembl_gene_id'
UNIPROT_PROT_ID = 'uniprot_prot_id'
DEFAULT_DELIMITER = "\t"
DEFAULT_ENCODING = 'utf-8'
DEFAULT_COMMENT = '#'
DEFAULT_CHEMBL_DB = 'chembl.db'
INF_VALUE = 1000
DEFAULT_SPECIES = 'homo_sapiens'

# ##### ENSEMBL REST PARAMETERS ######
ENSEMBL_REST = 'https://rest.ensembl.org'
