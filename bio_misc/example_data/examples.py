"""Provides centralised access to example data sets that can be used in tests
and also in example code and/or jupyter notebooks.

The `bio_misc.example_data.examples` module is very simple, this is
not really designed for editing via end users but they should call the two
public methods functions, `bio_misc.example_data.examples.get_data()`,
`bio_misc.example_data.examples.help()`.

Notes
-----
Data can be "added" either through functions that generate the data on the fly
or via functions that load the data from a static file located in the
``example_data`` directory. The data files being added  should be as small as
possible (i.e. kilobyte/megabyte range). The dataset functions should be
decorated with the ``@dataset`` decorator, so the example module knows about
them. If the function is loading a dataset from a file in the package, it
should look for the path in ``_ROOT_DATASETS_DIR``.

Examples
--------

Registering a function as a dataset providing function:

>>> @dataset
>>> def dummy_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that returns a small list.
>>>
>>>     Returns
>>>     -------
>>>     data : `list`
>>>         A list of length 3 with ``['A', 'B', 'C']``
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions.
>>>     \"\"\"
>>>     return ['A', 'B', 'C']

The dataset can then be used as follows:

>>> from bio_misc.example_data import examples
>>> examples.get_data('dummy_data')
>>> ['A', 'B', 'C']

A dataset function that loads a dataset from file, these functions should load
 from the ``_ROOT_DATASETS_DIR``:

>>> @dataset
>>> def dummy_load_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that loads a string from a file.
>>>
>>>     Returns
>>>     -------
>>>     str_data : `str`
>>>         A string of data loaded from an example data file.
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions. The path to this dataset is
>>>     built from ``_ROOT_DATASETS_DIR``.
>>>     \"\"\"
>>>     load_path = os.path.join(_ROOT_DATASETS_DIR, "string_data.txt")
>>>     with open(load_path) as data_file:
>>>         return data_file.read().strip()

The dataset can then be used as follows:

>>> from bio_misc.example_data import examples
>>> examples.get_data('dummy_load_data')
>>> 'an example data string'
"""
from bio_misc.ipd import common
from pathlib import Path
import pandas as pd
import urllib.request
import os
import sys
import re
import gzip
import csv
import shelve
import atexit
import configparser
import numpy as np
import numpy.ma as ma
# import pprint as pp


# The name of the example datasets directory
_EXAMPLE_DATASETS = "example_datasets"
"""The example dataset directory name (`str`)
"""

_ROOT_DATASETS_DIR = os.path.join(os.path.dirname(__file__), _EXAMPLE_DATASETS)
"""The root path to the dataset files that are available (`str`)
"""

_DATASETS = dict()
"""This will hold the registered dataset functions (`dict`)
"""

CACHE_ROOT = os.path.join(os.environ['HOME'], ".cache", "biomisc_cache")
Path(CACHE_ROOT).mkdir(parents=True, exist_ok=True)
"""Also provide access to a cache location for downloaded datasets
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dataset(func):
    """Register a dataset generating function. This function should be used as
    a decorator.

    Parameters
    ----------
    func : `function`
        The function to register as a dataset. It is registered as under the
        function name.

    Returns
    -------
    func : `function`
        The function that has been registered.

    Raises
    ------
    KeyError
        If a function of the same name has already been registered.

    Notes
    -----
    The dataset function should accept ``*args`` and ``**kwargs`` and should be
    decorated with the ``@dataset`` decorator.

    Examples
    --------
    Create a dataset function that returns a dictionary.

    >>> @dataset
    >>> def get_dict(*args, **kwargs):
    >>>     \"\"\"A dictionary to test or use as an example.
    >>>
    >>>     Returns
    >>>     -------
    >>>     test_dict : `dict`
    >>>         A small dictionary of string keys and numeric values
    >>>     \"\"\"
    >>>     return {'A': 1, 'B': 2, 'C': 3}
    >>>

    The dataset can then be used as follows:

    >>> from bio_misc.example_data import examples
    >>> examples.get_data('get_dict')
    >>> {'A': 1, 'B': 2, 'C': 3}

    """
    try:
        _DATASETS[func.__name__]
        raise KeyError("function already registered")
    except KeyError:
        pass

    _DATASETS[func.__name__] = func
    return func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_data(name, *args, **kwargs):
    """Central point to get the datasets.

    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a registered
        dataset function.
    *args
        Arguments to the data generating functions
    **kwargs
        Keyword arguments to the data generating functions

    Returns
    -------
    dataset : `Any`
        The requested datasets
    """
    try:
        return _DATASETS[name](*args, **kwargs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def list_datasets():
    """List all the registered datasets.

    Returns
    -------
    datasets : `list` of `tuple`
        The registered datasets. Element [0] for each tuple is the dataset name
        and element [1] is a short description captured from the docstring.
    """
    datasets = []
    for d in _DATASETS.keys():
        desc = re.sub(
            r'(Parameters|Returns).*$', '', _DATASETS[d].__doc__.replace(
                '\n', ' '
            )
        ).strip()
        datasets.append((d, desc))
    return datasets


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def help(name):
    """Central point to get help for the datasets.

    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a unique key in the
        DATASETS module level dictionary.

    Returns
    -------
    help : `str`
        The docstring for the function
    """
    docs = ["Dataset: {0}\n{1}\n\n".format(name, "-" * (len(name) + 9))]
    try:
        docs.extend(
            ["{0}\n".format(re.sub(r"^\s{4}", "", i))
             for i in _DATASETS[name].__doc__.split("\n")]
        )
        return "".join(docs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def somalogic_seq_ids(*args, **kwargs):
    """A mapping dict between somalogic seq_ids and uniprot_ids.

    Returns
    -------
    mappings : `dict`
        The keys of the `dict` are somalogic seq IDs and the values are lists
        of dictionaries that contain the uniprot IDs they map to and the status
        of the seq ID.
    """
    mappings = {}
    load_path = os.path.join(_ROOT_DATASETS_DIR, "uniprot_to_seq_id.txt.gz")
    with gzip.open(load_path, "rt") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        header = next(reader)
        for row in reader:
            row = dict([(i, row[idx]) for idx, i in enumerate(header)])
            # row['remove'] = bool(int(row['remove']))

            try:
                mappings[row['seq_id']].append(row)
            except KeyError:
                mappings[row['seq_id']] = [row]
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def uniprot_to_gene(assembly, *args, **kwargs):
    """A mapping dict between uniprot IDs (both SwissProt and TrEMBL) and
    ensembl gene IDs.

    Returns
    -------
    mappings : `dict`
        The keys of the `dict` are uniprot IDs and the values are lists
        of tuples the [0] ensembl gene ID, [1] gene version, [2] the mapping
        type i.e. ``DIRECT`` or ``SEQUENCE_MAP``.
    """
    load_path = _get_load_path(assembly)

    mappings = {}
    with gzip.open(load_path, "rt") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        header = next(reader)
        for row in reader:
            row = dict([(i, row[idx]) for idx, i in enumerate(header)])

            # There may be some undefined entries so these are skipped
            if row['uniprot_accession'] == '':
                continue
            atts = (
                row['ensembl_gene_id'],
                row['version_gene'],
                row['uniprot_mapping']
            )

            try:
                seen = [i for i in mappings[row['uniprot_accession']]
                        if i[0] == atts[0]]

                if len(seen) == 0:
                    mappings[row['uniprot_accession']].append(atts)
            except KeyError:
                mappings[row['uniprot_accession']] = [atts]
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def gene_to_uniprot(assembly, *args, **kwargs):
    """A mapping dict between ensembl gene IDs and uniprot IDs (SwissProt and
    TrEMBL).

    Returns
    -------
    mappings : `dict`
        The keys of the `dict` are ensembl gene IDs and the values are lists
        of tuples that contain the ``[0]`` uniprot IDs they map to ``[1]`` the
        database (either ``Uniprot/SPTREMBL`` or ``"Uniprot/SWISSPROT") and the
        type of mapping, i.e. ``DIRECT`` or ``SEQUENCE_MAP``.

    Notes
    -----
    Note that non-coding genes are included as well but with an empty mapping
    list, so it is possible to differentiate between bad gene IDs and
    non-coding entries.
    """
    load_path = _get_load_path(assembly)

    mappings = {}
    with gzip.open(load_path, "rt") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        header = next(reader)
        for row in reader:
            row = dict([(i, row[idx]) for idx, i in enumerate(header)])

            # There may be some undefined entries so these are skipped
            if row['ensembl_gene_id'] == '':
                continue

            # Non-coding genes
            if row['uniprot_accession'] == '':
                mappings[row['ensembl_gene_id']] = []
                continue

            atts = (
                row['uniprot_accession'],
                row['uniprot_database'],
                row['uniprot_mapping']
            )

            try:
                seen = [i for i in mappings[row['ensembl_gene_id']]
                        if i[0] == atts[0]]

                if len(seen) == 0:
                    mappings[row['ensembl_gene_id']].append(atts)
            except KeyError:
                mappings[row['ensembl_gene_id']] = [atts]
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_load_path(assembly):
    """Get the full path to the offline mapping file based on the genome assembly

    Parameters
    ----------
    assembly : `str`
        Recognised values are either ``b37`` or ``b38``.

    Returns
    -------
    load_path : `str`
        The path to the mapping file.
    """
    load_path = None
    if assembly == 'b37':
        load_path = os.path.join(
            _ROOT_DATASETS_DIR, "homo_sapiens_all.v104.b37.txt.gz"
        )
    elif assembly == 'b38':
        load_path = os.path.join(
            _ROOT_DATASETS_DIR, "homo_sapiens_all.v104.b38.txt.gz"
        )
    else:
        raise ValueError(
            "unknown assembly, please use either 'b37' or 'b38': {0}".format(
                assembly
            )
        )
    return load_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def dummy_data(*args, **kwargs):
    """A dummy dataset function that returns a small list.

    Returns
    -------
    data : `list`
        A list of length 3 with ``['A', 'B', 'C']``

    Notes
    -----
    This function is called ``dummy_data`` and has been decorated with a
    ``@dataset`` decorator which makes it available with the
    `example_data.get_data(<NAME>)` function and also
    `example_data.help(<NAME>)` functions.
    """
    return ['A', 'B', 'C']


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def dummy_load_data(*args, **kwargs):
    """A dummy dataset function that loads a string from a file.

    Returns
    -------
    str_data : `str`
        A string of data loaded from an example data file.

    Notes
    -----
    This function is called ``dummy_data`` and has been decorated with a
    ``@dataset`` decorator which makes it available with the
    `example_data.get_data(<NAME>)` function and also
    `example_data.help(<NAME>)` functions. The path to this dataset is built
    from ``_ROOT_DATASETS_DIR``.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "string_data.txt")
    with open(load_path) as data_file:
        return data_file.read().strip()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop1_vcf_data(*args, **kwargs):
    """Get all the paths related to test population 1 vcf data.

    Returns
    -------
    pop1_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.
    """
    return _pop_n_data(1, 'vcf', '.vcf.gz', '.vcf.gz.tbi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop1_bgen_data(*args, **kwargs):
    """Get all the paths related to test population 1 bgen data.

    Returns
    -------
    pop2_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.
    """
    return _pop_n_data(1, 'bgen', '.bgen', '.bgen.bgi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop1_corr_matrix(*args, **kwargs):
    """Get a correlation matrix for the population1 vcf calculated by
    plink 1.9.

    Returns
    -------
    pop1_corr_matrix : `pandas.DataFrame`
        The square pairwise correlation matrix for population 1.
    """
    pop_info = pop1_vcf_data()
    return _pop_n_corr_matrix(pop_info, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop1_variants(*args, **kwargs):
    """Get the test variant data for population 1.

    Returns
    -------
    pop1_variants : `list` of `dict`
        The variant information, will have keys ``chr_name``, ``start_pos``,
        ``is_unknown``. ``uni_id`` ``effect_allele`` and ``other_allele``.
    """
    pop_info = pop1_vcf_data()
    return _pop_n_variants(pop_info, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop2_vcf_data(*args, **kwargs):
    """Get all the paths related to test population 2 vcf data.

    Returns
    -------
    pop2_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.
    """
    return _pop_n_data(2, 'vcf', '.vcf.gz', '.vcf.gz.tbi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop2_bgen_data(*args, **kwargs):
    """Get all the paths related to test population 2 bgen data.

    Returns
    -------
    pop2_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.
    """
    return _pop_n_data(2, 'bgen', '.bgen', '.bgen.bgi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop2_corr_matrix(*args, **kwargs):
    """Get a correlation matrix for the population 2 vcf calculated by
    plink 1.9.

    Returns
    -------
    pop1_corr_matrix : `pandas.DataFrame`
        The square pairwise correlation matrix for population 1.
    """
    pop_info = pop2_vcf_data()
    return _pop_n_corr_matrix(pop_info, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop2_variants(*args, **kwargs):
    """Get the test variant data for population 2.

    Returns
    -------
    pop2_variants : `list` of `dict`
        The variant information, will have keys ``chr_name``, ``start_pos``,
        ``is_unknown``. ``uni_id`` ``effect_allele`` and ``other_allele``.
    """
    pop_info = pop2_vcf_data()
    return _pop_n_variants(pop_info, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop3_vcf_data(*args, **kwargs):
    """Get all the paths related to test population 3 vcf data.

    Returns
    -------
    pop3_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.
    """
    return _pop_n_data(3, 'vcf', '.vcf.gz', '.vcf.gz.tbi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop3_bgen_data(*args, **kwargs):
    """Get all the paths related to test population 3 bgen data.

    Returns
    -------
    pop3_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.

    """
    return _pop_n_data(3, 'bgen', '.bgen', '.bgen.bgi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop3_variants(*args, **kwargs):
    """Get the variants and counts for population 3.

    Returns
    -------
    pop3_variants : `list` of `dict`
        The variant information, will have keys ``chr_name``, ``start_pos``,
        ``is_unknown``. ``uni_id`` ``effect_allele`` and ``other_allele``. This
        will also have the keys ``flipped`` - boolean expected flipped status
        and ``counts`` - float 32 numpy masked array of the expected allele
        counts.
    """
    pop_info = pop3_vcf_data()
    return _pop_n_variant_counts(pop_info, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop4_vcf_data(*args, **kwargs):
    """Get all the paths related to test population 4 vcf data.

    Returns
    -------
    pop4_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.

    """
    return _pop_n_data(4, 'vcf', '.vcf.gz', '.vcf.gz.tbi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop4_bgen_data(*args, **kwargs):
    """Get all the paths related to test population 4 bgen data.

    Returns
    -------
    pop4_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.

    """
    return _pop_n_data(4, 'bgen', '.bgen', '.bgen.bgi')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def pop4_variants(*args, **kwargs):
    """Get the variants and counts for population 4.

    Returns
    -------
    pop4_variants : `list` of `dict`
        The variant information, will have keys ``chr_name``, ``start_pos``,
        ``is_unknown``. ``uni_id`` ``effect_allele`` and ``other_allele``. This
        will also have the keys ``flipped`` - boolean expected flipped status
        and ``counts`` - float 32 numpy masked array of the expected allele
        counts.
    """
    pop_info = pop4_vcf_data()
    return _pop_n_variant_counts(pop_info, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _pop_n_corr_matrix(pop_info, *args, **kwargs):
    """Get a correlation matrix for the population1 vcf calculated by
    plink 1.9.

    Parameters
    ----------
    pop_info : `dict`
        The population information must have the keys ``variants`` (path to a
        file containing the variants) and ``exclude_vars`` - the uni_ids of
        variants that we do not want to include.

    Returns
    -------
    pop_corr_matrix : `pandas.DataFrame`
        The square pairwise correlation matrix for the population.
    """
    matrix = pd.read_csv(pop_info['corr_matrix'], sep="\t", index_col=0)
    keeps = ~matrix.index.isin(pop_info['exclude_vars'])
    return matrix.loc[keeps, keeps]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _pop_n_variant_counts(pop_info, *args, **kwargs):
    """Get a list of variants and their allele counts for testing allele count
    extraction.

    Parameters
    ----------
    pop_info : `dict`
        The population information must have the keys ``variants`` (path to a
        file containing the variants).

    Returns
    -------
    variants : `list` of `dict`
        The variant information, will have keys ``chr_name``, ``start_pos``,
        ``is_unknown``. ``uni_id`` ``effect_allele`` and ``other_allele``. This
        will also have the keys ``flipped`` - boolean expected flipped status
        and ``counts`` - float 32 numpy masked array of the expected allele
        counts.
    """
    variants = []
    with open(pop_info['variants']) as infile:
        reader = csv.reader(infile, delimiter="\t")
        header = next(reader)

        for row in reader:
            var_data = dict(
                [(header[idx], row[idx]) for idx in range(0, 6, 1)]
            )
            var_data['counts'] = np.array(
                [float(row[idx]) if row[idx] != '.' else np.nan
                 for idx in range(6, len(header), 1)],
                dtype=np.float32
            )
            var_data['counts'] = ma.array(
                var_data['counts'], mask=np.isnan(var_data['counts'])
            )
            var_data['is_unknown'] = False
            var_data['flipped'] = bool(int(var_data['flipped']))
            var_data['start_pos'] = int(var_data['start_pos'])
            var_data['uni_id'] = "{0}_{1}_{2}_{3}".format(
                var_data['chr_name'], var_data['start_pos'],
                *sorted([var_data['effect_allele'], var_data['other_allele']])
            )
            variants.append(var_data)
    return variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _pop_n_variants(pop_info, *args, **kwargs):
    """Get a path to the variants from a population that will be used in a
    correlation matrix test.

    Parameters
    ----------
    pop_info : `dict`
        The population information must have the keys ``variants`` (path to a
        file containing the variants) and ``exclude_vars`` - the uni_ids of
        variants that we do not want to include.

    Returns
    -------
    variants : `list` of `dict`
        The variant information, will have keys ``chr_name``, ``start_pos``,
        ``is_unknown``. ``uni_id`` ``effect_allele`` and ``other_allele``.
    """
    variants = []
    with open(pop_info['variants']) as infile:
        reader = csv.DictReader(infile, delimiter="\t")
        for row in reader:
            v = common.Variant(
                row['chr_name'], row['start_pos'],
                row['effect_allele'], row['other_allele']
            )
            if v.uni_id not in pop_info['exclude_vars']:
                variants.append(v)
    return variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _pop_n_data(pop_no, file_format, extension, index):
    """Get the paths associated with a test population.

    Parameters
    ----------
    pop_n : `int`
        The number of the population.
    file_format : `str`
        The test population file format, i.e. ``bgen`` or ``vcf``.
    extension : `str`
        The test population file extension, i.e. ``.vcf.gz`` or ``.bgen``.
    index : `int`
        The test population index file extension, i.e. ``.vcf.gz.tbi`` or
        ``.bgen.bgi``.

    Returns
    -------
    paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.
    """
    ipd_test_path = "ipd_data"
    ipd_root = os.path.join(_ROOT_DATASETS_DIR, ipd_test_path)

    paths = dict(
        name=f"pop{pop_no}",
        species='human',
        assembly="b37",
        file_format=file_format,
        exclude_vars=["1_668630_<CN2>_G", "1_773090_<CN0>,<CN2>"],
        corr_matrix=os.path.join(
            ipd_root, f"test_pop{pop_no}_chr1_matrix.txt"
        ),
        variants=os.path.join(ipd_root, f"test_pop{pop_no}_chr1_variants.txt"),
        test_file=os.path.join(
            ipd_root, f"test_pop{pop_no}_chr{{CHR}}{extension}"
        ),
        tabix_index=os.path.join(ipd_root, f"test_pop{pop_no}_chr1{index}")
    )
    return paths


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def ipd_config_test(outdir, *args, **kwargs):
    """Get the path to a config file created on the fly with the paths to two
    population test vcf files
    """
    parser = configparser.ConfigParser(
        allow_no_value=True, delimiters=('=',)
    )
    # This ensures that the keys are case-sensitive
    parser.optionxform = str

    # The location we will write the full file with chain files and local vcf
    # and bgen files
    target_path = os.path.join(outdir, "test_config.ini")

    for i in [pop1_vcf_data(), pop2_vcf_data(), pop3_vcf_data(),
              pop4_vcf_data()]:
        section = "cohort.{0}.{1}.{2}.{3}".format(
            i['species'],
            i['assembly'],
            i['file_format'],
            i['name']
        )
        parser.add_section(section)
        parser.set(section, i['test_file'], None)

    for i in [pop1_bgen_data(), pop2_bgen_data(), pop3_bgen_data(),
              pop4_bgen_data()]:
        section = "cohort.{0}.{1}.{2}.{3}".format(
            i['species'],
            i['assembly'],
            i['file_format'],
            i['name']
        )
        parser.add_section(section)
        parser.set(section, i['test_file'], None)

    # Now write the new config file
    with open(target_path, 'w') as outini:
        parser.write(outini)
    return target_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def zarr_test_info(*args, **kwargs):
    """Get all the paths related to test population 3 bgen data.

    Returns
    -------
    pop3_paths : `dict`
        The keys are path types ``corr_matrix``, ``variants``, ``test_file``
        ``tabix_index`` and the values are full path names. Additional info is
        in the keys ``name``, ``species``, ``assembly``, ``file_format``.

    """
    zarr_test_path = "zarr_test"
    zarr_root = os.path.join(_ROOT_DATASETS_DIR, zarr_test_path)

    zarr_test_paths = []
    with open(os.path.join(zarr_root, "pop_data.txt")) as pop_data:
        reader = csv.DictReader(pop_data, delimiter="\t")
        for row in reader:
            for i in ["corr_matrix", "variants", "test_file", "tabix_index"]:
                row[i] = os.path.join(zarr_root, row[i])
            zarr_test_paths.append(row)
    return zarr_test_paths


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def zarr_config_test(outdir, *args, **kwargs):
    """Get the path to a config file created on the fly with the paths of the
    zarr test data in there.
    """
    parser = configparser.ConfigParser(
        allow_no_value=True, delimiters=('=',)
    )
    # This ensures that the keys are case-sensitive
    parser.optionxform = str

    # The location we will write the full file with chain files and local vcf
    # and bgen files
    target_path = os.path.join(outdir, "zarr_test_config.ini")

    for i in zarr_test_info():
        section = "cohort.{0}.{1}.{2}.{3}".format(
            i['species'],
            i['assembly'],
            i['file_format'],
            i['name']
        )
        parser.add_section(section)
        parser.set(section, i['test_file'], None)

    # Now write the new config file
    with open(target_path, 'w') as outini:
        parser.write(outini)
    return target_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def ld_clump_test_data(outdir, *args, **kwargs):
    """Get the paths and datasets required to perform LD clumping tests.

    Parameters
    ----------
    outdir : `str`
        A directory to output the config file to. The config file will have the
        paths to all the datasets needed to perform the test.
    """
    test_data_path = "clump_data"
    test_data_root = os.path.join(_ROOT_DATASETS_DIR, test_data_path)

    test_files = dict(
        corr_matrix="clump_corr.txt.gz",
        nobs_matrix="clump_nobs.txt.gz",
        cohort1_vars="cohort_c1.txt.gz",
        cohort1_infile="cohort_c1.vcf.gz",
        cohort1_index="cohort_c1.vcf.gz.tbi",
        cohort2_vars="cohort_c2.txt.gz",
        cohort2_infile="cohort_c2.vcf.gz",
        cohort2_index="cohort_c2.vcf.gz.tbi",
        cohort_combined_vars="cohort_combined.txt.gz",
        cohort_combined_infile="cohort_combined.vcf.gz",
        cohort_combined_index="cohort_combined.vcf.gz.tbi",
        cohort_sample_set="cohort_eur_samples.txt",
    )

    # Set up the test files with the correct paths
    for k, v in test_files.items():
        test_files[k] = os.path.join(test_data_root, v)
        open(test_files[k]).close()

    parser = _create_config_obj()

    # The location we will write the full file with chain files and local vcf
    # and bgen files
    target_path = os.path.join(outdir, "clump_test_config.ini")

    species = 'human'
    assembly = 'b37'
    file_format = 'vcf'

    test_cohorts = []
    for i in ['cohort_combined', 'cohort1', 'cohort2']:
        section = "cohort.{0}.{1}.{2}.{3}".format(
            species,
            assembly,
            file_format,
            i
        )
        test_cohorts.append(
            dict(
                name=i,
                species=species,
                assembly=assembly,
                file_format=file_format,
                variants=test_files[f"{i}_vars"],
                samples="TEST_SAMPLES"
            )
        )
        parser.add_section(section)
        parser.set(section, test_files[f"{i}_infile"], None)
    # Add the sample set
    sample_section = "samples.TEST_SAMPLES"
    parser.add_section(sample_section)
    with open(test_files['cohort_sample_set'], 'rt') as insamp:
        for row in insamp:
            parser.set(sample_section, row.strip(), None)

    # Now write the new config file
    with open(target_path, 'w') as outini:
        parser.write(outini)

    cm = pd.read_csv(test_files['corr_matrix'], delimiter="\t", index_col=0)
    nobs = pd.read_csv(test_files['nobs_matrix'], delimiter="\t", index_col=0)
    return target_path, cm, nobs, test_cohorts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _create_config_obj():
    """Set up a config object with the correct attributes for a genomic config
    file.

    Returns
    -------
    config : `configparser.ConfigParser`
        Empty config obejct.
    """
    parser = configparser.ConfigParser(
        allow_no_value=True, delimiters=('=',)
    )
    # This ensures that the keys are case-sensitive
    parser.optionxform = str
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def gene_synonyms(outdir):
    """Get the static gene synonyms data. If this is not available then it will
    be downloaded and processed.

    Parameters
    ----------
    outdir : `str`
        The directory where the synonyms data is/should be located.

    Returns
    -------
    db : `shelve`
        A shelve file with ensembl identifiers as keys and gene synonyms as
        values. The values are a list of tuples, where each tuple has the
        synonym source at ``[0]`` and the synonym at ``[1]``.
    """
    outdir = outdir or CACHE_ROOT

    if not os.path.isdir(outdir):
        raise FileNotFoundError(f"cache root path does not exist: {outdir}")

    gene_synonyms = os.path.join(outdir, "gene_synonyms")
    full_download = os.path.join(outdir, "gene_synonyms.txt.gz")

    try:
        # Try opening the then file to see if there
        open(f"{gene_synonyms}.dat").close()
    except FileNotFoundError:
        # Download and build
        setup_gene_synonyms(gene_synonyms, full_download)

    db = shelve.open(gene_synonyms)
    atexit.register(db.close)
    return db


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def setup_gene_synonyms(outpath, download):
    """Setup the gene synonyms cache by downloading the file from NIH and
    building into a shelve cache.

    Parameters
    ----------
    outpath : `str`
        The root output path for the sheve dictionary.
    download : `str`
        The path to download the synonyms file to. Note that if this already
        exists then no file is downloaded.

    Notes
    -----
    The download and sheve dictionary will be first processed into a temp file
    (same file names with .temp extension) before being copied to their
    locations upon success. The downloaded file will also be deleted when the
    shove dictionary is built.
    """
    # Fields in the the synonym file, whay they will be stored under and how
    # they will be processed
    fields = [
        ('GeneID', 'gene_id', _parse_str),
        ('Symbol', 'gene_symbol', _parse_str),
        ('LocusTag', 'locus_tag', _parse_str),
        ('Synonyms', 'synonym', _parse_list),
        ('dbXrefs', 'xrefs', _parse_regexp),
        ('description', 'description', _parse_str),
        ('Other_designations', 'other', _parse_other)
    ]

    # The temp locations
    temp_download_file = f"{download}.temp"
    temp_db_file = f"{outpath}.temp"

    try:
        # Does the download file already exists
        open(download).close()
        # Yes
    except FileNotFoundError:
        print(f"[info] downloading {download}", file=sys.stderr)
        # No, then download
        download_gene_synonyms(temp_download_file)
        # Successful download so rename
        os.rename(temp_download_file, download)
    finally:
        try:
            # Will be called in all cases so just make sure any temp
            # downloads do not hang about
            os.unlink(temp_download_file)
        except FileNotFoundError:
            pass

    try:
        # Open up the temp shelve
        with shelve.open(temp_db_file) as db:
            print(f"[info] building {outpath}.dat", file=sys.stderr)
            # Open up the download and parse with csv
            with gzip.open(download, 'rt') as infile:
                reader = csv.DictReader(infile, delimiter="\t")
                for row in reader:
                    # Will hold a list of synonyms that will be stored
                    # next to an ensembl gene ID key
                    store_data = []
                    for k, sk, parser in fields:
                        # skip empty fields
                        if row[k] != '-':
                            store_data.extend(parser(sk, row[k]))

                    # Find any ensembl IDs
                    store_keys = [
                        v for k, v in store_data
                        if k in ['ensembl', 'ensemblrapid']
                    ]

                    # And use them as keys in the shelve for storing the
                    # synonyms
                    for k in store_keys:
                        db[k] = store_data
        # Finally move the built shlve store to the final location
        os.rename(f"{temp_db_file}.dat", f"{outpath}.dat")
        os.rename(f"{temp_db_file}.dir", f"{outpath}.dir")
        os.rename(f"{temp_db_file}.bak", f"{outpath}.bak")
        # and remvoe the download
        os.unlink(download)
    finally:
        try:
            # Ensure no temp shelve dicts are hanging about
            os.unlink(temp_db_file)
        except FileNotFoundError:
            pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_str(field_name, field):
    """Parse a string field.

    Parameters
    ----------
    field_name : `str`
        The name of the field synonym.
    field : `str`
        The value of the field synonym.

    Returns
    -------
    synonym_data : `list` of `tuple`
        Each tuple will have the synonym database at ``[0]`` and the actual
        synonym value at ``[1]``.
    """
    return [(field_name, field)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_list(field_name, field):
    """Parse a string field that might contain an internal pipe delimiter.

    Parameters
    ----------
    field_name : `str`
        The name of the field synonym.
    field : `str`
        The value of the field synonym. This might have an internal pipe
        delimiter ``|``

    Returns
    -------
    synonym_data : `list` of `tuple`
        Each tuple will have the synonym database at ``[0]`` and the actual
        synonym value at ``[1]``.
    """
    return [(field_name, i) for i in field.split("|")]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_regexp(field_name, field):
    """Parse a string field that might contain an internal pipe delimiter,
    with the entries having the database leading their string i.e.
    ``db_name: synonym_value``.

    Parameters
    ----------
    field_name : `str`
        The name of the field synonym.
    field : `str`
        The value of the field synonym. This might have an internal pipe
        delimiter ``|`` and leading databases for the synonym values.

    Returns
    -------
    synonym_data : `list` of `tuple`
        Each tuple will have the synonym database at ``[0]`` and the actual
        synonym value at ``[1]``.
    """
    fields = []
    for i in field.split("|"):
        m = re.match(r'^((?P<DB>\w+):??)', i)
        db = m.group('DB')
        gid = i[m.span()[1]+1:]
        fields.append((db.lower(), gid))
    return fields


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_other(field_name, field):
    """Parse a string field that might contain an internal pipe delimiter.

    Parameters
    ----------
    field_name : `str`
        The name of the field synonym.
    field : `str`
        The value of the field synonym. This might have an internal pipe
        delimiter ``|``

    Returns
    -------
    synonym_data : `list` of `tuple`
        Each tuple will have the synonym database at ``[0]`` and the actual
        synonym value at ``[1]``.

    Notes
    -----
    This also removes the leading value ``LOW QUALITY PROTEIN:\s+`` from each
    synonym.
    """
    fields = []
    for i in field.split("|"):
        gid = re.sub(r'LOW QUALITY PROTEIN:\s+', '', i)
        fields.append((field_name, gid))
    return fields


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_gene_synonyms(outpath):
    """Download the gene information file from the NIH (via FTP)

    Parameters
    ----------
    outpath : `str`
        The path you want to download to.
    """
    try:
        urllib.request.urlretrieve(
            'ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz',
            outpath
        )
    except Exception:
        try:
            # Ensure no half downloaded data exists
            os.unlink(outpath)
        except FileNotFoundError:
            pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def site_overlaps(*args, **kwargs):
    """Get the paths to the site overlaps test data and expected output files.

    Returns
    -------
    test_file1 : `str`
        The path to the first test file.
    test_file1 : `str`
        The path to the first second file.
    exp_results : `tuple` of `str`
        The expected results files in this order: ``intersect_result.txt``,
        ``return_all_result.txt``, ``return1_result.txt``,
        ``return2_result.txt``,  ``na_intersect_result.txt``,
        ``na_return_all_result.txt``, ``na_return1_result.txt``,
        ``na_return2_result.txt``
    """
    test_data_path = "overlaps"
    test_data_root = os.path.join(_ROOT_DATASETS_DIR, test_data_path)
    return (
        os.path.join(test_data_root, 'file1.txt.gz'),
        os.path.join(test_data_root, 'file2.txt'),
        (
            os.path.join(test_data_root, 'intersect_result.txt'),
            os.path.join(test_data_root, 'return_all_result.txt'),
            os.path.join(test_data_root, 'return1_result.txt'),
            os.path.join(test_data_root, 'return2_result.txt'),
            os.path.join(test_data_root, 'na_intersect_result.txt'),
            os.path.join(test_data_root, 'na_return_all_result.txt'),
            os.path.join(test_data_root, 'na_return1_result.txt'),
            os.path.join(test_data_root, 'na_return2_result.txt'),
        ),
    )
