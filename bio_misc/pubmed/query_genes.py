from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments,
    log
)
from bio_misc.example_data import examples
from paper_scraper import pubmed
from ensembl_rest_client import client
from requests.exceptions import HTTPError
from tqdm import tqdm
from nltk.corpus import words
import nltk
import argparse
import csv
import os
import re
# import gzip
import sys
# import pprint as pp


_SCRIPT_NAME = 'bm-pubmed-gene-query'
"""The name of the script that is implemented in this module (`str`)
"""
_QUERY_CACHE = dict()
"""A dictionary to act as an in memory cache for the Ensembl queries in a
session (`dict`)
"""
SYNONYM_DBS = [
    ('EntrezGene', 'display_id'),
    ('HGNC', 'display_id'),
    ('Reactome gene', 'display_id'),
    ('CCDS', 'primary_id'),
    ('WikiGene', 'display_id'),
    ('Uniprot_gn', 'display_id'),
    ('Uniprot/SWISSPROT', 'primary_id'),
    ('RefSeq_ncRNA', 'primary_id'),
    ('RNAcentral', 'display_id')
]
"""The names and ID fields for the synonym databases in Ensembl, in each
tuple the database name is at ``[0]`` and the ID key is at ``[1]``
(`list` of `tuple`)
"""

try:
    CACHE_ROOT = os.environ['BIOMISC_CACHE']
except KeyError:
    CACHE_ROOT = None

STOP_GENES = set()
"""Will hold gene synonyms that are overlapping with English label words
 (`set`)
"""
GENE_SYNS = None
"""Will hold gene synonyms
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise_module_vars():
    """Initialise the module variables if needed.
    """
    global STOP_GENES
    global GENE_SYNS
    if len(STOP_GENES) == 0:
        try:
            STOP_GENES = set(words.words())
        except Exception:
            # Needs downloading can't workout how to except the LookupError
            nltk.download('words')
            STOP_GENES = set(words.words())

    # Get the gene synonym data
    if GENE_SYNS is None:
        GENE_SYNS = examples.get_data("gene_synonyms", CACHE_ROOT)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_pubmed_query(gene_ids, query_terms, min_gene_len=4):
    """Build a pubmed query string.

    Parameters
    ----------
    gene_ids : `list` of `str`
        The gene synonyms to add to the query.
    query_terms : `str`
        Additional pubmed query, to query alongside the gene names.
    min_gene_len : `int`, optional, default: `4`
        The cut point for minimum gene synonym length to be included in a
        pubmed query. Gene names bust be >= to this.

    Returns
    -------
    query : `str`
        The final pubmed query string.
    """
    initialise_module_vars()
    ok_gene_ids = []
    small_gene_ids = []
    stop_gene_ids = []
    for i in gene_ids:
        if len(i) < min_gene_len:
            small_gene_ids.append(i)
        elif i.lower() in STOP_GENES:
            stop_gene_ids.append(i)
        else:
            ok_gene_ids.append(i)
    # Remove short gene names
    # gene_ids = [i for i in gene_ids if len(i) >= min_gene_len]
    # gene_ids = [i for i in gene_ids if i.lower() not in STOP_GENES]

    if len(ok_gene_ids) == 0:
        raise ValueError("no genes to query")

    # build the gene query part
    genes = " OR ".join([f'"{i}"[All Fields]' for i in sorted(ok_gene_ids)])

    if query_terms is not None and len(query_terms) > 0:
        query = "({0}) AND ({1})".format(genes, query_terms)
    else:
        query = "({0})".format(genes)
    return query, ok_gene_ids, stop_gene_ids, small_gene_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_ensembl_synonyms(rc, species, ensembl_gene_id):
    """Query Gene synonyms from Ensembl.

    Parameters
    ----------
    rc : `ensembl_rest_client.client.Rest`
        The REST client to issue the query.
    species : `str`
        The species that the gene belongs to.
    ensembl_gene_id : `str`
        The ensembl gene ID to get the synonyms for.
    """
    mappings = [ensembl_gene_id]

    try:
        try:
            lkup = rc.get_lookup_id(
                ensembl_gene_id, species=species
            )
            mappings.append(lkup['display_name'])
        except KeyError:
            pass

        ids = rc.get_xrefs_id(
            ensembl_gene_id, species=species, all_levels=True
        )
    except HTTPError:
        return mappings

    for i in ids:
        for db, field in SYNONYM_DBS:
            try:
                if i['dbname'] == db:
                    mappings.append(i[field])
                    mappings.extend(i['synonyms'])
                continue
            except KeyError:
                pass

    mappings = list(set(mappings))
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_cached_synonyms(ensembl_gene_id):
    """Query Gene synonyms from the downloaded NIH gene info file.

    Parameters
    ----------
    ensembl_gene_id : `str`
        The ensembl gene ID to get the synonyms for.
    """
    initialise_module_vars()
    try:
        return [i[1] for i in GENE_SYNS[ensembl_gene_id]]
    except KeyError:
        return []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_input_file(infile, rc, delimiter=con.DEFAULT_DELIMITER,
                     encoding=con.DEFAULT_ENCODING,
                     comment=con.DEFAULT_COMMENT,
                     ensembl_gene_id="ensembl_gene_id",
                     species="homo_sapiens",
                     pubmed_query="pubmed_query",
                     verbose=False, min_gene_len=4):
    """Parse the input search terms into memory.

    Parameters
    ----------
    infile : `str`
        The input file path.
    rc : `ensembl_rest_client.client.Rest`
        The rest client object to perform the queries.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    ensembl_gene_id : `str`, optional, default: `ensembl_gene_id`
        The name of the ensembl gene ID column that contains the IDs to get
        synonyms for.
    species : `str`, optional, default: `homo_sapiens`
        The default species to use for the rest queries.
    pubmed_query : `str`, optional, default: `pubmed_query`
        The name of the column that contains pubmed query text, don't
        forget to quote query terms appropriately in this column.
    citations_out : `str`
        The output file for the citation counts.
    verbose : `bool`, optional, default: `False`
        Report progress.
    min_gene_len : `int`, optional, default: `4`
        The cut point for minimum gene synonym length to be included in a
        pubmed query. Gene names bust be >= to this.
    """
    search_terms = []
    with utils.BaseParser(infile, encoding=encoding, delimiter=delimiter,
                          comment=comment) as parser:
        ensembl_id_idx = parser.get_col_idx(ensembl_gene_id)
        pubmed_query_idx = parser.get_col_idx(pubmed_query)
        for row in tqdm(parser, disable=not verbose,
                        desc="[info] getting gene synonyms",
                        unit=" genes"):
            ens_synonyms = query_ensembl_synonyms(
                rc, species, row[ensembl_id_idx]
            )
            cached_synonyms = query_cached_synonyms(row[ensembl_id_idx])
            synonyms = list(set(ens_synonyms + cached_synonyms))

            query, ok_gene_ids, stop_gene_ids, small_gene_ids = \
                build_pubmed_query(
                    synonyms, row[pubmed_query_idx], min_gene_len=min_gene_len
                )
            search_terms.append(
                [query, row, ok_gene_ids, stop_gene_ids, small_gene_ids]
            )
    return parser.header, search_terms


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_info_writer(outfile, **kwargs):
    """Get the file object and writer to the citation info file.

    Parameters
    ----------
    outfile : `str`
        The output path to the citation information file.
    **kwargs
        Keyword arguments to `csv`

    Returns
    -------
    file_obj : ``
        The output file object.
    writer : `csv.DictWriter`
        The writer object.

    Notes
    -----
    This also writes the header of the file.
    """
    file_obj = open(outfile, 'wt')

    try:
        write_obj = csv.DictWriter(
            file_obj, pubmed.MedlineRecord.MAIN_FIELD_HEADER + ['user_term'],
            **kwargs
        )
        write_obj.writeheader()
    except Exception:
        file_obj.close()
        raise

    return file_obj, write_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_info_row(search_summary, citation, sub_delimiter="|"):
    """Get an output row data for the citation information.

    Parameters
    ----------
    user_search : `str`
        The search string that was given to pubmed (not the pubmed translation
        of it).
    search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
        The search summary object to extract from.
    citation : `paper_scraper.pubmed.MedlineRecord`
        The medline citation.
    sub_delimiter : `str`, optional, default: `|`
        The value to use for nested sub delimiters in the output row.

    Returns
    -------
    outrow : `dict`
        A list that can be used with `csv.DictWriter`
    """
    mt = []
    for term, modifiers, is_pref in citation.get_mesh_terms():
        modifiers = "/".join(modifiers)
        mt.append("[{0}|{1}|{2}]".format(term, modifiers, int(is_pref)))

    outrow = citation.get_main_fields()
    outrow['mesh_terms'] = "".join(mt)
    outrow['user_term'] = search_summary.user_search
    return outrow


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_count_writer(outfile, input_header, **kwargs):
    """Get the file object and writer to the citation count file.

    Parameters
    ----------
    outfile : `str`
        The output path to the citation counts file.
    input_header : `list` of `str`
        The input header row.
    **kwargs
        Keyword arguments to `csv`

    Returns
    -------
    file_obj : ``
        The output file object.
    writer : `csv.DictWriter`
        The writer object.

    Notes
    -----
    This also writes the header of the file.
    """
    file_obj = open(outfile, 'wt')

    try:
        write_obj = csv.writer(
            file_obj, **kwargs
        )
        write_obj.writerow(
            input_header +
            ['ncitations', 'all_gene_synonyms', 'genes_used', 'stop_genes',
             'small_genes', 'quoted_phrase_not_found', 'phrase_not_found',
             'phrase_ignored', 'user_term', 'pubmed_term']
        )
    except Exception:
        file_obj.close()
        raise

    return file_obj, write_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_count_row(search_summary, input_row, ok_genes, stop_genes,
                            small_genes, sub_delimiter="|"):
    """Get an output row data for the citation count.

    Parameters
    ----------
    search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
        The search summary object to extract from.
    input_row : `list` of `str`
        The input row that the search counts will be concatenated with.
    ok_genes : `list` of `str`
        Gene synonyms that will be used in the query.
    stop_genes : `list` of `str`
        Gene synonyms that have been removed because they overlap English
        dictionary words.
    small_genes : `list` of `str`
        Gene synonyms that have been removed because their synonym name
        length is too short.
    sub_delimiter : `str`, optional, default: `|`
        The value to use for nested sub delimiters in the output row.

    Returns
    -------
    outrow : `list`
        A list that can be used with `csv.writer`
    """
    all_genes = ok_genes.copy()

    for i in search_summary.get_quoted_phrase_not_found():
        i = re.sub(r'^"', "", i)
        i = re.sub(r'"\[All Fields\]$', "", i)
        try:
            ok_genes.pop(ok_genes.index(i))
        except ValueError:
            pass

    return input_row + [
        search_summary.get_count(),
        pubmed.none_join(all_genes, sub_delimiter),
        pubmed.none_join(ok_genes, sub_delimiter),
        pubmed.none_join(stop_genes, sub_delimiter),
        pubmed.none_join(small_genes, sub_delimiter),
        pubmed.none_join(
            search_summary.get_quoted_phrase_not_found(), sub_delimiter
        ),
        pubmed.none_join(search_summary.get_phrase_not_found(), sub_delimiter),
        pubmed.none_join(search_summary.get_phrase_ignored(), sub_delimiter),
        search_summary.user_search,
        search_summary.get_term(),
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def count_pubmed(search_input, input_header, email, citation_counts_file,
                 *args, verbose=False, relday=0, delimiter="\t",
                 sub_delimiter="|", **kwargs):
    """Perform a text search against pubmed and store the citation counts only.

    This is a utility function for use with the CMD-line, although can be used
    in an API call. However, for full control, see
    `paper_scraper.pubmed.PubmedQuery`.

    Parameters
    ----------
    search_input : `list` of `tuple`
        The searches information. Each tuple should contain ``(search_query,
        input_row, ok_genes, stop_genes, small_genes)``. The ``search_query``
        should be formatted in the same way you would on the pubmed website.
        ``ok_genes`` are gene synonyms that will be used in the query.
        ``stop_genes`` are genes that overlap English dictionary words and
        ``small_genes`` are gene synonyms where their length is too short.
    input_header : `list` or `str`
        The header of the input file.
    email : `str`
        The email address of the user performing the search. This is so pubmed
        can contact users performing excessive searches rather than simply
        blacklisting their IP.
    citation_counts_file : `str`
        The file name to write the citation counts to.
    *args
        Ignored
    verbose : `bool`, optional, default: `False`
        Provide a progress update.
    relday : `int`, optional, default: `0`
        The relative day count window in which to perform the search.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the output file.
    sub_delimiter : `str`, optional, default: `|`
        The delimiter to use for nested delimited sub-fields in the output
        file.
    **kwargs
        Ignored

    See also
    --------
    paper_scraper.pubmed.PubmedQuery
    """
    # Make sure we have distinct delimiters and sub delimiters
    pubmed.check_delimiters(delimiter, sub_delimiter)

    q = pubmed.PubmedQuery(email)

    cc, ccw = _get_citation_count_writer(
        citation_counts_file, input_header, delimiter=delimiter
    )

    try:
        for i in tqdm(search_input, disable=not verbose, unit=" search terms",
                      desc="[info] searching pubmed"):
            search_term, input_row, ok_genes, stop_genes, small_genes = i
            search_summary = q.count_text(search_term, relday=relday)
            ccw.writerow(_get_citation_count_row(
                search_summary, input_row, ok_genes, stop_genes, small_genes,
                sub_delimiter=sub_delimiter
            ))
    finally:
        cc.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def search_pubmed(search_input, input_header, email, citation_counts_file,
                  citation_info_file, verbose=False, elink=True, relday=0,
                  delimiter="\t", sub_delimiter="|"):
    """Perform a text search against pubmed and store the citation info and
    counts.

    This is a utility function for use with the CMD-line, although can be used
    in an API call. However, for full control, see
    `paper_scraper.pubmed.PubmedQuery`.

    Parameters
    ----------
    search_input : `list` of `tuple`
        The searches information. Each tuple should contain ``(search_query,
        input_row, ok_genes, stop_genes, small_genes)``. The ``search_query``
        should be formatted in the same way you would on the pubmed website.
        ``ok_genes`` are gene synonyms that will be used in the query.
        ``stop_genes`` are genes that overlap English dictionary words and
        ``small_genes`` are gene synonyms where their length is too short.
    input_header : `list` or `str`
        The header of the input file.
    email : `str`
        The email address of the user performing the search. This is so pubmed
        can contact users performing excessive searches rather than simply
        blacklisting their IP.
    citation_counts_file : `str`
        The file name to write the citation counts to.
    citation_info_file : `str`
        The file name to write the citation information to.
    verbose : `bool`, optional, default: `False`
        Provide a progress update.
    elink : `bool`, optional, default: `False`
        Gather eLink journal provider/download URL info. This will slow the
        query down.
    relday : `int`, optional, default: `0`
        The relative day count window in which to perform the search.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the output file.
    sub_delimiter : `str`, optional, default: `|`
        The delimiter to use for nested delimited sub-fields in the output
        file.

    See also
    --------
    paper_scraper.pubmed.PubmedQuery
    """
    # Make sure we have distinct delimiters and sub delimiters
    pubmed.check_delimiters(delimiter, sub_delimiter)

    q = pubmed.PubmedQuery(email)

    ci, ciw = _get_citation_info_writer(
        citation_info_file, delimiter=delimiter
    )
    cc, ccw = _get_citation_count_writer(
        citation_counts_file, input_header, delimiter=delimiter
    )

    try:
        for i in tqdm(search_input, disable=not verbose, unit=" search terms",
                      desc="[info] searching pubmed"):
            search_term, input_row, ok_genes, stop_genes, small_genes = i
            search_summary, citations, errors = q.search_text(
                search_term, elink=elink, relday=relday, verbose=verbose
            )
            for c in citations:
                ciw.writerow(_get_citation_info_row(
                    search_summary, c, sub_delimiter=sub_delimiter
                ))

            ccw.writerow(_get_citation_count_row(
                search_summary, input_row, ok_genes, stop_genes, small_genes,
                sub_delimiter=sub_delimiter
            ))
    finally:
        ci.close()
        cc.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    rc = client.Rest(url=args.rest)
    verbose = False
    if args.verbose > 1:
        verbose = True

    try:
        header, search_terms = parse_input_file(
            args.search_terms, rc, delimiter=args.delimiter, verbose=verbose,
            encoding=args.encoding, comment=args.comment,
            ensembl_gene_id=args.gene_id, species=args.species,
            pubmed_query=args.query, min_gene_len=args.min_gene_len
        )
        if args.counts_only is False:
            search_pubmed(
                search_terms, header, args.email, args.citation_counts,
                args.citation_info, verbose=verbose, elink=args.elink,
                relday=args.relday, delimiter=args.delimiter,
                sub_delimiter="|"
            )
        else:
            count_pubmed(
                search_terms, header, args.email, args.citation_counts,
                verbose=verbose, relday=args.relday, delimiter=args.delimiter,
                sub_delimiter="|"
            )
        log.log_end(logger)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Query gene names against pubmed along with other pubmed "
        "search terms. Gene name synonyms are extracted from Ensembl"
    )
    parser.add_argument(
        'search_terms',
        type=str,
        help="A search file containing the Ensembl Gene ID and associated"
        " pubmed query 1-per line, with each gene ID matched to a query."
    )
    parser.add_argument('-o', '--counts-only',  action="store_true",
                        help="Do not download citations, just get the citation"
                        " counts")
    arguments.add_file_arguments(parser)
    arguments.add_rest_arguments(parser, species=True)
    arguments.add_id_column_arguments(parser, uniprot_id=False,
                                      ensembl_id=True)
    parser.add_argument('--query', type=str, default="pubmed_query",
                        help="The name of the pubmed query column")
    parser.add_argument('-e', '--email',
                        type=str,
                        help="A value (treated as a string) to multiply")
    parser.add_argument('-I', '--citation-info',
                        type=str, default="citation_info.txt",
                        help="A file to write all the citation info to")
    parser.add_argument('--citation-counts',
                        type=str, default="citation_counts.txt",
                        help="A file to write all the citation counts to")
    parser.add_argument('--relday',
                        type=int, default=0,
                        help="The relative days back to perform the search")
    parser.add_argument('--min-gene-len',
                        type=int, default=4,
                        help="The length of the gene name must be >= "
                        "than this to be included in the pubmed query")
    parser.add_argument('--elink',  action="store_true",
                        help="Download the eLink journal provider and URLs,"
                        " this slows the queries down but you have a direct"
                        " hyperlink to the publication")
    parser.add_argument('-v', '--verbose',  action="count",
                        help="give more output, use -vv to turn on progress "
                        "monitoring")
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
