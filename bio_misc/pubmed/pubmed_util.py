"""
Created on 8 May 2012
Updated on 1 May 2019
@author: chris.l.finan@googlemail.com
"""

# For finding the caller from the stack trace
# (maybe unreliable in some circumstances)
import inspect

# Regular expressions
import re

# Imports the Entries modules to query pubmed
from Bio import Entrez
from Bio import Medline
import urllib
import socket
# from HtmlHacks import UnicodeDecode
import textwrap
# import pprint
import time
# import traceback
# import sys

# To issue warnings without erroring out
import warnings
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PubmedBase(object):
    """
    Base class for querying pubmed
    """
    def __init__(self, email, verbose=False, batch_size=500, limit=4000):
        """
        Initialise

        Parameters
        ----------

        email :`str`
            The user's E-mail address, this is required by pubmed when querying
            programmatically
        verbose :`bool` (optional)
            print what is happening, Defaults to False
        batch_size :`int` (optional)
            The batch size for an individual call to pubmed?? default=500
        limit :`int` (optional)
            Result limit, results set larger than this will result in a prompt
            to ask the user if they are ok with that result set size.
            defaults to 4000
        """

        if email is None:
            raise ValueError("No E-mail provided!")

        # Store the E-mail, also make sure it is assigned to Entrez so the NLM
        # know about it
        self.email = email
        Entrez.email = email
        self.verbose = verbose

        # The batch size
        self.batch_size = batch_size

        # The limit at which we start to ask if the user is
        # ok with the amount of results
        self.limit = limit

        # The PMIDs are held here (when collected). I have done this
        # as I have had loads of problems getting the LinkOut
        # URLs from the History, some times it works / other times
        # it does not work. So I have created an ID list attributes
        # where check PMIDs are copied into when retrived by search term
        # or parsed by the user. Then if the history fails these are used
        # as a direct (non-history) search
        self.pmids = []

        # Store all the PMIDs that gave recognised error so these can be
        # checked and/or output
        self.failed_pmids = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self):
        """
        Base class result fetcher
        """
        self.data = {}

        # Check we have not exceeded any limits
        count = self._check_limit()
        self.msg("Found %i results" % count)

        # Now start looping through the results in batches of
        # self.batch_size
        # I am going for the MEDLINE data first as this initialises
        # a dict of results that the URLs are appended to
        for start in range(0, count, self.batch_size):
            self._fetch_medline(start, count)

        # Get the eLink URLs and journal providers these are tagged to the
        # MEDline records. Note that these can not be searched for in batches
        # if you try to do that then they are just returned at once
        # EDIT: 13/11/2013 - This only seems to work if there are less than
        # 1000 results, so it is only worth to do this if the results are
        # in that range. To get all the urls etc. you should get all the PMIDs
        # and search in batches of 1500 grabbing the URLS etc.
        if count > 0:
            # and count <= 1500: # only do this if there is something to download!
            self._fetch_url()
        else:
            self.msg("count out of range so will not get urls")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _fetch_medline(self, start, count):
        """
        Fetch the medline data and store it

        Parameters
        ----------

        start : :obj:`int`
            The start position of the download (in the whole batch). This is
            used for verbose messages
        count : :obj:`int`
            The expected number of entries in the download. This is used for
            verbose messages

        Raises
        ------

        RuntimeError
            If some unsuspected error occurs
        """

        # The end position is either the batch size or the count
        end = min(count, start + self.batch_size)

        # If we are verbose issue a message
        self.msg("Going to download record %i to %i" % (start + 1, end))

        # Fetch a batch of data
        fetch_handle = Entrez.efetch(db="pubmed",
                                     rettype="medline",
                                     retmode="text",
                                     retstart=start,
                                     retmax=self.batch_size,
                                     webenv=self.searched.get_web_env(),
                                     query_key=self.searched.get_query_key())

        # Get the data and parse it back out as a list
        data = list(Medline.parse(fetch_handle))

        # close the handle
        fetch_handle.close()

        # Record how many records we have fetched
        self.msg("The list is '{0}' records long".format(len(data)))

        # We can check for potential errors here by checking the download size
        # and if it is the same as the batch size. I have noticed that when
        # things (unexplained) go wrong the list is about 2 records long and
        # no where near the batch size, so we can check that
        potential_error = False
        if len(data) != ((end - start) + 1):
            potential_error = True

        # Store the data internally
        for i in data:
            try:
                # Attempt to set the record via the PMID, this will fail with
                # a KeyError if the PMID does not exist (maybe it was not a
                # True PMID?)
                self.data[i['PMID']] = MedlineRecord(i)

                 # store it in case it is needed
                self.pmids.append(i['PMID'])
            except KeyError:
                # The pubmedID does not exist
                try:
                    # Now we attempt to check the error message to see if it
                    # because the PMID was incorrect, if it was then we just
                    # move on if not then we will error out this will generate
                    # a KeyError if id: does not exist and an IndexError if [0]
                    # is not found. Either way that is not error we are looking
                    # for
                    error_message = i['id:'][0]

                    # Now look for the actual error message. If we can't find
                    # that then we fail
                    # { 'id:': [ '785614 Error occurred: The following PMID is not available: 22785614']}
                    if re.search(r'Error occurred: The following PMID is not available',
                                 error_message) is None:
                        # TODO: extract the PMID from the error message and store in
                        # TODO: self.failed_pmids
                        raise KeyError(
                            "unregognised error {0}".format(str(error_message)))
                except (KeyError, IndexError):
                    # If we get here then we Truly have failed
                    # traceback.print_exc(file=sys.stderr)
                    warnings.warn("This key error seems to occur when there "
                                  "is an error in the pubmed_id. But sometimes"
                                  " it means that something horrible has "
                                  "happened to the download. Hopefully this "
                                  "will be picked up below")
                    pp.pprint(i)

                    # If we previously suspected an error has occured then exit
                    if potential_error is True:
                        raise RuntimeError("It looks very much like "
                                           "something horrible has happened")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _fetch_url(self, requery=False):
        """
        Get the URLs and the journal providers

        Parameters
        ----------

        requery :`bool` (optional)
            Indicates that this is a call after we have re-queried to get the
            URLs
        """

        if requery is True:
            self.msg("Trying to fetch after a re-query")

        # the time to sleep for between executions
        secs = 2
        self.msg("Going to download URLs for records")

        # remove spaces from the provider
        p = re.compile(r"\s+")

        # A list of pubmed IDs that don't have any
        # medline records
        no_medline = []

        # Try 2 times to get the data sleeping for 2 seconds
        # each time. This might help the error that I get when fetching
        # a large number of hits with URLs
        data = []
        for i in range(0, 2):
            # get a batch of data
            try:
                data = Entrez.read(
                            Entrez.elink(
                                        dbfrom="pubmed",
                                        cmd="prlinks",
                                        webenv=self.searched.get_web_env(),
                                        query_key=self.searched.get_query_key()
                                        )
                                   )
            except (RuntimeError, socket.error) as e:
                self.msg("There was a problem retrieving the data!")
                self.msg("Failed with: {0}".format(str(e)))

            # Just make sure we have returned some data
            self.msg("The length of data is {0} (it should be 1)".format(
                len(data)))

            # exit loop early if we have data
            if len(data) > 0:
                break
            time.sleep(secs)

        # only do this if we have some data
        if len(data) > 0:
            # Loop through the results
            res_count = 0
            for i in data[0]['IdUrlList']['IdUrlSet']:
                if i['ObjUrl']:
                    url = i['ObjUrl'][0]['Url']
                    idNo = i['Id']
                    provider = i['ObjUrl'][0]['Provider']['Name']
                    provider = provider.lower()
                    provider = p.sub("", provider)

                    try:
                        self.data[idNo]
                        res_count += 1
                        self.data[idNo].add_data("GN", "PROVIDER: " + provider)
                        self.data[idNo].add_data("GN", "URL: " + url.strip())
                    except KeyError:
                        no_medline.append(idNo)

                    # if self.data.has_key(idNo):
                    #     res_count += 1
                    #     self.data[idNo].add_data("GN","PROVIDER: "+provider)
                    #     self.data[idNo].add_data("GN","URL: "+url.strip())
                    # else:
                    #     no_medline.append(idNo)

            # Record how many records we have fetched
            self.msg("The list is %s records long" % (res_count))

            # Record the PMIDs that were problematic, change the display
            # depending on how many there are
            if len(no_medline) > 5:
                self.msg("The following IDs had no medline information "
                         "(but had eLink info)")
                self.msg("{0}...[Total {1} PMIDs]".format(
                    ",".join(no_medline[0:5]),
                    len(no_medline)))
            elif len(no_medline) <= 5 and len(no_medline) > 0:
                self.msg("The following IDs had no medline information "
                         "(but had eLink info)")
                self.msg("{0}...".format(",".join(no_medline)))

            # Just in case we want to check that it ran OK
            return True
        else:
            self.msg("There was a problem getting the URLs for this search")

            # if this isn't a call from a re-query then try again
            if requery is False:
                self.msg("Attempting to re-query for URL download!")
                self.search()

                 # Try again and indicate that it is a re-query
                return self._fetch_url(requery=True)
            else:
                # Just in case we want to check that it DID NOT run OK
                return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_limit(self):
        """
        Check that we have not exceeded the limit
        """

        # Check if there are search results
        if not self.searched:
            # Basically skip the download if there are no search results
            return 0
        elif self.limit <= 0:
            # We don't want to check
            return self.count_results()
        elif self.count_results() > self.limit:
            # we have exceeded the limit, ask the user what to do
            while True:
                print("limit = {0}, Results = {1}".format(self.limit,
                                                          self.count_results()))
                i = input("Press <ENTER> to continue or enter a number to trim"
                          " the results:")

                # If enter has been pressed then we want to continue
                if i == '':
                    return self.count_results()

                # Otherwise check for an integer
                try:
                    i = int(i)
                except ValueError:#
                    print("Unknown value {0}...try again".format(i))

                # Make sure the number the user has entered is sensible
                if i >= 0 and i <= self.count_results():
                    return i
                else:
                    print("Value: '{0}' out of range".format(i))
        else:
            # otherwise everything is OK
            return self.count_results()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def count_results(self):
        """
        Get a count of the results
        """
        # TODO: do I need to check for None
        return int(self.searched.get_count())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_results(self):
        """
        Return a list of the results, should be called after fetch
        """
        return self.data.values()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_search(self):
        """
        Return the search information, this could be useful for error checking
        the search to make sure pubmed has searched for what you expect
        """
        return self.searched

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def msg(self, m):
        """
        Print the message if verbosity is True
        """
        if self.verbose is True:
            print('+ [{0}] {1}'.format(inspect.stack()[1][3], m))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PubmedIdQuery(PubmedBase):
    """
    This is passed a list of pubmed IDs and will search pubmed for the IDs and
    return a list of results
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def check_pubmed_id(idlist, error=False):
        """
        A static method for checking that the pubmed ids look ok as a default
        it will return a list of OK IDs and a List of not Ok iDs. At present to
        pass the test the ID must be an integer digit between 7-8 digits long

        Parameters
        ----------

        idlist : :obj:`list` of `str` or `int`
            A list of pubmed IDs to test, these are cast to str and tested with
            a regexp
        error : :obj:`bool`, optional
            Shall we raise a ValueError if any IDs fail the test

        Returns
        -------

        tuple
            A tuple containing a list of passed IDs as the first element and
            failed ids as the second

        Raises
        ------

        ValueError
            If any of the IDs are not pubmed IDs and error = True
        """

        # At the moment I have set a min/max length for the PMIDs
        pubmed_min_length = 7
        pubmed_max_length = 8

        # Lists to hold failed and passed IDs respectively
        noid = []
        idok = []

        # Loop through the ID list
        for i in idlist:
            # Cast to a string
            i = str(i)

            # Test with a REGEXP and if OK then add to the passed list if not
            # the add to the failed list
            if re.search(r'^\d{%i,%i}$' % (pubmed_min_length,
                                           pubmed_max_length), str(i)):
                idok.append(i)
            else:
                noid.append(i)

        # Finally if we are reporing errors and have some failed IDs then raise
        # the ValueError
        if error is True and len(noid) > 0:
            # List the pubmed IDs that failed
            raise ValueError("The list contained '{0}' invalid pubmed id(s): "
                             "{1}".format((len(noid), ",".join(noid))))

        # If we get here, we are either not generating errors, or we are and
        # have no errors so we return a tuple of [0] good pmids, [1] bad pmids
        # (which will be an empty list if we have none)
        return idok, noid

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, email, searchfor, verbose=False, batch_size=500):
        """
        Initialise

        Parameters
        ----------

        email :`str`
            The user's E-mail address, this is required by pubmed when querying
            programmatically
        searchfor :`list` of `int`
            A list of the pubmed IDs to search for
        verbose :`bool` (optional)
            print what is happening, Defaults to False
        batch_size :`int` (optional)
            The batch size for an individual call to pubmed?? default=500
        """
        # Call the super class to store common variables. Make the limit = 0
        # as calls from here we know how many pubmed IDs are present
        super(PubmedIdQuery, self).__init__(email, verbose=verbose,
                                            batch_size=batch_size, limit=0)

        # Make sure the pubmed IDs have been passed
        if not searchfor:
            raise ValueError("No Pubmed IDs provided!")

        # And check that they are OK
        idok, noid = PubmedIdQuery.check_pubmed_id(searchfor, error=True)

        # this ensures that the IDs are stored as strings
        self.searchfor = idok

        # This is what will actually be posted during the search
        self.to_post = self.searchfor

        # Posted Value have the IDs been posted to pubmed
        self.searched = False

        # ELink limit (beyond which the History seems to disappear!!)
        self.elink_limit = 999

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search(self):
        """
        Conduct the search, this does not actually retrieve any results
        """
        self.msg("Initiating search...")

        # I have had failures here with:
        # RuntimeError: Search Backend failed: Error 103 (Software caused connection abort) @connection=0x763ca40
        # So I will put this in a try except block
        search_results = None
        try:
            # The pubmed IDs have to be strings joined on ,
            search_results = Entrez.read(
                Entrez.epost("pubmed",
                             id=",".join([str(i) for i in self.to_post])))
        except RuntimeError as e:
            self.msg("The search failed...with the following error:"
                     " '{0}'".format(str(e)))
            return False

        # if there are results
        if search_results:
            try:
                # Ensure that the search results have a count
                search_results['Count']
            except KeyError:
                search_results['Count'] = len(self.to_post)

            # If we are verbose then message out the search statistcs
            self.searched = PubmedSearchSummary(search_results)
            self.msg("Search completed...found '{0}' results".format(
                self.searched.get_count()))
            self.msg("WebEnv = '{0}'".format(self.searched.get_web_env()))
            self.msg("SearchTerm = '{0}'".format(self.searched.get_term()))
            self.msg("QuotedPhraseNotFound = {0}".format(
                ",".join(self.searched.get_quoted_phrase_not_found())))
            self.msg("PhraseNotFound = '{0}'".format(
                ",".join(self.searched.get_phrase_not_found())))
            self.msg("PhraseIgnored = '{0}'".format(
                ",".join(self.searched.get_phrase_ignored())))
        else:
            self.msg("No search results available")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _fetch_url(self):
        """
        I will overide the base class Get the URLs and the journal providers
        """
        # if we are less than 1000 results then use the super class version as
        # this should work
        if self.searched.get_count() <= self.elink_limit:
            self.msg("Have '{0}' results under the limit of '{1}'".format(
                len(self.searchfor), self.elink_limit))
            super(PubmedIdQuery, self)._fetch_url()
        else:
            self.msg("Have '{0}' results OVER the limit of '{1}'".format(
                len(self.searchfor), self.elink_limit))
            self.msg("Will have to break down the posts to get the URLs for "
                     "results!")

            # We have to break it down in to self.elink_limit sized chunks
            # TODO: This does not make sense, why not control at the level of
            # TODO: the search method? Using this appoach will involve more
            # TODO: searches
            for start in range(0, len(self.searchfor), self.elink_limit):
                end = min(len(self.searchfor), start + self.elink_limit)
                self.to_post = self.searchfor[start:end]
                self.msg("Searching for records '{0}' to '{1}'".format(
                    start + 1, end))
                self.search()
                self.msg("Getting URLs for records '{0}' to '{1}'".format(
                    start + 1, end))
                super(PubmedIdQuery, self)._fetch_url()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PubmedQuery(PubmedBase):
    """
    Given a pubmed query string, this will search pubmed
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, email, searchfor, verbose=False, batch_size=500,
                 limit=4000, relday=0):
        """
        Initialise

        Parameters
        ----------

        email :`str`
            The user's E-mail address, this is required by pubmed when querying
            programmatically
        searchfor :`str`
            The search term that will be used
        verbose :`bool` (optional)
            print what is happening, Defaults to False
        batch_size :`int` (optional)
            The batch size for an individual call to pubmed?? default=500
        limit :`int` (optional)
            Result limit, results set larger than this will result in a prompt
            to ask the user if they are ok with that result set size.
            defaults to 4000
        relday :`int`
            Relday is the timeframe that the search will look in so a relday
            of 365 is get the hits from the last year a relday of 0 just gets
            all hits irrespective of time frame
        """
        # Call the super class to store the common variables
        super(PubmedQuery, self).__init__(email,
                                          verbose=verbose,
                                          batch_size=batch_size,
                                          limit=limit)

        # Make sure the search term has been passed
        if not searchfor or searchfor == "":
            raise ValueError("No search term provided")

        # This is what we will search for, make sure it is a string
        self.searchfor = str(searchfor)
        self.msg("Using search term '{0}'".format(self.searchfor))

        # The relday if 0 then make it none
        self.relday = relday
        if relday <= 0:
            self.relday = None

        self.msg("Using relday '{0}'".format(relday))

        # Has a search been performed? note that the search is performed and
        # the results are posted to the history on the NIH server
        self.searched = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search(self):
        """
        Conduct the search, this does not actually retrieve any results
        """
        self.msg("Initiating search...")

        # I have had failures here with:
        # RuntimeError: Search Backend failed: Error 103 (Software caused connection abort) @connection=0x763ca40
        # So I will put this in a try except block
        search_results = None
        try:
            search_results = Entrez.read(Entrez.esearch(db="pubmed",
                                                        term=self.searchfor,
                                                        reldate=self.relday,
                                                        datetype="pdat",
                                                        usehistory="y"))
        except (RuntimeError, urllib.URLError) as e:
            self.msg("The search failed...with the following error:"
                     " '{0}'".format(str(e)))
            return False

        # if there are results
        if search_results:
            # If we are verbose then message out the search statistcs
            self.searched = PubmedSearchSummary(search_results)
            self.msg("Search completed...found '{0}' results".format(
                self.searched.get_count()))
            self.msg("WebEnv = '{0}'".format(self.searched.get_web_env()))
            self.msg("SearchTerm = '{0}'".format(self.searched.get_term()))
            self.msg("QuotedPhraseNotFound = {0}".format(
                ",".join(self.searched.get_quoted_phrase_not_found())))
            self.msg("PhraseNotFound = '{0}'".format(
                ",".join(self.searched.get_phrase_not_found())))
            self.msg("PhraseIgnored = '{0}'".format(
                ",".join(self.searched.get_phrase_ignored())))
        else:
            self.msg("No search results available")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PubmedSearchSummary(object):
    """
    A handy container for a pubmed search, this just provides structured 
    interaction with the search result (not the actual data the search gets
    back)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, search):
        """
        Initialise

        Parameters
        ----------

        search :`dict`
            A dictionary of the results from the Entrez.read command
        """

        if not search:
            raise ValueError("No search results provided")

        # Store them
        self.search = search

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_count(self):
        """
        Return the number of results
        """
        return self.search['Count']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_term(self):
        """
        return the actual search term used to search pubmed
        """
        return self._get_value_for('QueryTranslation')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_quoted_phrase_not_found(self):
        """
        These are the phases that are 'in quotes' that are not found by the
        pubmed search
        """
        try:
            return self.search['WarningList']['QuotedPhraseNotFound']
        except KeyError:
            return ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phrase_not_found(self):
        """
        These are all the phrases that are that are not found by the pubmed
        search
        """
        try:
            return self.search['ErrorList']['PhraseNotFound']
        except KeyError:
            return ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phrase_ignored(self):
        """
        These are the phrases ignored by pubmed
        """
        try:
            return self.search['WarningList']['PhraseIgnored']
        except KeyError:
            return ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_web_env(self):
        """
        This allows the retrival of a search
        """
        return self._get_value_for('WebEnv')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_query_key(self):
        """
        The query key is also used for search retrival
        """
        return self._get_value_for('QueryKey')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_value_for(self, key):
        """
        a helper method to get the value from the search result dictionary or
        return an empty string if it does not exist

        Parameters
        ----------

        key :`str`
            The key to look up on the search results

        Returns
        -------

        value :`any`
            The value associated with the key or "" if the key is not present
        """

        try:
            return self.search[key]
        except KeyError:
            return ""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MedlineRecord(object):
    """
    Represents the data in a pubmed record
    """
    TAGS = [('PMID', 'PubMed Unique Identifier'),
            ('OWN', 'Owner'),
            ('STAT', 'Status'),
            ('DA', 'Date Created'),
            ('DCOM', 'Date Completed'),
            ('LR', 'Date Last Revised'),
            ('IS', 'ISSN'),
            ('VI', 'Volume'),
            ('IP', 'Issue'),
            ('DP', 'Date of Publication'),
            ('TI', 'Title'),
            ('PG', 'Pagination'),
            ('LID', 'Location Identifier'),
            ('AB', 'Abstract'),
            ('CI', 'Copyright Information'),
            ('AD', 'Affiliation', list),
            ('FAU', 'Full Author', list),
            ('AU', 'Author', list),
            ('CN', 'Corporate Author', list),
            ('LA', 'Language', list),
            ('SI', 'Secondary Source ID', list),
            ('GR', 'Grant Number', list),
            ('PT', 'Publication Type', list),
            ('DEP', 'Date of Electronic Publication', list),
            ('PL', 'Place of Publication', list),
            ('TA', 'Journal Title Abbreviation', list),
            ('JT', 'Journal Title', list),
            ('JID', 'NLM Unique ID', list),
            ('CON', 'Comment on', list),
            ('RN', 'Registry Number/EC Number', list),
            ('SB', 'Subset', list),
            ('CIN', 'Comment in', list),
            ('MH', 'MeSH Terms', list),
            ('EIN', 'Erratum in', list),
            ('PMC', 'PubMed Central Identifier', list),
            ('EDAT', 'Entrez Date', list),
            ('MID', 'Manuscript Identifier', list),
            ('OID', 'Other ID', list),
            ('GN', 'General Note', list),
            ('MHDA', 'MeSH Date', list),
            ('IR', 'Investigator Name', list),
            ('CRDT', 'Do not Know', list),
            ('FIR', 'Full Investigator Name', list),
            ('PHST', 'Publication History Status', list),
            ('PMCR', 'Do not know', list),
            ('AID', 'Article Identifier', list),
            ('IRAD', 'Investigator Affiliation', list),
            ('PST', 'Publication Status', list),
            ('GS', 'Gene Symbol', list),
            ('RF', 'Number of References', str),
            ('OAB', 'Other Abstract', list),
            ('OCI', 'Other Copyright Information', list),
            ('OT', 'Other Term', list),
            ('OTO', 'Other Term Owner', list),
            ('PS', 'Personal Name as Subject', list),
            ('FPS', 'Full Personal Name as Subject', list),
            ('PUBM', 'Publishing Model', list),
            ('NM', 'Substance Name', list),
            ('ORI', 'Original report in', list),
            ('SFM', 'Space Flight Mission', list),
            ('TT', ' Transliterated Title', list),
            ('EFR', 'Erratum for', list),
            ('CRI', 'Corrected and Republished in', list),
            ('CRF', 'Corrected and Republished from', list),
            ('PRIN', 'Partial retraction in', list),
            ('PROF', 'Partial retraction of', list),
            ('RPI', 'Republished in', list),
            ('RPF', 'Republished from', list),
            ('RIN', 'Retraction in', list),
            ('ROF', 'Retraction of', list),
            ('UIN', 'Update in', list),
            ('UOF', 'Update of', list),
            ('SPIN', 'Summary for patients in', list),
            ('SO', 'Source', str)]

    TAG_SET = set([i[0] for i in TAGS])
    TAG_DESC = dict([(i[0], i[1]) for i in TAGS])
    TAG_ORDER = dict([(i[0], c) for c, i in enumerate(TAGS)])
    WRAP_WIDTH = 80
    MAIN_FIELDS = [('pubmed_id', 'PMID', 'get_data_for'),
                   ('pubmed_central_id', 'PMC', 'get_data_for'),
                   ('pub_year', None, 'get_pub_year'),
                   ('pub_month', None, 'get_pub_month'),
                   ('title', 'TI', 'get_data_for'),
                   ('abstract', 'AB', 'get_data_for'),
                   ('first_author', None, 'get_first_author'),
                   ('last_author', None, 'get_last_author'),
                   ('journal_title', 'JT', 'get_data_for'),
                   ('journal_provider', None, 'get_journal_provider'),
                   ('download_url', None, 'get_download_url'),
                   ('doi', None, 'get_doi'),
                   ('summary_string', 'SO', 'get_data_for'),
                   ('mesh_terms', None, 'get_mesh_terms')]

    # Build a header list
    MAIN_FIELD_HEADER = [i[0] for i in MAIN_FIELDS]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, tagdict):
        '''
        Initialise

        Parameters
        ----------

        tagdict :`dict`
            A tag dict can be supplied as a quick initiation of the class
        '''
        # Initialise the data
        self.tag_values = {}
        if tagdict:
            self.tag_values = tagdict

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __str__(self):
        """
        Call tor str(obj)
        """
        medstr = []
        # Loop through the medline tags in order
        for i in MedlineRecord.TAGS:
             # Get the ID of the tag
            t = i[0]

            try:
                formatted = []
                # If it is not a list entry
                if isinstance(self.tag_values[t], list) is False:
                    # formatted is an array of lines (or ossible a single line
                    formatted = self._wrap_and_format(t, self.tag_values[t])
                    medstr.extend(formatted)
                else:
                    for j in self.tag_values[t]:
                        formatted = self._wrap_and_format(t, j)
                        medstr.extend(formatted)
            except KeyError:
                pass

        # This will return a leading blank space the record
        # and then a newline at the end
        return "\n".join(medstr)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return self.__str__()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _wrap_and_format(self, tag, string):
        """
        Controls the word wrapping

        Parameters
        ----------

        tag :`str`
            The tag represented by the string
        string :`str`
            The string to wrap (if needed)

        Returns
        -------

        formatted :`list` of `str`
            A list containing medline formatted strings for the tag
        """
         # create a copy of the string
        s = string
        # s = UnicodeDecode.UnicodeNormalise.normalise(s,ask=True)

        formatted = textwrap.wrap(s, width=self.__class__.WRAP_WIDTH)

        # Formatted is a list of arapped strings, now we want to add an indent
        # and also the tag on the first line
        for i, l in enumerate(formatted):
            if i > 0:
                formatted[i] = "%-6s%s" % (" ", l)
            else:
                formatted[i] = "%-4s- %s" % (tag, l)
        return formatted

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_main_fields(self):
        """
        A helper method to process the main fields of a MEDLINE record into a
        list that can be written to a file
        """
        # Initialise the dict
        # record = dict([(i, None) for i in self.__class__.MAIN_FIELD_HEADER])
        record = {}
        for i in self.__class__.MAIN_FIELDS:
            # get the method call
            method = getattr(self, i[2])
            record[i[0]] = method(i[1])
        return record

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_main_field_list(self):
        """
        A helper method to process the main fields of a MEDLINE record into a
        list that can be written to a file
        """
        # Initialise the dict
        # record = dict([(i, None) for i in self.__class__.MAIN_FIELD_HEADER])
        record = []
        for i in self.__class__.MAIN_FIELDS:
            # get the method call
            record.append(getattr(self, i[2])(i[1]))
        return record

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def is_tag_allowed(self, tag):
        """
        Determines if a tag is allowed in a medline record

        Parameters
        ----------

        tag :`str`
            The MEDLINE tag

        Returns
        -------

        data :`bool`
            True if the tag is allowed False if not
        """
        return tag in self.__class__.TAG_SET

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_description(self, tag):
        """
        Returns the meaning of a tag

        Parameters
        ----------

        tag :`str`
            The MEDLINE tag

        Returns
        -------

        description :`str`
            The full description of the tag
        """
        if tag in self.__class__.TAG_SET:
            return tag in self.__class__.TAG_DESC[tag]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_order(self, tag):
        """
        Returns the order of a tag in the medline record

        Parameters
        ----------

        tag :`str`
            The MEDLINE tag

        Returns
        -------

        order :`int`
            The index of the tag in a medline record
        """
        if tag in self.__class__.TAG_SET:
            return tag in self.__class__.TAG_ORDER[tag]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_data(self, tag, data):
        """
        Adds data to a tag. If the tag is not a list then the data gets
        over-written, if it is a list then the data gets appended

        Parameters
        ----------

        tag :`str`
            The MEDLINE tag
        data :`any`
            The data to add to the tag
        """
        if tag in self.__class__.TAG_SET:
            try:
                self.tag_values[tag]
            except KeyError:
                # The tag is not represented, so we initialise as a list
                self.tag_values[tag] = []

            # Now we adjust the data
            if isinstance(self.tag_values[tag], list) is False:
                self.tag_values[tag] = data
            else:
                self.tag_values[tag].append(data)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_data_for(self, tag):
        """
        Return the data for a specific TAG

        Parameters
        ----------

        tag :`str`
            The MEDLINE tag

        Returns
        -------

        data :`any`
            The data associated with the tag, could be a list
        """
        try:
            return self.tag_values[tag].strip()
        except KeyError:
            if tag in self.__class__.TAG_SET:
                return None
            raise KeyError("unknown tag '{0}'".format(tag))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_pub_year(self, *args):
        """
        return the publication year
        """

        try:
            dp = self.tag_values['DP'].strip()
            dates = re.split(r'\s+', dp)
            return int(dates[0])
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_pub_month(self, *args):
        """
        return the publication year
        """

        try:
            dp = self.tag_values['DP'].strip()
            dates = re.split(r'\s+', dp)
            return dates[1]
        except (KeyError, IndexError):
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_first_author(self, *args):
        """
        Return the first author
        """
        try:
            au = self.tag_values['AU'][0]
            return au.strip()
        except (KeyError, IndexError):
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_last_author(self, *args):
        """
        Return the first author
        """
        try:
            au = self.tag_values['AU'][-1]
            return au.strip()
        except (KeyError, IndexError):
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_journal_provider(self, *args):
        """
        Return the journal provider
        """
        test = r'PROVIDER:\s+'

        try:
            gn = self.tag_values['GN']

            for i in gn:
                if re.match(test, i):
                    return re.sub(test, '', i).strip()
            return None
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_download_url(self, *args):
        """
        Return the download url
        """
        test = r'URL:\s+'

        try:
            gn = self.tag_values['GN']

            for i in gn:
                if re.match(test, i):
                    return re.sub(test, '', i).strip()
            return None
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_doi(self, *args):
        """
        Return the doi
        """
        test = r'\s*\[doi\]\s*$'

        try:
            lid = self.tag_values['LID'].strip()

            if re.search(test, lid):
                return re.sub(test, '', lid)

            aid = self.tag_values['AID']
            for i in aid:
                if re.search(test, i):
                    return re.sub(test, '', i).strip()
            return None
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_mesh_terms(self, *args):
        """
        Return the mesh terms
        """
        all_mesh = []
        try:
            mh = self.tag_values['MH']

            for i in mh:
                major = False

                # There may be modifiers
                sub_mh = i.split(r'/')

                # Now serach for a major modifier 
                for c, i in enumerate(sub_mh):
                    if i.startswith("*"):
                        major = True
                        sub_mh[c] = i[1:]

                key_term = sub_mh.pop(0)

                # To induce and index error when no modifiers
                try:
                    sub_mh[0]
                    modifiers = "/".join(sub_mh)
                except IndexError:
                    modifiers = None
                all_mesh.append((key_term, modifiers, major))
            return all_mesh
        except KeyError:
            return None
