"""Perform a negative log10 transformation of a p-value column in such a way
that out of range values are appropriately handled. For example, if the pvalue
is too small the python decimal package is used to -log10 transform and prevent
any zeroing of out or range values.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from tqdm import tqdm
import math
import decimal
import argparse
import sys
import os
import csv
# import pprint as pp


_SCRIPT_NAME = 'bm-log-pvalue'
"""The name of the script that is output when verbose is True (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def log_pvalue(infile, outfile=None, pvalue=con.PVALUE, tmp_dir=None,
               inf_value=con.INF_VALUE, verbose=False,
               delimiter=con.DEFAULT_DELIMITER, comment=con.DEFAULT_COMMENT,
               encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of flipped alleles.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    pvalue : `str`, optional, default: `bio_misc.common.constants.PVALUE`
        The name of the pvalue column.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    inf_value : `float`, optional, default: `bio_misc.common.constants.INF_VALUE`
        The fall back -log10(P-value) value to use for out of precision
        p-values.
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_log_pvalue(infile, pvalue=pvalue, comment=comment,
                                 inf_value=inf_value, yield_header=True,
                                 delimiter=delimiter, encoding=encoding),
                desc="[progress] log transforms: ",
                unit=" transforms",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_log_pvalue(infile, pvalue=con.PVALUE, inf_value=con.INF_VALUE,
                     comment=con.DEFAULT_COMMENT, yield_header=True,
                     delimiter=con.DEFAULT_DELIMITER,
                     encoding=con.DEFAULT_ENCODING):
    """Yield rows containing of -log10 transformed p-values.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    pvalue : `str`, optional, default: `bio_misc.common.constants.PVALUE`
        The name of the pvalue column.
    inf_value : `float`, optional, default: `bio_misc.common.constants.INF_VALUE`
        The fall back -log10(P-value) value to use for out of precision
        p-values.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If ``flip_to`` is not one of ``sort``, ``pos`` or ``neg`` or
        ``effect_type`` is not one of ``beta``, ``log_or`` or ``or``.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    with utils.BaseParser(infile, **kwargs) as parser:
        pvalue_idx = parser.get_col_idx(pvalue)
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                ['log10_pvalue']
            )

        for idx, row in enumerate(parser, 1):
            row.append(log_transform(row[pvalue_idx], inf_value=inf_value))
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def log_transform(pvalue, inf_value=con.INF_VALUE):
    """Log transform the pvalue.

    Parameters
    ----------
    pvalue : `str`
        The p-value
    inf_value : `float`, optional, default: `bio_misc.common.constants.INF_VALUE`
        The value to use if the p-value is already infinite or is 0 indicating
        that it has had prior loss of precision. This could also be set to
        ``NoneType`` if needed.

    Returns
    -------
    log10_pvalue : `float`
        The -log10 transformed p-value.

    Raises
    ------
    TypeError
        if the transformed pvalue is negative which probably indicates it
        was transformed already
    decimal.InvalidOperation
        If all transformation attempts yield an Nan value

    Notes
    -----
    This will first attempt a regular log transform but if that fails, this it
    will try an arbitrary precision transform using decimal. If that fails or
    any of the resulting pvalues are negative then it is an error. If the
    transformed pvalue is inf (positive), then it is set to inf_value (should
    be set to very big and recognisable). If the pvalue is a string that can't
    be handled or ``NoneType``, then a ``NoneType`` is returned.
    """
    try:
        # Using decimal is expensive, so try without first
        pvalue = -math.log10(float(pvalue))
    except ValueError:
        try:
            pvalue = float(
                -decimal.Decimal(pvalue).log10()
            )
        except decimal.InvalidOperation:
            # Some other non-transformable string
            return None
    except TypeError:
        # Some other non-castable string
        return None

    if pvalue < 0:
        raise TypeError("are you sure your pvalue is not already logged?")
    elif math.isnan(pvalue):
        raise decimal.InvalidOperation("NaN pvalue")

    return pvalue if not math.isinf(pvalue) else inf_value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        log_pvalue(args.infile, outfile=args.outfile, pvalue=args.pvalue,
                   inf_value=args.inf, tmp_dir=args.tmp_dir,
                   verbose=args.verbose, delimiter=args.delimiter,
                   comment=args.comment, encoding=args.encoding)
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        '-P', '--pvalue', type=str, default=con.PVALUE,
        help="The name of the pvalue column (default: {}).".format(
            con.PVALUE
        )
    )
    parser.add_argument(
        '-I', '--inf', type=float, default=con.INF_VALUE,
        help="The value to substitute in for infinite values"
        " (default: {}).".format(con.INF_VALUE)
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
