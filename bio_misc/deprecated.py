# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    # Moved to pyaddons
    raise DeprecationWarning(
        "This has been deprecated in bio-misc. Use pyaddons instead"
        " (https://cfinan.gitlab.io/pyaddons/scripts/python_scripts.html).",
        DeprecationWarning
    )
