"""Use ensembl gene identifiers to query GeneCards and extract synonyms and
some text annotation. This caches repeat queries so no need to unique the gene
IDs.

This will require Google Chrome and Chrome webdriver to be installed  and in
your ``PATH``. The chrome driver needs to match your google chrome version.
See `here <https://chromedriver.chromium.org/downloads>`_

When running the script, an instance Google Chrome browser will open up and
the script will have control of it. For this reason, this script should be run
on a graphical desktop. Whilst is it possible to control Google Chrome headless
, it is not as stable so it is not currently supported by this script.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from tqdm import tqdm
import argparse
import csv
import os
import time
import random
import re
# import sys
# import pprint as pp


_SCRIPT_NAME = 'bm-genecards-annotate'
"""The name of the script that is implemented in this module (`str`)
"""
_DESC = __doc__
"""A description of the script (`str`)
"""
_ANNOTATION_CACHE = {}
"""A cache for completed queries to prevent excessive web queries (`dict`)
"""

SEARCH = "https://www.genecards.org/"
"""The main search page for genecards (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_cards(infile, driver, outfile=None, verbose=False, tmp_dir=None,
                   no_annotation=False, gene_id=con.ENSEMBL_GENE_ID,
                   delimiter=con.DEFAULT_DELIMITER,
                   encoding=con.DEFAULT_ENCODING,
                   comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    and output file of gene card annotations.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    driver : `selenium.webdriver.Chrome`
        A chrome webdriver, others will probably work but are untested.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    no_annotation : `bool`, optional, default: `False`
        Report all the genes that have no annotations in gene cards in addition
        to those that do have an annotation. This is not implemented yet and
        those with no annotations are output as a default.
    gene_id : `str`, optional, default: `bio_misc.common.constants.ENSEMBL_GENE_ID`
        An Ensembl gene ID column containing the genes to search for. Other IDs
        will also probably work in many cases but an Ensembl ID should work in
        all cases.
    delimiter : `str`, optional, default `bio_misc.common.constants.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `bio_misc.common.constants.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `bio_misc.common.constants.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    # This will open an output file if outfile is not None or - otherwise 'out'
    # will be STDOUT
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_gene_cards(infile, driver, gene_id=gene_id,
                                 no_annotation=no_annotation,
                                 yield_header=True, delimiter=delimiter,
                                 encoding=encoding,
                                 comment=comment),
                desc="[progress] fetching genecards annotations: ",
                unit=" returned annotations",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_gene_cards(infile, driver, gene_id=con.ENSEMBL_GENE_ID,
                     no_annotation=False, yield_header=True,
                     delimiter=con.DEFAULT_DELIMITER,
                     encoding=con.DEFAULT_ENCODING,
                     comment=con.DEFAULT_COMMENT):
    """Yield rows of gene annotation data.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    driver : `selenium.webdriver.Chrome`
        A chrome webdriver, others will probably work but are untested.
    gene_id : `str`, optional, default: `bio_misc.common.constants.ENSEMBL_GENE_ID`
        An Ensembl gene ID column containing the genes to search for. Other IDs
        will also probably work in many cases but an Ensembl ID should work in
        all cases.
    no_annotation : `bool`, optional, default: `False`
        Report all the genes that have no annotations in gene cards in addition
        to those that do have an annotation. This is not implemented yet and
        those with no annotations are output as a default.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    delimiter : `str`, optional, default `bio_misc.common.constants.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `bio_misc.common.constants.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `bio_misc.common.constants.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If the gene_id column can not be found in the header.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    with utils.BaseParser(infile, **kwargs) as parser:
        # Get the location of the gene column
        gene_id_idx = parser.get_col_idx(gene_id)
        if yield_header is True:
            yield parser.header + \
                ['summary', 'synonyms']

        for idx, row in enumerate(parser, 1):
            gene_id = row[gene_id_idx]
            gene_info = query_genecards(driver, gene_id)

            # Join the summary and alias information into a flat structure
            row.append(" ".join(gene_info['summaries']))
            row.append("|".join(gene_info['aliases']))
            yield row

            # To slow down the queries
            random_sleep(1, 2)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def random_sleep(lower_limit, upper_limit):
    """Sleep for a random amount of time between the lower limit and the upper
    limit.

    Parameters
    ----------
    lower_limit :`float`
        The lower limit to sleep for
    upper_limit :`float`
        The upper limit to sleep for

    Returns
    -------
    sleep_time :`float`
        The duration of the sleep
    """
    sleep_time = random.uniform(float(lower_limit), float(upper_limit))

    # Perform the random sleep betwen both values
    time.sleep(sleep_time)

    return sleep_time


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_genecards(driver, ensembl_id):
    """Query gene cards for a gene ID and extract the synonyms and summary
    information.

    Parameters
    ----------
    driver : `selenium.webdriver.Chrome`
        A chrome webdriver, others will probably work but are untested.
    ensembl_id : `str`
        An Ensembl gene ID to search for. Other IDs will also probably work in
        many cases but an Ensembl ID should work in all cases.

    Returns
    -------
    summary_info : `dict`
        The summary information downloaded from gene cards. The `dict` has two
        keys, ``aliases`` that has a list of alias names for the gene and
        ``summaries`` that contains free text information that GeneCards has on
        the gene.

    Notes
    -----
    There is a module level cache that is used to store previous query results.
    If the gene is present in the cache then it is used instead of querying
    gene cards. This is to reduce the number of expensive queries for longer
    repetitive gene lists.
    """
    try:
        # First see if the information has been downloaded previously
        return _ANNOTATION_CACHE[ensembl_id]
    except KeyError:
        pass

    # Navigate to the search page and get the web elements needed to conduct
    # the search
    search_box, go_button = goto_search(driver)
    search_box.send_keys(ensembl_id)
    go_button.click()

    info = dict(aliases=[], summaries=[])
    info['aliases'] = get_alias(driver)
    info['summaries'] = get_summaries(driver)
    _ANNOTATION_CACHE[ensembl_id] = info
    return info


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def goto_search(driver):
    """Navigate to the GeneCards gene search page and and find the search text
    box (and clear it) and the search GO button.

    Parameters
    ----------
    driver : `selenium.webdriver.Chrome`
        A chrome webdriver, others will probably work but are untested.

    Returns
    -------
    search_box : `selenium.WebElement`
        A cleared search box where the gene ID will be placed.
    go_button : `selenium.WebElement`
        The '\'GO\' button to initiate the search by calling it's ``click()``
        method

    Raises
    ------
    IndexError
        If either the search for the search box or button elements returns 0 or
        >1 result. This probably indicates a website structure change.
    """
    errors = 0

    # Allow for the possibility that the search box does not load first time
    # I have noticed some of these errors popping up on a long run
    while errors < 4:
        # Navigate to the search page
        driver.get(SEARCH)

        search_box = driver.find_elements_by_xpath(
            '//div[@data-ng-controller = "randomGeneController" and '
            '@ng-init="init(\'TRPV1\')"]//div[@class = "input-group"]/'
            'input[@type = "text" and @data-ng-model="currentGene" '
            'and @ng-enter="goToLookup()"]'
        )

        if len(search_box) == 1:
            break
        errors += 1
        time.sleep(1)

    if len(search_box) != 1:
        raise IndexError(
            "expected a single search box not: {0}".format(len(search_box))
        )

    search_box = search_box[0]
    default_gene = search_box.get_attribute("value")

    button = driver.find_elements_by_xpath(
        '//div[@class = "input-group"]/span[@class = "input-group-btn"]/'
        'button[@data-ga-label="{0}" and text() = "GO"]'.format(default_gene)
    )

    if len(button) != 1:
        raise IndexError(
            "expected a single button not: {0}".format(len(button))
        )

    # Other options to clear the search box if clear stops working
    # search_box.sendKeys(Keys.CONTROL + "a")
    # search_box.sendKeys(Keys.DELETE)
    # Remove the content from the search box
    search_box.clear()

    # Return the elements needed to execute the query
    return search_box, button[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_alias(driver):
    """Get the aliases for a gene.

    Parameters
    ----------
    driver : `selenium.webdriver.Chrome`
        A chrome webdriver, others will probably work but are untested.

    Returns
    -------
    aliases : `list` of `str`
        The aliases for the gene.

    Notes
    -----
    This requires that the browser is already on the webpage with all the gene
    information in it.
    """
    alias_fields = driver.find_elements_by_xpath(
        '//section[@id="aliases_descriptions"]//li'
    )
    aliases = []
    for i in alias_fields:
        gene_alias = i.text
        refs = get_superscript_text(i)

        # Remove trailing superscript text that is not part of the alias
        aliases.append(re.sub(r'\s+{0}$'.format(refs), '', gene_alias))
    return aliases


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_superscript_text(element):
    """Get any superscript text that is a child element of the element that has
    been passed to this function.

    Parameters
    ----------
    element : `selenium.WebElement`
        A web element that may or maynot have a superscript HTML element below
        it.

    Returns
    -------
    superscript_text : `str`
        The text of all the superscript elements separated by spaces. This is
        useful to remove any trailing superscripts from the text of the parent
        element.
    """
    superscript = element.find_elements_by_xpath('.//sup')
    return " ".join([i.text for i in superscript])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_summaries(driver):
    """Get the summary text for a gene.

    Parameters
    ----------
    driver : `selenium.webdriver.Chrome`
        A chrome webdriver, others will probably work but are untested.

    Returns
    -------
    summary : `list` of `str`
        The summaries for the gene.

    Notes
    -----
    This requires that the browser is already on the webpage with all the gene
    information in it.
    """
    summaries = driver.find_elements_by_xpath(
        '//section[@id="summaries"]/div[@class="gc-subsection"]'
    )
    summary_text = []
    for i in summaries:
        text = i.text
        try:
            # The heading line is modified to be within double square brackets
            heading = i.find_element_by_xpath(
                './div[@class="gc-subsection-header"]'
            )
            text = re.sub(
                r'^{0}'.format(heading.text),
                '[[{0}]]'.format(heading.text),
                text
            )
        except Exception:
            # Probably no /div with gc-subsection-header
            pass

        summary_text.append(text.replace('\n', ' '))
    return summary_text


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise_chrome(driver_path=None, download_dir=None, headless=False):
    """
    Initialise a chrome webdriver to download the files. Note that I can't make
    this headless at the moment as downloading does not seem to work in
    headless mode

    Parameters
    ----------
    driver_path : `str` or `NoneType`, optional, default: `NoneType`
        The path to the chrome webdriver. If it is ``NoneType`` then it is
        assumed to be within your PATH.
    download_dir : `str` or `NoneType`, optional, default: `NoneType`
        An alternate location for file downloads. `NoneType` means use the
        default location.
    headless : `str` or `NoneType`, optional, default: `NoneType`
        Operate in headless mode. I have had mixed results with headless
        operation.

    Returns
    -------
    webdriver : `selenium.webdriver`
        The webdriver to control the browser.
    """
    chrome_options = Options()
    # Downloads do not work in headless mode - see here for potential solutions
    # https://github.com/TheBrainFamily/chimpy/issues/108
    if headless is True:
        chrome_options.add_argument("--headless")

    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-gpu")

    if download_dir is not None:
        prefs = {"profile.default_content_settings.popups": 0,
                 "download.default_directory": download_dir,
                 "directory_upgrade": True}
        chrome_options.add_experimental_option("prefs", prefs)

    kwargs = {'options': chrome_options}
    if driver_path is not None:
        kwargs['executable_path'] = driver_path

    driver = webdriver.Chrome(**kwargs)

    return driver


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    driver = initialise_chrome(headless=args.headless)

    try:
        get_gene_cards(
            args.infile, driver, outfile=args.outfile,
            delimiter=args.delimiter, verbose=args.verbose,
            encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, gene_id=args.gene_id,
            no_annotation=args.missing
        )
    finally:
        driver.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, uniprot_id=False)
    parser.add_argument(
        '--missing', action="store_true",
        help="Include rows that have no annotation in the output file."
    )
    parser.add_argument(
        '--headless', action="store_true",
        help="Attempt to run headless (i.e. no browser window), this may not"
        " work."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
