"""Assign rsIDs to the ID field in a VCF file. This uses a tabix mapper so is
 not suitable for massive VCF files. It was largely written to assign rsIDs to
 genomic chunks when lifting over UK BioBank.

Please note, this can not handle multi-allelic sites. Please make sure these
are converted to bi-alleilic sites before using. Also, you will require access
to gwas-norm to use this, which is currently private. If you do not have access
then this will not work.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name,
)
from Bio import bgzf
from pyaddons import log, utils
from tqdm import tqdm
from gwas_norm.variants import mapper
import stdopen
import argparse
import sys
import os
import re
import csv
# import pprint as pp


_SCRIPT_NAME = "bm-vcf-id-mapper"
"""The name of the script (`str`)
"""

# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""

VCF_COLUMNS = [
    '#CHROM',
    'POS',
    'ID',
    'REF',
    'ALT',
    'QUAL',
    'FILTER',
    'INFO'
]
CHR_COL = VCF_COLUMNS.index('#CHROM')
START_COL = VCF_COLUMNS.index('POS')
ID_COL = VCF_COLUMNS.index('ID')
REF_COL = VCF_COLUMNS.index('REF')
ALT_COL = VCF_COLUMNS.index('ALT')
UNMAPPED_VARIANT = '.'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``bio_misc.annotations.vcf_id_mapper.run_var_id_mapper``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)
    prog_verbose = log.progress_verbose(verbose=args.verbose)
    try:
        run_var_id_mapper(args.vcf_file, args.mapper_file,
                          output_file=args.outfile,
                          verbose=prog_verbose,
                          tmpdir=args.tmpdir)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'mapper_file', type=str, help="The path to the mapper file."
    )
    parser.add_argument(
        '-i', '--vcf-file',
        type=str,
        help="The input VCF file, if not provided, input is assumed to be"
        " from STDIN"
    )
    parser.add_argument(
        '-o', '--outfile',
        type=str,
        help="The output VCF file, if not provided, output is assumed to be"
        " to STDOUT"
    )
    parser.add_argument(
        '-T', '--tmpdir',
        type=str,
        help="File output is written via temp, you can change the temp"
        " directory here. The default is to use the system tmp location."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Give more output, use -vv to turn on progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_var_id_mapper(vcf_file, mapper_file, output_file=None, verbose=False,
                      tmpdir=None):
    """The API entry point for running the mapper.

    Parameters
    ----------
    vcf_file : `str`
        The path to the input VCF file. If not given then input is expected
        from STDIN.
    mapper_file : `str`
        The path to the mapper file.
    output_file : `str`, optional, default: `NoneType`
        The output file name if not given output will be to STDOUT.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    tmpdir : `str`, optional, default: `NoneType`
        The location of tmp. If not given system tmp will be used. Output files
        will be written via tmp.
    """
    in_method = utils.get_open_method(vcf_file, allow_none=True)
    out_method = utils.get_open_method(output_file, allow_none=True)

    if output_file is not None and re.search(r'b?.gz$', output_file):
        out_method = bgzf.open

    # Create a no frills mapper
    map_obj = mapper.TabixVcfVariantMapper(mapper_file)

    with stdopen.open(vcf_file, mode="rt", method=in_method) as infile:
        with stdopen.open(output_file, mode="wt",
                          method=out_method, use_tmp=True,
                          tmpdir=tmpdir) as outfile:
            header_rows = 0
            row = '##'
            while row.startswith('##'):
                try:
                    row = next(infile)
                except StopIteration as e:
                    raise ValueError("no VCF data") from e
                header_rows += 1
                outfile.write(row)

            if header_rows == 0:
                raise ValueError("no VCF header rows found '##")

            sample_line = row.split("\t")
            if sample_line[:8] != VCF_COLUMNS:
                raise ValueError("unexpected sample line")

            reader = csv.reader(infile, delimiter="\t")

            tqdm_kwargs = dict(
                unit=" rows",
                desc="[info] mapping VCF",
                disable=not verbose
            )

            map_obj = mapper.TabixVcfVariantMapper(mapper_file)
            map_obj.open()
            try:
                for row in tqdm(reader, **tqdm_kwargs):
                    try:
                        start_pos = int(row[START_COL])
                    except (TypeError, ValueError):
                        # issue with the start coordinate
                        row[ID_COL] = UNMAPPED_VARIANT
                        outfile.write("{0}\n".format("\t".join(row)))
                        continue

                    mapping = map_obj.map_variant(
                        row[CHR_COL], start_pos, row[REF_COL], row[ALT_COL],
                        allele_norm=False, var_id=row[ID_COL]
                    )

                    try:
                        row[ID_COL] = mapping.map_row[2]
                    except Exception:
                        # Any unforeseen issues
                        # TODO: Tidy this up
                        row[ID_COL] = UNMAPPED_VARIANT

                    # If there is a NoneType variant make sure it is set to
                    # missing variant
                    if row[ID_COL] is None:
                        row[ID_COL] = UNMAPPED_VARIANT

                    outfile.write("{0}\n".format("\t".join(row)))
            finally:
                map_obj.close()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
