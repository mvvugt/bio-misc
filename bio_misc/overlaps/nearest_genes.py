"""Get the nearest genes to coordinates specified in an input file (or STDIN)
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con
from ensembl_rest_client import client, utils as rest_utils
from tqdm import tqdm
import argparse
import csv
import os
# import sys
# import pprint as pp


DEFAULT_REST = 'https://rest.ensembl.org'
DEFAULT_NGENES = 1
HEADER = [
    ('ensembl_gene_id', 'gene_id'),
    ('external_gene_id', 'external_name'),
    ('gene_chr_name', 'seq_region_name'),
    ('gene_start_pos', 'start'),
    ('gene_end_pos', 'end'),
    ('gene_strand', 'strand'),
    ('gene_biotype', 'biotype'),
    ('gene_description', 'description'),
    ('gene_distance_rank_from_input', 'distance_rank'),
    ('gene_distance_rank_biotype_from_input', 'distance_rank_biotype'),
    ('gene_min_distance_from_input', 'min_dist'),
    ('gene_overlap_with_input', 'overlap'),
    ('gene_assembly', 'assembly_name'),
    ('gene_version', 'version'),
    ('gene_source', 'source')
]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_nearest_genes(infile, rest_client, species, outfile=None,
                      chr_name=con.CHR_NAME, start_pos=con.START_POS,
                      end_pos=None, ngenes=1, delimiter=con.DEFAULT_DELIMITER,
                      verbose=False, encoding=con.DEFAULT_ENCODING,
                      tmp_dir=None, comment=con.DEFAULT_COMMENT):
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_nearest_genes(infile, rest_client, species,
                                    chr_name=chr_name, start_pos=start_pos,
                                    end_pos=start_pos, ngenes=ngenes,
                                    encoding=encoding, delimiter=delimiter,
                                    comment=comment),
                desc="[progress] fetching nearest genes: ",
                unit=" returned genes"
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_nearest_genes(infile, rest_client, species, chr_name=con.CHR_NAME,
                        start_pos=con.START_POS, end_pos=None, ngenes=1,
                        encoding=con.DEFAULT_ENCODING, yield_header=True,
                        comment=con.DEFAULT_COMMENT,
                        delimiter=con.DEFAULT_DELIMITER):
    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        end_pos=end_pos,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )
    with utils.CoordsParser(infile, **kwargs) as parser:
        if yield_header is True:
            yield parser.header + ['nearest_gene_input_idx'] + \
                [i[0] for i in HEADER]
        for idx, row in enumerate(parser, 1):
            chr_name, start_pos, end_pos = parser.get_coords(row)
            nearest_genes = rest_utils.get_n_nearest_genes(
                rest_client, species, chr_name, start_pos, end_pos=end_pos,
                use_center=False, biotypes=None, ngenes=ngenes
            )

            for i in nearest_genes:
                yield row + [idx] + process_record(i)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_record(record):
    """
    """
    processed_record = []
    for header_name, rest_name in HEADER:
        try:
            processed_record.append(record[rest_name])
        except KeyError:
            if rest_name == 'external_gene_id':
                processed_record.append(record['gene_id'])
            elif rest_name == 'distance_rank':
                processed_record.append(1)
            else:
                processed_record.append(None)
    return processed_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog('bm-nearest-genes', pkg_name, __version__)
    m.msg_args(args)

    rest = client.Rest(url=args.rest)

    get_nearest_genes(
        args.infile, rest, args.species, outfile=args.outfile,
        chr_name=args.chr_name, start_pos=args.start_pos, end_pos=args.end_pos,
        ngenes=args.ngenes, delimiter=args.delimiter, verbose=args.verbose,
        encoding=args.encoding, comment=args.comment, tmp_dir=args.tmp_dir
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Find the nearest genes to regions that have been supplied"
        " in an input file"
    )
    parser.add_argument(
        'infile', type=str, nargs='?',
        help="An input file, if not supplied or - then input is"
        "assumed to be from STDIN. All input must have a header."
    )
    parser.add_argument(
        '-o', '--outfile', type=str,
        help="An output file, if not supplied then output is assumed to be "
        "to STDOUT."
    )
    parser.add_argument(
        '-d', '--delimiter', type=str, default=con.DEFAULT_DELIMITER,
        help="The delimiter of the input and output (default: {}).".format(
            con.DEFAULT_DELIMITER
        )
    )
    parser.add_argument(
        '-t', '--tmp-dir', type=str,
        help="The tmp directory to use (default: system temp)."
    )
    parser.add_argument(
        '--comment', type=str, default=con.DEFAULT_COMMENT,
        help="The comment character in the file. (default: {})".format(
            con.DEFAULT_COMMENT
        )
    )
    parser.add_argument(
        '-E', '--encoding', type=str, default=con.DEFAULT_ENCODING,
        help="The default encoding of the file (default: {}).".format(
            con.DEFAULT_ENCODING
        )
    )
    parser.add_argument(
        '-r', '--rest', type=str, default=DEFAULT_REST,
        help="The Ensembl REST endpoint to use. (default: {})".format(
            DEFAULT_REST
        )
    )
    parser.add_argument(
        '-n', '--ngenes', type=int, default=1,
        help="The number of nearest genes to output (default: {}).".format(
            DEFAULT_NGENES
        )
    )
    parser.add_argument(
        '--species', type=str, default='human',
        help="The species for the gene lookup (default: human)."
    )
    parser.add_argument(
        '-c', '--chr-name', type=str, default=con.CHR_NAME,
        help="The name of the chromosome column (default: {}).".format(
            con.CHR_NAME
        )
    )
    parser.add_argument(
        '-s', '--start-pos', type=str, default=con.START_POS,
        help="The start position column (default: {}).".format(
            con.START_POS
        )
    )
    parser.add_argument(
        '-e', '--end-pos', type=str,
        help="The end position column if not supplied then start position is"
        " used as end position."
    )
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
