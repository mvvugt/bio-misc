"""Take sets of loci regions and look for any loci are that are link via trans
pQTL associations. So, there are cis-pQTLs in the one loci and trans pQTLs
(for one of the genes in the cis loci) in a separate loci.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bx.intervals import intersection
from tqdm import tqdm
import math
import pandas as pd
import gzip
import argparse
import csv
import os
import sys
import pysam
import pprint as pp


_SCRIPT_NAME = 'bm-trans-qtl-link'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_trans_qtl_link(infile, tabix_file, outfile=None, chr_name=con.CHR_NAME,
                       start_pos=con.START_POS, end_pos=None,
                       delimiter=con.DEFAULT_DELIMITER, verbose=False,
                       encoding=con.DEFAULT_ENCODING, tmp_dir=None,
                       comment=con.DEFAULT_COMMENT, loci_id='loci_id',
                       ensembl_gene_id=con.ENSEMBL_GENE_ID,
                       pvalue=5E-08):
    """The main high level API function that takes an input file and generates
    and output file of matching entries.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chain_file : `str`
        The path to the chain file that is used to map between the source
        assembly and the target assembly.
    fail_file : `str`
        The path to a file where the rows that are unable to be lifted over
        will be written.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional, default: `NoneType`
        The end position column in the input file, if not present then the
        start position is used for end position.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    verbose : `bool`, optional, default: `False`
        Report progress.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Returns
    -------
    liftover_fails : `int`
        The number of rows that failed to liftover and were written to
        ``fail_file``.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_trans_qtl_link(
                    infile, tabix_file, chr_name=chr_name,
                    yield_header=True, start_pos=start_pos,
                    end_pos=end_pos, encoding=encoding,
                    comment=comment, loci_id=loci_id,
                    ensembl_gene_id=ensembl_gene_id,
                    delimiter=delimiter,
                    pvalue=pvalue,
                ),
                desc="[progress] trans links: ",
                unit=" returned rows",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_trans_qtl_link(infile, tabix_file, chr_name=con.CHR_NAME,
                         yield_header=True, start_pos=con.START_POS,
                         end_pos=None, encoding=con.DEFAULT_ENCODING,
                         comment=con.DEFAULT_COMMENT, loci_id='loci_id',
                         ensembl_gene_id=con.ENSEMBL_GENE_ID,
                         pvalue=5E-08, flank=1E6,
                         delimiter=con.DEFAULT_DELIMITER):
    """Yield rows of lifted over coordinates.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chain_file : `str`
        The path to the chain file that is used to map between the source
        assembly and the target assembly.
    fail_file : `str`
        The path to a file where the rows that are unable to be lifted over
        will be written.
    fail_counter : `dict`
        A dictionary where the number of failed liftovers are written. This is
        required as this function yields and can not return anything.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional, default: `NoneType`
        The end position column in the input file, if not present then the
        start position is used for end position.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to the fail file, a temp file is used and copied over to the
        final location when finished. Use this to specify a different temp
        location. If `NoneType` then the system default temp location is used.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    """
    # The lookup file has -log10 pvalues
    pvalue = -math.log10(pvalue)

    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        end_pos=end_pos,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    # Open the tabix file for reading and grab the header for it
    lookup, tabix_header = _open_lookup(tabix_file)

    # We need to read in the loci and cache them for this
    output_header, loci, loci_regions, gene_set = _read_loci(
        infile, loci_id, ensembl_gene_id, tabix_header, flank,
        **kwargs
    )

    if yield_header is True:
        yield output_header

    # Catch all the region data, so all variants that overlap with
    # the genes
    region_cache = _cache_regions(
        loci, lookup, tabix_header, gene_set, pvalue
    )

    for source_id, source_loc in loci.items():
        source_chr, source_start, source_end = _get_region_bounds(source_loc)

        for test_id, test_loc in loci.items():
            if source_id == test_id:
                continue
            # print(source_id, test_id)
            # Do the source and test regions overlaps, if so then it is not
            # a trans region
            test_chr, test_start, test_end = _get_region_bounds(test_loc)
            overlaps = loci_regions[source_id].find(test_start, test_end)

            if len(overlaps) > 1 and overlaps[0][0] == test_chr:
                continue

            test_region = region_cache[test_id]
            for gene_coords, source_row in source_loc:
                try:
                    matches = test_region[
                        test_region.ensembl_gene_id == gene_coords[3]
                    ]

                    if matches.shape[0] > 0:
                        for m in matches.itertuples(index=False):
                            dist_from_test = -1
                            if source_chr == test_chr:
                                dist_from_test = min(
                                    abs(m.start_pos - source_start),
                                    abs(m.start_pos - source_end)
                                )
                            yield (
                                source_row +
                                [source_id, test_id, dist_from_test] +
                                list(m)
                            )
                            # print(matches.shape)
                except AttributeError:
                    # Matches is empty
                    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _cache_regions(loci, lookup, header, gene_set, pvalue):
    region_cache = {}
    for lid, loc in loci.items():
        # print(lid)
        test_chr, test_start, test_end = _get_region_bounds(loc)
        # print(test_chr, test_start, test_end)
        region_cache[lid] = _query_region(
            lookup, test_chr, test_start, test_end, header,
            pvalue=pvalue, genes=gene_set
        )
        # print(region_cache[lid].shape)
    return region_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_loci(infile, loci_id, ensembl_gene_id, tabix_header, flank,
               **kwargs):
    loci = {}
    loci_regions = {}
    gene_set = set()

    with utils.CoordsParser(infile, **kwargs) as parser:
        # test_header = ["test_" for i in parser.header]
        output_header = parser.header + parser.get_unique_col(
            ['source_loci_idx', 'matching_loci_idx', 'dist_from_test'] +
            tabix_header
        )

        for idx, row in enumerate(parser, 1):
            chr_name, start_pos, end_pos = parser.get_coords(row)
            loci_idx = parser.get_col_idx(loci_id)
            gene_idx = parser.get_col_idx(ensembl_gene_id)

            try:
                loci[row[loci_idx]].append(
                    [[chr_name, start_pos, end_pos, row[gene_idx]], row]
                )
                loci_regions[row[loci_idx]].insert(
                    max(1, start_pos - flank), end_pos + flank, (chr_name, row[gene_idx])
                )
            except KeyError:
                loci[row[loci_idx]] = [
                    [[chr_name, start_pos, end_pos, row[gene_idx]], row]
                ]
                loci_regions[row[loci_idx]] = intersection.IntervalTree()
                loci_regions[row[loci_idx]].insert(
                    start_pos, end_pos, (chr_name, row[gene_idx])
                )
            gene_set.add(row[gene_idx])

    gene_set = list(gene_set)
    return output_header, loci, loci_regions, gene_set


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _open_lookup(tabix_file):
    with gzip.open(tabix_file, 'rt') as lookup_file:
        reader = csv.reader(lookup_file, delimiter="\t")
        header = next(reader)
    lookup = pysam.TabixFile(tabix_file)
    return lookup, header


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_region_bounds(loci):
    """
    """
    chr_name = set([i[0][0] for i in loci])

    if len(chr_name) > 1:
        pp.pprint(chr_name)
        raise KeyError("loci should be on the same chromsome")
    chr_name = list(chr_name)[0]
    start = sorted(loci, key=lambda x: x[0][1])[0][0][1]
    end = sorted(loci, key=lambda x: x[0][2], reverse=True)[0][0][2]
    return chr_name, start, end


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _query_region(lookup, chr_name, region_start, region_end, header,
                  genes=[], pvalue=8.3):
    """
    """
    gene_idx = header.index('ensembl_gene_id')
    pval_idx = header.index('pvalue')
    region = pd.DataFrame(
        _do_query(
            lookup, chr_name, region_start, region_end, gene_idx, pval_idx,
            genes=genes, pvalue=pvalue
        )
    )
    if len(region) > 0:
        region.columns = header
    return region


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _do_query(lookup, chr_name, region_start, region_end, gene_idx, pval_idx,
              genes=[], pvalue=8.3):
    """
    """
    for row in lookup.fetch(chr_name, region_start, region_end,
                            parser=pysam.asTuple()):
        # print(row[gene_idx])
        # print(float(row[pval_idx]))
        if row[gene_idx] in genes and float(row[pval_idx]) >= 8.3:
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        get_trans_qtl_link(
            args.infile, args.tabix_qtl_file, outfile=args.outfile,
            delimiter=args.delimiter, encoding=args.encoding,
            comment=args.comment, tmp_dir=args.tmp_dir, verbose=args.verbose,
            chr_name=args.chr_name, start_pos=args.start_pos,
            end_pos=args.end_pos, loci_id=args.loci_id,
            ensembl_gene_id=args.gene_id, pvalue=args.pvalue
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Link distinct loci via trans pQTLs"
    )
    parser.add_argument(
        'tabix_qtl_file',
        default=str,
        help="A tabix indexed QTL lookup file"
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_genomic_coords_arguments(parser)
    parser.add_argument(
        '--pvalue', type=float, default=5E-08,
        help="The p-value cut off below which we include trans-signals"
    )
    parser.add_argument(
        '-L', '--loci-id', type=str, default="loci_idx",
        help="A column indicating the genes belonging to a locus, the input"
        " file should be ordered on this"
    )
    parser.add_argument(
        '--gene-id', type=str, default="ensembl_gene_id",
        help="An ensembl gene ID column"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
