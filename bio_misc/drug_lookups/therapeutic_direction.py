"""
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.drug_lookups import (
    chembl_queries as cq,
    bnf_queries as bq,
    target_effects as te
)
from tqdm import tqdm
import argparse
import csv
import os
import sys
import re
import math
# import pprint as pp


PHENO_FILE_DELIMITER = "\t"
_SCRIPT_NAME = "bm-therapeutic-direction"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_therapeutic_direction(infile, chembl_conn, bnf_conn, pheno_dir=None,
                              outfile=None, verbose=False,
                              prot_id=con.UNIPROT_PROT_ID, pheno='phenotype',
                              beta='beta', tmp_dir=None,
                              comment=con.DEFAULT_COMMENT,
                              delimiter=con.DEFAULT_DELIMITER,
                              encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of therapeutic directions.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    pheno_dir : `dict` or `NoneType`, optional, default: `NoneType`
        The keys from the dictionary are phenotypes and the values are the
        integer pathological direction where 1 is a positive beta being
        pathogenic and -1 being a negative beta being pathogenic. If this is
        ``NoneType`` then the default will be a positive beta is pathogenic and
        a negative beta is protective for all phenotypes (outcomes).
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    pheno : `str`, optional, default: `phenotype`
        The name of the phenotype (MR outcome) column.
    beta : `str`, optional, default: `beta`
        The name of the MR point estimate column, note that this should either
        be a beta or a log(OR) and  not OR.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_therapeutic_direction(
                    infile, chembl_conn, bnf_conn, pheno_dir=pheno_dir,
                    prot_id=prot_id, pheno=pheno, beta=beta, comment=comment,
                    delimiter=delimiter, encoding=encoding, yield_header=True
                ),
                desc="[progress] fetching therapeutic direction: ",
                unit=" effect(s)",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_therapeutic_direction(infile, chembl_conn, bnf_conn, pheno_dir=None,
                                prot_id=con.UNIPROT_PROT_ID, pheno='phenotype',
                                beta='beta', comment=con.DEFAULT_COMMENT,
                                delimiter=con.DEFAULT_DELIMITER,
                                encoding=con.DEFAULT_ENCODING,
                                yield_header=True):
    """Yield therapeutic direction results from an input file.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    pheno_dir : `dict` or `NoneType`, optional, default: `NoneType`
        The keys from the dictionary are phenotypes and the values are the
        integer pathological direction where 1 is a positive beta being
        pathogenic and -1 being a negative beta being pathogenic. If this is
        ``NoneType`` then the default will be a positive beta is pathogenic and
        a negative beta is protective for all phenotypes (outcomes).
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    pheno : `str`, optional, default: `phenotype`
        The name of the phenotype (MR outcome) column.
    beta : `str`, optional, default: `beta`
        The name of the MR point estimate column, note that this should either
        be a beta or a log(OR) and  not OR.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.

    Yields
    ------
    output_row : `list`
        The therapeutic direction summary of for the protein ID and a drug
        indication against that target.
    """
    chembl_session = chembl_conn()
    bnf_session = bnf_conn()

    kwargs = dict(
        comment=comment,
        delimiter=delimiter,
        encoding=encoding
    )

    pheno_sign_func = mr_dir_sign
    if pheno_dir is not None:
        pheno_sign_func = pheno_dir_sign

    try:
        with utils.BaseParser(infile, **kwargs) as parser:
            prot_id_idx = parser.get_col_idx(prot_id)
            pheno_idx = parser.get_col_idx(pheno)
            beta_idx = parser.get_col_idx(beta)

            if yield_header is True:
                yield parser.header + parser.get_unique_col(
                    ['mr_result_idx', 'mr_sign', 'pathological_dir',
                     'mr_effect_dir', 'predicted_therapeutic_mechanism',
                     'treatment_response'] + te.HEADER
                )

            for idx, row in enumerate(parser, 1):
                has_effects = False
                prot_id_value = row[prot_id_idx].strip()

                if prot_id is None:
                    continue

                phenotype_value = process_pheno(row[pheno_idx].strip())
                beta_value = float(row[beta_idx].strip())
                mr_sign = math.copysign(1, beta_value)
                pheno_sign = pheno_sign_func(
                    beta_value, phenotype_value, pheno_dir
                )

                mr_effect = 'unknown'
                mr_pred_mech = 'unknown'
                if mr_sign != pheno_sign:
                    mr_effect = 'protective'
                    mr_pred_mech = 'activation'
                elif mr_sign == pheno_sign:
                    mr_effect = 'risk'
                    mr_pred_mech = 'inhibition'

                mr_effects = [mr_sign, pheno_sign, mr_effect, mr_pred_mech]

                for summary_record in te.get_target_summary(
                        chembl_session, bnf_session, prot_id_value
                ):
                    if summary_record[te.DRUG_EFFECT_TYPE] == 'side_effect':
                        break

                    has_effects = True
                    summary_row = []
                    try:
                        for col in te.HEADER:
                            summary_row.append(summary_record[col])

                        treatment_response = get_treatment_response(
                            mr_pred_mech, summary_record[te.MECHANISM_TYPE]
                        )

                        yield row + [idx] + mr_effects + [treatment_response] \
                            + summary_row
                    except KeyError:
                        yield row + [idx] + mr_effects + [None] + \
                            summary_row + ([None] * (len(te.HEADER) - 6))

                if has_effects is False:
                    yield row + [idx] + mr_effects + \
                        [None, prot_id_value] + \
                        ([None] * (len(te.HEADER) - 1))
    finally:
        chembl_session.close()
        bnf_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def mr_dir_sign(beta, *args, **kwargs):
    """
    """
    return math.copysign(1, beta)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def pheno_dir_sign(beta, phenotype_value, pheno_dir):
    """
    """
    return pheno_dir[phenotype_value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_treatment_response(mr_mechanism, action_type):
    """
    """
    treatment_response = 'unknown'
    if mr_mechanism == 'activation' and action_type in cq.ACTIVE_MECH:
        treatment_response = 'beneficial'
    elif mr_mechanism == 'inhibition' and action_type in cq.INHIBIT_MECH:
        treatment_response = 'beneficial'
    elif action_type in cq.INHIBIT_MECH or action_type in cq.ACTIVE_MECH:
        treatment_response = 'adverse'
    return treatment_response


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_phenotype_file(pheno_file):
    """Read in the phenotype file.

    Parameters
    ----------
    pheno_file : `str`
        The path to the phenotype file.

    Returns
    -------
    phenotype_directions : `dict`
        The keys from the dictionary are phenotypes and the values are the
        integer pathological direction.

    Notes
    -----
    The phenotype file must have a column labelled ``phenotype`` and a column
    called ``pathological_direction`` where the if the pathological direction
    is increasing it will be coded as 1 and decreasing will be -1. The file
    must be tab delimited.
    """
    pheno_col = 'phenotype'
    patho_col = 'pathological_direction'
    pheno_directions = {}

    with utils.BaseParser(pheno_file,
                          delimiter=PHENO_FILE_DELIMITER) as parser:
        pheno_idx = parser.get_col_idx(pheno_col)
        patho_idx = parser.get_col_idx(patho_col)
        for row in parser:
            row[pheno_idx] = process_pheno(row[pheno_idx])
            pheno_directions[row[pheno_idx]] = int(row[patho_idx])
    return pheno_directions


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_pheno(pheno):
    """Basic phenotype standardisation

    Parameters
    ----------
    pheno : `str`
        The phenotype to standardise.

    Returns
    -------
    standard_pheno : `str`
        The standardised phenotype.
    """
    return re.sub(r'\W+', '-', pheno)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """Main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    # Session maker
    chembl_conn = cq.chembl_sqlite_connect(args.chembl_db)
    bnf_conn = bq.bnf_sqlite_connect(args.bnf_db)

    pheno_dir = None
    if args.pheno_dir is not None:
        pheno_dir = read_phenotype_file(args.pheno_dir)

    try:
        get_therapeutic_direction(
            args.infile, chembl_conn, bnf_conn, pheno_dir=pheno_dir,
            outfile=args.outfile, delimiter=args.delimiter,
            verbose=args.verbose, encoding=args.encoding,
            comment=args.comment, tmp_dir=args.tmp_dir,
            prot_id=args.prot_col, pheno=args.pheno_col, beta=args.beta_col
        )
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")

    # process_infile(
    #     chembl_sm, bnf_sm, args.mr_result, args.phenotype_direction,
    #     args.outfile, prot_col=args.prot_col, pheno_col=args.phenotype_col,
    #     beta_col=args.beta_col
    # )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """
    Initialise the command line arguments. We accept one argument that is
    optional. It is either the path to a database or the name of a section
    header in a configfile. If a section header in a config file then this
    should contain SQLAlchemy connection parameters

    Returns
    -------
    parser : `argpare.ArgumentParser`
        The "unparsed" argument parser
    """
    parser = argparse.ArgumentParser(
        description="Take a set of MR results, pull out drugs mapping to the"
        " targets and based on the pathological direction of the outcome and"
        " the drug mechanism of action on the target, determine if the drug"
        " treatment for the indicated disease will move the MR outcome in a"
        " more or less pathological direction"
    )
    parser.add_argument(
        'bnf_db', type=str,
        help="A path to the BNF SQLite database"
    )
    parser.add_argument(
        'chembl_db', type=str,
        help="A path to the ChEMBL SQLite database"
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        '--pheno-dir', type=str,
        help="A pathological phenotype direction file. This should have two"
        " columns and be tab-delimited. The first column should be named "
        "'phenotype' and have the MR outcome name as it appears in the "
        "mr_result file. The second column should be pathological_direction"
        " and should be 1 for increase in in the pathological direction or "
        "-1 for decrease is in the pathological direction. If this file is "
        "not provided then positive MR betas are deemed pathological and "
        "negative MR betas are deemed protective."
    )
    parser.add_argument(
        '--prot-col', type=str, default='uniprot_id',
        help="The name of the column with the uniprot ID in the MR results "
        "file"
    )
    parser.add_argument(
        '--pheno-col', type=str, default='phenotype_x',
        help="The name of the column with the phenotype in the MR results "
        "file - must match the phenotypes listed in phenotype_direction"
    )
    parser.add_argument(
        '--beta-col', type=str, default='point',
        help="The name of the column with the beta in the MR results "
        "file"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
