"""
"""
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, text
import pprint as pp
import os


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def bnf_sqlite_connect(db):
    """Connect to a SQLite database using SQLAlchemy.

    Parameters
    ----------
    db : `str`
        A path to an SQLite database

    Returns
    -------
    session_maker : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions:
        ``session = session_maker()``
    """
    engine = create_engine(
        'sqlite:////{0}'.format(
            os.path.realpath(os.path.expanduser(db))
        )
    )
    return sessionmaker(bind=engine)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_effects(session, chembl_target_id):
    """
    """
    sql = text(
        """
        select  cm.compound_chembl_id
        , cm.mapping_synonym
        , cm.compound_pref_name
        , cm.molecule_type
        , cm.full_mapping
        , d.drug_name as bnf_drug_name
        , ctc.target_mapping_relationship
        , ctc.mechanism_of_action
        , ctc.target_chembl_id
        , ctc.target_type
        , ctc.target_pref_name
        , ctc.action_type
        , ctc.uniprot_accession
        , ctc.uniprot_description
        , de.drug_effect_type
        , de.drug_effect_freq
        , de.drug_effect_name
        , de.drug_class_effect
        from chembl_target_components ctc
        join chembl_mapping cm
        on ctc.chembl_mapping_id = cm.chembl_mapping_id
        join drug_component dc
        on cm.drug_component_id = dc.drug_component_id
        join drug d on dc.drug_id = d.drug_id
        join drug_effect de on d.drug_id = de.drug_id
        where target_chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_target_id})
    return query.fetchall()
