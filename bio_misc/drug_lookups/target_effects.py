"""Get known drug effects for a target (indications and side effects). This
looks for uniprot IDs that map to ChEMBL efficacy targets. Note: this uses
both ChEMBL and the BNF get indications (both) and side effects (BNF).
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.drug_lookups import (
    chembl_queries as cq,
    bnf_queries as bq
)
from tqdm import tqdm
import argparse
import csv
import os
import sys
import re
# import pprint as pp


_SCRIPT_NAME = 'bm-drug-target-effects'
"""The name of the script that is implemented in this module (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""
_SUMMARY_CACHE = {}
"""A module level cache for the summary queries to prevent excessive
(expensive) database queries (`dict`)
"""

# Some column names in the summary
QUERY_PROTEIN = 'query_protein_id'
CHEMBL_ID = 'target_chembl_id'
PREF_NAME = 'target_pref_name'
ORGANISM = 'target_organism'
TYPE = 'target_type'
N_COMPONENTS = 'target_n_components'
DRUG_EFFECT_SOURCE = 'drug_effect_source'
DRUG_EFFECT_TYPE = 'drug_effect_type'
DRUG_EFFECT_FREQ = 'drug_effect_freq'
COMPOUND_NAME = 'compound_name'
COMPOUND_CHEMBL_ID = 'compound_chembl_id'
MECHANISM_TYPE = 'drug_mechanism'
MAX_PHASE = 'max_phase'
DRUG_EFFECT = 'drug_effect'
DIRECT_INTERACTION = 'drug_target_direct_interaction'
BINDING_SITE_COMMENT = 'drug_binding_site_notes'
DISEASE_EFFICACY = 'drug_efficacy_mechanism'
MECHANISM_COMMENT = 'drug_mechanism_notes'
MOLECULE_TYPE = 'drug_molecule_type'
FULL_MAPPING = 'drug_name_full_mapping'
DRUG_CLASS_EFFECT = 'drug_class_effect'

HEADER = [
    QUERY_PROTEIN,
    CHEMBL_ID,
    PREF_NAME,
    ORGANISM,
    TYPE,
    N_COMPONENTS,
    DRUG_EFFECT_SOURCE,
    DRUG_EFFECT_TYPE,
    DRUG_EFFECT_FREQ,
    COMPOUND_NAME,
    COMPOUND_CHEMBL_ID,
    MOLECULE_TYPE,
    MECHANISM_TYPE,
    MAX_PHASE,
    DRUG_EFFECT,
    DIRECT_INTERACTION,
    BINDING_SITE_COMMENT,
    DISEASE_EFFICACY,
    MECHANISM_COMMENT,
    FULL_MAPPING,
    DRUG_CLASS_EFFECT
]

# Other fields in the summary info but not in the output
BNF_EFFECTS = 'bnf_effects'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    # Session maker
    chembl_conn = cq.chembl_sqlite_connect(args.chembl)

    bnf_conn = None
    if args.bnf is not None:
        bnf_conn = bq.bnf_sqlite_connect(args.bnf)

    try:
        get_drug_target_effects(
            args.infile, chembl_conn, bnf_conn=bnf_conn, outfile=args.outfile,
            delimiter=args.delimiter, verbose=args.verbose,
            encoding=args.encoding, comment=args.comment, tmp_dir=args.tmp_dir,
            prot_id=args.prot_id, no_target=args.missing
        )
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, ensembl_id=False)
    arguments.add_drug_data_arguments(parser)
    arguments.add_missing_args(
        parser,
        help="Include proteins that have no ChEMBL target in the output"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()

    if args.chembl is None:
        raise ValueError("Must pass a ChEMBL database argument")

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_drug_target_effects(infile, chembl_conn, bnf_conn=None, outfile=None,
                            verbose=False, prot_id=con.UNIPROT_PROT_ID,
                            tmp_dir=None, comment=con.DEFAULT_COMMENT,
                            no_target=False, delimiter=con.DEFAULT_DELIMITER,
                            encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of drug summaries.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`, optional, default: `NoneType`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.

    See also
    --------
    :ref:`documentation page <dt_effects>`
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_drug_target_effects(
                    infile, chembl_conn, bnf_conn=bnf_conn, prot_id=prot_id,
                    no_target=no_target, comment=comment, delimiter=delimiter,
                    encoding=encoding, yield_header=True
                ),
                desc="[progress] fetching target effects: ",
                unit=" effect(s)",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_drug_target_effects(infile, chembl_conn, bnf_conn=None,
                              prot_id=con.UNIPROT_PROT_ID,
                              no_target=False, comment=con.DEFAULT_COMMENT,
                              delimiter=con.DEFAULT_DELIMITER,
                              encoding=con.DEFAULT_ENCODING,
                              yield_header=True):
    """Yield drug target summary results from an input file.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`, optional, default: `NoneType`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.

    Yields
    ------
    output_row : `list`
        The high level target summary of for the protein ID (including rows
        that have no target mapping).

    Notes
    -----
    The drug targets summary is a high level overview of counts of specific
    attributes on a target.

    See also
    --------
    :ref:`documentation page <dt_effects>`
    """
    chembl_session = chembl_conn()

    try:
        bnf_session = bnf_conn()
    except TypeError:
        # Probably NoneType, which is OK, if not then we raise
        if bnf_conn is not None:
            raise
        bnf_session = None

    kwargs = dict(
        comment=comment,
        delimiter=delimiter,
        encoding=encoding
    )

    try:
        with utils.BaseParser(infile, **kwargs) as parser:
            prot_id_idx = parser.get_col_idx(prot_id)

            if yield_header is True:
                yield parser.header + parser.get_unique_col(
                    ['target_effects_input_idx'] + HEADER
                )

            # Loop through each row in the input file
            for idx, row in enumerate(parser, 1):
                # Get the protein ID value from the row
                prot_id_value = row[prot_id_idx]

                # Will keep track on any drug effects that have been found
                # in the databases
                has_effects = False

                # If there is a protein ID for this row, then perform the
                # lookup
                if prot_id_value is not None:
                    for summary_record in get_target_summary(
                        chembl_session, bnf_session, prot_id_value
                    ):
                        has_effects = True
                        summary_row = []
                        try:
                            for col in HEADER:
                                summary_row.append(summary_record[col])
                            yield row + [idx] + summary_row
                        except KeyError:
                            if no_target is True:
                                yield row + [idx] + summary_row + \
                                    ([None] * (len(HEADER) - 6))

                # If no drug effects have been found and we are outputting
                # the rows with no target matches. Then yield a blank row
                if has_effects is False and no_target is True:
                    yield row + [idx, prot_id_value] + \
                        ([None] * (len(HEADER) - 1))
    finally:
        chembl_session.close()

        if bnf_session is not None:
            bnf_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_summary(chembl_session, bnf_session, prot_id):
    """Get a target summary for a given uniprot ID

    Parameters
    ----------
    chembl_session : `sqlalchemy.Session`
        The sesion object to query against ChEMBL.
    bnf_session : `sqlalchemy.Session` or `NoneType`
        The session object to query against the BNF. If not available pass
        `NoneType`.
    prot_id : `str`
        The protein ID to map to ChEMBL target identifiers. In reality this
        does not have to be a protein ID it could be anything that is a target
        component in ChEMBL. There are no restrictions on the query

    Returns
    -------
    target_summary : `dict`
        The full summary information on the target.
    """
    try:
        # First does a summary appear in the module level cache?
        return _SUMMARY_CACHE[prot_id]
    except KeyError:
        pass

    chembl_summary = cq.get_protein_info(chembl_session, prot_id)
    summaries = []
    for i in chembl_summary:
        has_effect = False

        if bnf_session is not None:
            i[BNF_EFFECTS] = [
                dict(be) for be in bq.get_target_effects(
                    bnf_session, i[CHEMBL_ID]
                )
            ]
        else:
            i[BNF_EFFECTS] = []

        summary_record = {
            QUERY_PROTEIN: prot_id,
            CHEMBL_ID: i[CHEMBL_ID],
            PREF_NAME: i[PREF_NAME],
            ORGANISM: i[ORGANISM],
            TYPE: i[TYPE],
            N_COMPONENTS: len(i[cq.COMPONENTS])
        }
        for ci in i[cq.INDICATIONS]:
            has_effect = True
            drug_effect = summary_record.copy()
            drug_effect[DRUG_EFFECT_SOURCE] = 'chembl'
            drug_effect[DRUG_EFFECT_TYPE] = 'indication'
            drug_effect[DRUG_EFFECT_FREQ] = 'all'
            drug_effect[COMPOUND_NAME] = ci['compound_pref_name']
            drug_effect[COMPOUND_CHEMBL_ID] = ci[COMPOUND_CHEMBL_ID]
            drug_effect[MOLECULE_TYPE] = ci['molecule_type']
            drug_effect[MECHANISM_TYPE] = ci['action_type']
            drug_effect[MAX_PHASE] = ci['max_phase_for_ind']
            drug_effect[DRUG_EFFECT] = ci['mesh_heading']
            drug_effect[DIRECT_INTERACTION] = ci['direct_interaction']
            drug_effect[BINDING_SITE_COMMENT] = ci['binding_site_comment']
            drug_effect[DISEASE_EFFICACY] = ci['disease_efficacy']

            try:
                drug_effect[MECHANISM_COMMENT] = \
                    re.sub(r'[\r\n]', '', ci['mechanism_comment'])
            except TypeError:
                drug_effect[MECHANISM_COMMENT] = None

            drug_effect[FULL_MAPPING] = None
            drug_effect[DRUG_CLASS_EFFECT] = None
            summaries.append(drug_effect)

        for be in i[BNF_EFFECTS]:
            has_effect = True
            drug_effect = summary_record.copy()
            drug_effect[DRUG_EFFECT_SOURCE] = 'bnf'
            drug_effect[DRUG_EFFECT_TYPE] = be['drug_effect_type']
            drug_effect[DRUG_EFFECT_FREQ] = be['drug_effect_freq']
            drug_effect[COMPOUND_NAME] = be['compound_pref_name']
            drug_effect[COMPOUND_CHEMBL_ID] = be[COMPOUND_CHEMBL_ID]
            drug_effect[MOLECULE_TYPE] = be['molecule_type']
            drug_effect[MECHANISM_TYPE] = be['action_type']
            drug_effect[MAX_PHASE] = 4
            drug_effect[DRUG_EFFECT] = be['drug_effect_name']
            drug_effect[DIRECT_INTERACTION] = None
            drug_effect[BINDING_SITE_COMMENT] = None
            drug_effect[DISEASE_EFFICACY] = None
            drug_effect[MECHANISM_COMMENT] = None
            drug_effect[FULL_MAPPING] = be['full_mapping']
            drug_effect[DRUG_CLASS_EFFECT] = be['drug_class_effect']
            summaries.append(drug_effect)

        # Here we have a ChEMBL target with no indication or side effect data
        # we generate a row with empty drug effect data, so the target is still
        # returned
        if has_effect is False:
            summaries.append(_get_missing_effect(summary_record))

    # Update the cache
    _SUMMARY_CACHE[prot_id] = summaries
    return summaries


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_missing_effect(summary):
    """This will add empty drug effects columns to the summary.

    Parameters
    ----------
    summary : `dict`
        The ChEMBL target summary. The empty drug effect fields will be added
        to this.

    Returns
    -------
    drug_effect_summary : `dict`
        The ChEMBL target summary with empty drug effect fields added.

    Notes
    -----
    This is used in  cases where, the target is in ChEMBL but does not have any
    indication data in ChEMBL or the BNF.
    """
    drug_effect = summary.copy()
    fields = [
        DRUG_EFFECT_SOURCE, DRUG_EFFECT_TYPE, DRUG_EFFECT_FREQ,
        COMPOUND_NAME, COMPOUND_CHEMBL_ID, COMPOUND_CHEMBL_ID,
        MOLECULE_TYPE, MECHANISM_TYPE, MAX_PHASE, DRUG_EFFECT,
        DIRECT_INTERACTION, BINDING_SITE_COMMENT, DISEASE_EFFICACY,
        MECHANISM_COMMENT, FULL_MAPPING, DRUG_CLASS_EFFECT
    ]
    for i in fields:
        drug_effect[i] = None
    return drug_effect


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
