"""Wrapper functions for running crossmap functionality.
"""
from bio_misc.common import utils
from cmmodule.utils import read_chain_file, map_coordinates
import gzip
import os
import shutil
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_chain(chain_file):
    """
    A wrapping function to provide a constant interface to the crossmap
    `cmmodule.utils.read_chain_file` function.

    Parameters
    ----------
    chain_file : str
        The path to the chain file

    Returns
    -------
    map_tree : dict
        The keys of the dict are chromosome names and the values are
        `bx.Interval` objects used to look for overlaps between the current
        genome build and the one being mapped to
    """
    map_tree, target_chr_sizes, source_chr_sizes = read_chain_file(
        chain_file,
        print_table=False
    )
    return map_tree


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_crossmap(map_tree, chr_name, start_pos, end_pos, strand='+'):
    """
    Provides an interface to the crossmap `cmmodule.utils.map_coordinates`
    function that actually does the liftover.

    Parameters
    ----------
    map_tree : dict
        The keys of the dict are chromosome names and the values are
        `bx.Interval` objects used to look for overlaps between the current
        genome build and the one being mapped to
    chr_name : str
        The chromosome name of the query segment
    start_pos : int
        The start position of the query segment
    end_pos : int
        The end position of the query segment
    strand : str, optional, default: '+'
        The strand for he query segment, note that the strands are '+' and '-'
        for forward and reverse strand

    Returns
    -------
    mapping : list
        The first element of the list is the liftover chromosome, the second is
        the liftover start position and the third is the liftover end position.
        Note that this implementation only returns values for liftovers that
        do not span gaps (unlike crossmap it's self). If the region can't be
        lifted or spans a gap a NULL liftover is returned. This is '0' for
        chr_name and -1 for start position and end position.
    """
    mappings = map_coordinates(
        map_tree,
        chr_name,
        start_pos,
        end_pos,
        q_strand=strand,
        print_match=False
    ) or []

    if len(mappings)/2 == 1:
        return mappings[1]
    else:
        return ['0', -1, -1, '-']


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Liftover(object):
    """
    A wrapper for lifting over a flat file
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, fail_file, mapper, chr_name_idx, start_pos_idx,
                 ref_allele_idx, alt_allele_idx, end_pos_idx=None,
                 delimiter="\t", tmp_dir=None, header=True):
        """
        """
        self.infile = infile
        self.mapper = mapper
        self.chr_name_idx = chr_name_idx
        self.start_pos_idx = start_pos_idx
        self.ref_allele_idx = ref_allele_idx
        self.alt_allele_idx = alt_allele_idx
        self.end_pos_idx = end_pos_idx
        self.delimiter = delimiter
        self.fail_file = fail_file
        self.has_header = header
        self._tmp_dir = tmp_dir
        self._temp_fail = None
        self._fail_obj = None
        self._infile_obj = None
        self.header = None
        self.nlifted = 0
        self.nrejected = 0
        self._close_infile = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        """
        if args[0] is not None:
            # Error out
            self._fail_obj.close()
            os.unlink(self._temp_fail)
        else:
            self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        """
        if isinstance(self.infile, str):
            self._close_infile = True
            if utils.is_gzip(self.infile) is True:
                self._infile_obj = gzip.open(self.infile, 'rt')
            else:
                self._infile_obj = open(self.infile, 'rt')
        else:
            self._infile_obj = self.infile

        self._temp_fail = utils.get_tmp_file(dir=self._tmp_dir)
        self._fail_obj = gzip.open(self._temp_fail, 'wt')

        if self.has_header is True:
            self.header = next(self._infile_obj)
            self._fail_obj.write(self.header)
            self.header = self.header.strip().split(self.delimiter)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        """
        if self._close_infile is True:
            self._infile_obj.close()
        self._fail_obj.close()
        shutil.move(self._temp_fail, self.fail_file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        """
        while True:
            line = next(self._infile_obj)
            row = line.rstrip().split(self.delimiter)

            # Determine the end position for the liftover, which is the longest
            # alternate allele length, to ensure that the alt allele does not
            # span any gaps
            start_pos = int(row[self.start_pos_idx])

            # We calculate the max alt length
            max_alt_len = len(row[self.alt_allele_idx]) \
                if (',' in row[self.alt_allele_idx]) is False else \
                max([len(i) for i in row[self.alt_allele_idx].split(',')])

            end_pos = start_pos + max_alt_len - 1

            # run crossmap to perform the liftover
            lifted = run_crossmap(
                self.mapper,
                row[self.chr_name_idx],
                start_pos,
                end_pos,
                strand='+'
            )

            # A '0' chromosome will indicate a liftover failure, in which
            # case the row is written to the liftover failure file
            if lifted[0] != '0':
                # Assign the lifted over data
                row[self.chr_name_idx], row[self.start_pos_idx] = \
                    lifted[0], str(lifted[1])
                self.nlifted += 1

                row[self.end_pos_idx] = str(
                    lifted[1] + len(row[self.ref_allele_idx]) - 1
                )
                return row
            else:
                # Send row to rejects and do not execute the rest of
                # the loop
                self._fail_obj.write(line)
                self.nrejected += 1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def crossmap_df(chain_file, data):
    """
    Provides the ability to liftover a dataframe. Note this was for testing
    purposes and is very slow (uses apply), so probably should not be used -
    at least for large datasets.

    Parameters
    ----------
    map_tree : dict
        The keys of the dict are chromosome names and the values are
        `bx.Interval` objects used to look for overlaps between the current
        genome build and the one being mapped to
    data : :obj:`pandas.DataFrame`
        The data containing the columns `chr_name`, `start_pos`, `end_pos`

    Returns
    -------
    data : :obj:`pandas.DataFrame`
        The data containing the liftover columns `chr_name_cm`, `start_pos_cm`,
        `end_pos_cm` and `nmappings_cm` (the number of mappings). In the case of
        split mappings the first part of the split is listed but the nmappings
        will be > 1
    """
    assign_cols = [
        'chr_name_cm',
        'start_pos_cm',
        'end_pos_cm',
        'nmappings_cm'
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _do_crossmap_apply(row):
        mappings = map_coordinates(
            chain_file,
            row['chr_name'],
            row['start_pos'],
            row['end_pos'],
            q_strand='+',
            print_match=False
        )

        try:
            if len(mappings) > 1:
                return mappings[1][0], int(mappings[1][1]),\
                    int(mappings[1][2]), len(mappings)/2
        except TypeError:
            pass
        return '0', -1, -1, 0

    data[assign_cols] = data.apply(
        _do_crossmap_apply,
        axis=1,
        result_type='expand'
    )
    return data
