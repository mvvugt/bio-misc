"""A streamlined liftover program that wraps low level CrossMap API calls. This
is designed to liftover generic flat files. It does not do any reference
genome reference sequence checking (for variants). This is left to other
(faster) programs.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.liftover import crossmap
from tqdm import tqdm
import argparse
import csv
import os
import sys
import gzip
# import pprint as pp


_SCRIPT_NAME = 'bm-flat-lift'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_liftover(infile, chain_file, fail_file, outfile=None,
                 chr_name=con.CHR_NAME, start_pos=con.START_POS,
                 end_pos=None, ref_allele=None, delimiter=con.DEFAULT_DELIMITER,
                 verbose=False, encoding=con.DEFAULT_ENCODING,
                 tmp_dir=None, comment=con.DEFAULT_COMMENT,
                 output_comments=False, output_index=False):
    """The main high level API function that takes an input file and generates
    and output file of matching entries.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chain_file : `str`
        The path to the chain file that is used to map between the source
        assembly and the target assembly.
    fail_file : `str`
        The path to a file where the rows that are unable to be lifted over
        will be written.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional, default: `NoneType`
        The end position column in the input file, if not present then the
        start position is used for end position.
    ref_allele : `str` or `NoneType`, optional, default: `NoneType`
        The name of the reference allele column in the input file (if present).
        If given and ``end_pos`` is ``NoneType``, then the end position will be
        calculated based on the start position and the length of the reference
        allele.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    verbose : `bool`, optional, default: `False`
        Report progress.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    output_comments : `bool`, optional, default: `False`
        Output any comment lines that occur before the header line.
    output_index : `bool`, optional, default: `False`
        Output an row index column as the last column in the file.

    Returns
    -------
    liftover_fails : `int`
        The number of rows that failed to liftover and were written to
        ``fail_file``.
    """
    fail_counter = dict()

    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        # writer = csv.writer(out, delimiter=delimiter,
        #                     lineterminator=os.linesep,
        #                     dialect='excel-tab',
        #                     quoting=csv.QUOTE_NONE)
        for row in tqdm(
                yield_liftover(infile, chain_file, fail_file, fail_counter,
                               chr_name=chr_name, start_pos=start_pos,
                               end_pos=start_pos, ref_allele=ref_allele,
                               encoding=encoding, delimiter=delimiter,
                               comment=comment, output_index=output_index,
                               yield_comments=output_comments),
                desc="[progress] lifting over: ",
                unit=" returned rows",
                disable=not verbose
        ):
            out.write("{0}\n".format("\t".join([str(i) for i in row])))
            # writer.writerow(row)
    return fail_counter['LIFTOVER_FAIL']


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_liftover(infile, chain_file, fail_file, fail_counter,
                   chr_name=con.CHR_NAME, yield_header=True,
                   start_pos=con.START_POS, end_pos=None, ref_allele=None,
                   encoding=con.DEFAULT_ENCODING, comment=con.DEFAULT_COMMENT,
                   delimiter=con.DEFAULT_DELIMITER, tmp_dir=None,
                   yield_comments=False, output_index=False):
    """Yield rows of lifted over coordinates.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chain_file : `str`
        The path to the chain file that is used to map between the source
        assembly and the target assembly.
    fail_file : `str`
        The path to a file where the rows that are unable to be lifted over
        will be written.
    fail_counter : `dict`
        A dictionary where the number of failed liftovers are written. This is
        required as this function yields and can not return anything.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional, default: `NoneType`
        The end position column in the input file, if not present then the
        start position is used for end position.
    ref_allele : `str` or `NoneType`, optional, default: `NoneType`
        The name of the reference allele column in the input file (if present).
        If given and ``end_pos`` is ``NoneType``, then the end position will be
        calculated based on the start position and the length of the reference
        allele.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to the fail file, a temp file is used and copied over to the
        final location when finished. Use this to specify a different temp
        location. If `NoneType` then the system default temp location is used.
    yield_comments : `bool`, optional, default: `False`
        Yield any comment lines that occur before the header line. This is only
        active if yield_header is active.
    output_index : `bool`, optional, default: `False`
        Output an row index column as the last column in the file.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    """
    index_func = _dummy
    if output_index:
        index_func = _add_index_column

    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        end_pos=end_pos,
        ref_allele=ref_allele,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    mapper = crossmap.read_chain(chain_file)
    nfails = 0

    with utils.stdopen(fail_file, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding, method=gzip.open) as fails:
        fail_writer = csv.writer(fails, delimiter=delimiter,
                                 lineterminator=os.linesep,
                                 dialect='unix', quoting=csv.QUOTE_NONE)
        with utils.CoordsParser(infile, **kwargs) as parser:
            if yield_header is True:
                if yield_comments is True:
                    for i in parser.header_comments:
                        yield i

                out_header = parser.header
                if output_index is True:
                    out_header = out_header + ['liftover_input_idx']
                yield out_header
            fail_writer.writerow(parser.header)

            for idx, row in enumerate(parser, 1):
                chr_name, start_pos, end_pos = parser.get_coords(row)

                if chr_name is None or start_pos is None:
                    continue

                try:
                    lift_site = crossmap.run_crossmap(
                        mapper, chr_name, start_pos, end_pos
                    )
                except AttributeError:
                    lift_site = [-1, -1, -1]
                    # print(chr_name, file=sys.stderr)
                    # print(start_pos, file=sys.stderr)
                    # print(end_pos, file=sys.stderr)
                    # print(pp.pformat(row), file=sys.stderr)
                    # raise

                if lift_site[1] > -1:
                    row[parser.chr_name_idx] = lift_site[0]
                    row[parser.start_pos_idx] = str(lift_site[1])
                    row[parser.end_pos_idx] = str(lift_site[2])
                    yield index_func(row, idx)
                else:
                    # Write to fails
                    nfails += 1
                    fail_writer.writerow(row)
    fail_counter['LIFTOVER_FAIL'] = nfails


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _dummy(row, idx):
    return row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _add_index_column(row, idx):
    return row + [idx]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        fails = get_liftover(
            args.infile, args.chain_file, args.fail_file, outfile=args.outfile,
            delimiter=args.delimiter, encoding=args.encoding,
            comment=args.comment, tmp_dir=args.tmp_dir, verbose=args.verbose,
            chr_name=args.chr_name, start_pos=args.start_pos,
            end_pos=args.end_pos, ref_allele=args.ref_allele,
            output_comments=args.out_comments, output_index=not args.no_index
        )
        m.msg("there were {0} failed liftover rows".format(fails))
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Liftover a flat file"
    )
    parser.add_argument(
        'chain_file',
        default=str,
        help="The chain file that contains the locations of assembly mappings"
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    parser.add_argument(
        '-f', '--fail-file',
        type=str,
        default='liftover_failures.txt.gz',
        help="A file to write liftover failures to"
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_genomic_coords_arguments(parser)
    parser.add_argument(
        '-R', '--ref-allele',
        type=str,
        default=None,
        help="A reference allele column, this can be used in place of an end"
        " position column and the end position will be the start position "
        "and the length of the reference allele"
    )
    parser.add_argument(
        '--out-comments',
        action="store_true",
        help="Output any comment lines that occur before the header (in "
        "addition to the header)"
    )
    parser.add_argument(
        '--no-index',
        action="store_true",
        help="Do not output an index column as the last row in the file."
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    args.chain_file = os.path.realpath(
        os.path.expanduser(args.chain_file)
    )
    open(args.chain_file).close()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
